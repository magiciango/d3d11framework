#pragma once
#include "Component.h"

class CMove :
    public Component
{
public:
    static size_t tag;
    CMove(Actor* obj, DirectX::XMFLOAT3 speed = { 0,0,0 }, DirectX::XMFLOAT3 force = { 0,0,0 }, DirectX::XMFLOAT3 magnetic = { 0,0,0 });
    CMove();
    void Move(Transform& transform,float dt);
    void Stop();
    void SetSpeed(DirectX::XMFLOAT3& speed);
    void SetSpeed(float x, float y, float z);
    void SetSpeed(const DirectX::XMFLOAT3& direction, float speed);
    void AddForce(const DirectX::XMFLOAT3& direction, float force);
    void AddElastic(float strength);
public:
    DirectX::XMFLOAT3 m_Speed;              //速度(单位:
    DirectX::XMFLOAT3 m_Force;              //力场加速度，提供加速度(单位：pixel/s^2)
    DirectX::XMFLOAT3 m_Magnetic;           //磁场加速度，我觉得用不上，提供旋转
    float m_MaxSpeed;                       //最大速度标量，超过该速度会匀速
    float m_Friction;                       //摩擦力，自动减速(单位:pixel/s^2）
    bool m_moveble = true;
};

