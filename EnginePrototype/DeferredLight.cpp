#include "DeferredLight.h"
#include "Effects.h"
#include <DirectXColors.h>
HRESULT DeferredLight::InitAll(int texWidth, int texHeight, bool generateMips, ID3D11Device* device)
{
	HRESULT hr;
	m_pGeometryPass = std::make_unique<TextureRender>();
	m_pLightPass = std::make_unique<TextureRender>();
	hr = m_pGeometryPass->InitResource(texWidth, texHeight, false, 2, DXGI_FORMAT_R16G16B16A16_FLOAT, generateMips, device);
	if (FAILED(hr))
		return hr;
	hr = m_pLightPass->InitResource(texWidth, texHeight, false, 3, DXGI_FORMAT_R32G32B32A32_FLOAT, generateMips, device);
	if (FAILED(hr))
		return hr;
	Geometry::MeshData<VertexPosTex> data = Geometry::Create2DShow();
	return hr;
}

void DeferredLight::Begin(ID3D11DeviceContext* deviceContext)
{
	BasicEffect::Get().SetRenderDefferedPass(deviceContext);
	m_pGeometryPass->Begin(Colors::Black, deviceContext);
}

void DeferredLight::EndGPass(ID3D11DeviceContext* deviceContext)
{
	m_pGeometryPass->End();
	auto& effect = BasicEffect::Get();
	effect.SetTextureDiffuse(m_pGeometryPass->GetOutputTexture(0));
	effect.SetTextureNormal(m_pGeometryPass->GetOutputTexture(1));
	effect.SetRenderDefferedPreL(deviceContext);
	effect.Apply(deviceContext);
	m_pLightPass->Begin(Colors::Black,deviceContext);
}

void DeferredLight::EndLPass(ID3D11DeviceContext* deviceContext)
{
	m_pLightPass->End(deviceContext);
	auto& effect = BasicEffect::Get();
	effect.SetTextureDiffuse(nullptr);
	effect.SetTextureNormal(nullptr);
	effect.SetRenderDefferedDraw(deviceContext);
	effect.SetAmbientTexture(m_pLightPass->GetOutputTexture(0));
	effect.SetDiffuseTexture(m_pLightPass->GetOutputTexture(1));
	effect.SetSpecTexture(m_pLightPass->GetOutputTexture(2));
	effect.Apply(deviceContext);
}

ID3D11ShaderResourceView* DeferredLight::GetOutputTexture(Output type)
{
	switch (type)
	{
	case DeferredLight::Output::Position:
		return m_pGeometryPass->GetOutputTexture(0);
		break;
	case DeferredLight::Output::Normal:
		return m_pGeometryPass->GetOutputTexture(1);
		break;
	case DeferredLight::Output::Ambient:
		return m_pLightPass->GetOutputTexture(0);
		break;
	case DeferredLight::Output::Diffuse:
		return m_pLightPass->GetOutputTexture(1);
		break;
	case DeferredLight::Output::Spec:
		return m_pLightPass->GetOutputTexture(2);
		break;
	default:
		break;
	}
}
