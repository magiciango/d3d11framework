#include "SEnd.h"
#include "Box.h"
#include "Tree.h"

bool SEnd::Init()
{
	return InitResource();
}

int SEnd::UpdateScene(float dt)
{
	Input& input = D3DAppGet::InputState();
	static float time = 0;
	time += dt;
	auto cam1st = std::dynamic_pointer_cast<FirstPersonCamera>(m_pCamera);
	cam1st->MoveForward(dt * 6);
	cam1st->LookTo(cam1st->GetPosition(), XMFLOAT3(0, sinf(time*0.5), 2), XMFLOAT3(0, 1, 0));
	//cam1st->Pitch(sinf(time)*0.001);
	/*bool shift = input.GetKeyDown(Keyboard::LeftControl);
	if (input.GetKeyDown(Keyboard::W))
		cam1st->MoveForward(dt * (shift ? 20.0f : 6.0f));
	if (input.GetKeyDown(Keyboard::S))
		cam1st->MoveForward(dt * (shift ? -20.0f : -6.0f));
	if (input.GetKeyDown(Keyboard::A))
		cam1st->Strafe(dt * (shift ? -20.0f : -6.0f));
	if (input.GetKeyDown(Keyboard::D))
		cam1st->Strafe(dt * (shift ? 20.0f : 6.0f));

	if (input.GetKeyDown(Keyboard::Right))
		cam1st->RotateY(dt * 6.25f);
	if (input.GetKeyDown(Keyboard::Left))
		cam1st->RotateY(-dt * 6.25f);
	if (input.GetKeyDown(Keyboard::Up))
		cam1st->Pitch(dt * 6.25f);
	if (input.GetKeyDown(Keyboard::Down))
		cam1st->Pitch(-dt * 6.25f);*/
	m_DBox.Update(dt);

	if (D3DAppGet::InputState().GetKeyPressed(Keyboard::Q))
		return 0;
	return -1;
}

void SEnd::OnResize()
{
	DX11Contexts& renderer = D3DAppGet::AllContexts();
	m_pColorBrush.Reset();
	// 创建固定颜色刷和文本格式
	HR(renderer.pd2dRenderTarget->CreateSolidColorBrush(
		D2D1::ColorF(D2D1::ColorF::White),
		m_pColorBrush.GetAddressOf()));
	HR(renderer.pdwriteFactory->CreateTextFormat(L"宋体", nullptr, DWRITE_FONT_WEIGHT_NORMAL,
		DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 15, L"zh-cn",
		m_pTextFormat.GetAddressOf()));


	/*if (m_pCamera != nullptr)
	{
		m_pCamera->SetFrustum(XM_PI / 3, AspectRatio(), 1.0f, 1000.0f);
		m_pCamera->SetViewPort(0.0f, 0.0f, (float)m_ClientWidth, (float)m_ClientHeight);
		m_BasicEffect.SetProjMatrix(m_pCamera->GetProjXM());
	}*/
}

void SEnd::DrawScene(bool isOpaquePass)
{
	auto& effect = BasicEffect::Get();
	//BasicEffect::Get().SetRenderDefault(D3DAppGet::pContext());
	BasicEffect::Get().SetAnimeFrame(-1);
	BasicEffect::Get().SetViewMatrix(m_pCamera->GetViewXM());
	BasicEffect::Get().SetEyePos(m_pCamera->GetPosition());
	m_DBox.Draw(m_pCamera.get());
	m_List.DrawOpaque(&effect, m_pCamera->GetViewXM(), m_pCamera->GetProjXM());
	
}

bool SEnd::InitResource()
{
	m_ScreenMesh.SetBuffer(Geometry::Create2DShow());
	m_DBox.Init();
	// ******************
	// 初始化摄像机
	//
	auto camera = std::shared_ptr<FirstPersonCamera>(new FirstPersonCamera);
	m_pCamera = camera;
	camera->SetViewPort(0.0f, 0.0f, (float)D3DAppGet::Width(), (float)D3DAppGet::Height());
	camera->GetTransform().SetPosition(0, 5, -15);

	auto& effect = BasicEffect::Get();
	auto& renderer = D3DAppGet::AllContexts();
	effect.SetViewMatrix(m_pCamera->GetViewXM());
	effect.SetEyePos(m_pCamera->GetPosition());

	m_pCamera->SetFrustum(XM_PI / 3, D3DAppGet::AspectRatio(), 0.5f, 1000.0f);

	effect.SetProjMatrix(m_pCamera->GetProjXM());

	//初始化游戏对象
	//ObjectManager::Instance()->Clear();
	auto obj = new Box(0, 0, 0, 0, 0, 0, 20, 20, 20);
	auto com = obj->GetComponent<CGeometry>();
	com->SetBuffer(Geometry::Create2DShow(XMFLOAT2(0.5f, -0.5f), XMFLOAT2(0.5f, 0.5f)));
	ComPtr<ID3D11ShaderResourceView> texture;
	HR(CreateWICTextureFromFile(D3DAppGet::pDevice(), L"..\\Texture\\wood.jpg", nullptr, texture.GetAddressOf()));
	com->SetTexture(texture.Get());
	m_List.AddOpaqueObj(obj);
	/*SkyBox = new AAtmospheric();
	SkyBox->Init();*/
	//初始化天空盒
	m_SkyBox.InitResource(std::vector<std::wstring>{
		L"..\\Texture\\Storforsen\\posx.jpg", L"..\\Texture\\Storforsen\\negx.jpg",
			L"..\\Texture\\Storforsen\\posy.jpg", L"..\\Texture\\Storforsen\\negy.jpg",
			L"..\\Texture\\Storforsen\\posz.jpg", L"..\\Texture\\Storforsen\\negz.jpg", });

	// 环境光
	DirectionalLight dirLight;
	dirLight.ambient = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	dirLight.diffuse = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	dirLight.specular = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	dirLight.direction = XMFLOAT3(0.0f, -1.0f, 0.0f);
	BasicEffect::Get().SetDirLight(0, dirLight);
	sunPos = XMFLOAT3(0, 1, 0);
	// 灯光
	PointLight pointLight;
	pointLight.position = XMFLOAT3(0.0f, 10.0f, -10.0f);
	pointLight.ambient = XMFLOAT4(0.3f, 0.3f, 0.3f, 1.0f);
	pointLight.diffuse = XMFLOAT4(0.6f, 0.6f, 0.6f, 1.0f);
	pointLight.specular = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);
	pointLight.att = XMFLOAT3(0.0f, 0.1f, 0.0f);
	pointLight.range = 25.0f;
	BasicEffect::Get().SetPointLight(0, pointLight);

	m_pColorBrush.Reset();
	HR(D3DAppGet::AllContexts().pd2dRenderTarget->CreateSolidColorBrush(
		D2D1::ColorF(D2D1::ColorF::Black),
		m_pColorBrush.GetAddressOf()));
	HR(D3DAppGet::AllContexts().pdwriteFactory->CreateTextFormat(L"宋体", nullptr, DWRITE_FONT_WEIGHT_NORMAL,
		DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 55, L"zh-cn",
		m_pTextFormat.GetAddressOf()));

	return true;
}
