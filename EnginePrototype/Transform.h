
#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <DirectXMath.h>

class Transform
{
public:
	Transform() = default;
	Transform(const DirectX::XMFLOAT3& scale, const DirectX::XMFLOAT3& rotation, const DirectX::XMFLOAT3& position);
	~Transform() = default;

	Transform(const Transform&) = default;
	Transform& operator=(const Transform&) = default;

	Transform(Transform&&) = default;
	Transform& operator=(Transform&&) = default;

	#pragma region Get Function

	// Scale取得
	DirectX::XMFLOAT3 GetScale() const;
	// Scale取得
	DirectX::XMVECTOR GetScaleXM() const;

	// Rotation取得(Radian)
	DirectX::XMFLOAT3 GetRotation() const;
	// Rotation取得(Radian)
	DirectX::XMVECTOR GetRotationXM() const;

	// Position取得
	DirectX::XMFLOAT3 GetPosition() const;
	// Position取得
	DirectX::XMVECTOR GetPositionXM() const;

	// 右方向取得
	DirectX::XMFLOAT3 GetRightAxis() const;
	// 右方向取得
	DirectX::XMVECTOR GetRightAxisXM() const;

	// 上方向取得
	DirectX::XMFLOAT3 GetUpAxis() const;
	// 上方向取得
	DirectX::XMVECTOR GetUpAxisXM() const;

	// 前方向取得
	DirectX::XMFLOAT3 GetForwardAxis() const;
	// 前方向取得
	DirectX::XMVECTOR GetForwardAxisXM() const;

	// 世界変換マトリックス(Local to World)
	DirectX::XMFLOAT4X4 GetLocalToWorldMatrix() const;
	// 世界変換マトリックス(Local to World)
	DirectX::XMMATRIX GetLocalToWorldMatrixXM() const;

	// 世界変換逆マトリックス(World to Local)
	DirectX::XMFLOAT4X4 GetWorldToLocalMatrix() const;
	// 世界変換逆マトリックス(World to Local)
	DirectX::XMMATRIX GetWorldToLocalMatrixXM() const;
#pragma endregion

	#pragma region Set Scale
	// Scale設定(=)
	void SetScale(const DirectX::XMFLOAT3& scale);
	// Scale設定(=)
	void SetScale(float x, float y, float z);

	//Scale設定(+=)
	void AddScale(const DirectX::XMFLOAT3& scale);
	//Scale設定(+=)
	void AddScale(float x, float y, float z);

	//Scale設定(*=)
	void MultiScale(const DirectX::XMFLOAT3& scale);
	//Scale設定(*=)
	void MultiScale(float x, float y, float z);
#pragma endregion

	#pragma region Set Rotation

	// オブジェクトは、Z-X-Y順で回る
	// Euler角度(Radian)指定
	void SetRotation(const DirectX::XMFLOAT3& eulerAnglesInRadian);
	// Euler角度(Radian)指定
	void SetRotation(float x, float y, float z);

	// 回転
	void Rotate(const DirectX::XMFLOAT3& eulerAnglesInRadian);
	void Rotate(const DirectX::XMVECTOR& quaternation);
	// 原点を通た軸を中心に回転
	void RotateAxis(const DirectX::XMFLOAT3& axis, float radian);
	// 指定ポイントを通た軸を中心に回転
	void RotateAround(const DirectX::XMFLOAT3& point, const DirectX::XMFLOAT3& axis, float radian);

	// 指定ポイントを見る
	void LookAt(const DirectX::XMFLOAT3& target, const DirectX::XMFLOAT3& up = { 0.0f, 1.0f, 0.0f });
	// 指定方向へ見る
	void LookTo(const DirectX::XMFLOAT3& direction, const DirectX::XMFLOAT3& up = { 0.0f, 1.0f, 0.0f });
#pragma endregion

	#pragma region Set Position
	// Position設定(=)
	void SetPosition(const DirectX::XMFLOAT3& position);
	// Position設定(=)
	void SetPosition(float x, float y, float z);
	// Position設定(+=)
	void AddPosition(const DirectX::XMFLOAT3& position);
	// Position設定(+=)
	void AddPosition(float x, float y, float z);

	// 指定方向へ移動
	void Translate(const DirectX::XMFLOAT3 & direction, float length);
#pragma endregion

private:
	// 回転マトリックスから回転角度求む
	DirectX::XMFLOAT3 GetEulerAnglesFromRotationMatrix(const DirectX::XMFLOAT4X4& rotationMatrix);

private:
	DirectX::XMFLOAT3 m_Scale = { 1.0f, 1.0f, 1.0f };				
	DirectX::XMFLOAT3 m_Rotation = {};			
	//DirectX::XMFLOAT4 m_RotationQuat = {};
	DirectX::XMFLOAT3 m_Position = {};								
};

#endif


