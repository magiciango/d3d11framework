#include "Game.h"

#include "d3dUtil.h"
#include "DXTrace.h"

#include "CAudio.h"
namespace
{
	static Game* g_pGame = nullptr;
}
Game::Game(HINSTANCE hInstance)
	:D3DApp(hInstance),
	m_SceneNumber(0),
	m_NextScene(0),
	m_FadeUsed(true),
	m_FadeAmount(0.5),
	m_FadeType(Game::FadeType::normal)
{
	if (g_pGame != nullptr)
		throw std::exception("Game is a singleton!");
	g_pGame = this;
}
Game* Game::Get()
{
	return g_pGame;
}
;
bool Game::Init()
{
	if (!D3DApp::Init())
		return false;

	RenderStates::InitAll(m_Contexts.pd3dDevice.Get());
	if (!InitResource())
		return false;

	
	return true;
}

void Game::OnResize()
{
	assert(m_Contexts.pd2dFactory);
	assert(m_Contexts.pdwriteFactory);
	m_Contexts.pd2dRenderTarget.Reset();

	D3DApp::OnResize();

	ComPtr<IDXGISurface> surface;
	HR(m_Contexts.pSwapChain->GetBuffer(0, __uuidof(IDXGISurface), reinterpret_cast<void**>(surface.GetAddressOf())));
	D2D1_RENDER_TARGET_PROPERTIES props = D2D1::RenderTargetProperties(
		D2D1_RENDER_TARGET_TYPE_DEFAULT,
		D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED));
	HRESULT hr = m_Contexts.pd2dFactory->CreateDxgiSurfaceRenderTarget(surface.Get(), &props, m_Contexts.pd2dRenderTarget.GetAddressOf());
	surface.Reset();
	if (hr != S_OK)
	{
		assert(m_Contexts.pd2dRenderTarget);
	}
	for (auto& i : m_pScenes)
	{
		i->OnResize();
	}
}

void Game::UpdateScene(float dt)
{
	#pragma region ImguiFrame
	// Start the Dear ImGui frame
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
#pragma endregion


	m_Input.Update();
	if (m_Input.GetKeyPressed(Keyboard::Escape))
	{
		SendMessage(MainWnd(), WM_DESTROY, 0, 0);
	}
	static bool FadeDebugger = false;
#pragma region ImguiFadeDebug

	/*ImGui::Begin("GameDebugger");
	ImGui::Checkbox("DeferredLighting", &UseDeferred);
	ImGui::Checkbox("OITRendering", &UseOIT);
	ImGui::Checkbox("FadeDebuger", &FadeDebugger);
	if (ImGui::Button("Normal"))
		m_FadeType = FadeType::normal;
	if(ImGui::Button("Curl"))
		m_FadeType = FadeType::curl;
	if(ImGui::Button("Grid"))
		m_FadeType = FadeType::Grid;
	if (FadeDebugger)
	{
		ImGui::SliderFloat("FadeAmount", &m_FadeAmount, 0, 1);
	}
	ImGui::End();*/

#pragma endregion
	if (m_FadeUsed)
	{
		if (!FadeDebugger)
			m_FadeAmount += dt * 0.5;
		if (m_FadeAmount > 1)
		{
			m_FadeAmount = 0;
			m_FadeUsed = false;
			SetScene();
		}
	}
	else
	{
		static bool EndScene = false;
		m_NextScene = m_pScenes[m_SceneNumber]->UpdateScene(dt);
		if (m_Input.GetKeyPressed(Keyboard::U))
		{
			m_NextScene = EndScene ? 0 : 2;
			EndScene = !EndScene;
		}
		if (m_NextScene != -1)
		{
			m_FadeUsed = true;
			m_FadeEffect.SetMousePosition(m_Input.GetMousePosX(), m_Input.GetMousePosY());
			LoadScene(m_NextScene);
		}
	}
	m_Input.ResetScroll();
}

void Game::DrawScene()
{
	assert(m_Contexts.pd3dImmediateContext);
	assert(m_Contexts.pSwapChain);

	auto pContext = D3DAppGet::pContext();
	pContext->ClearRenderTargetView(m_pRenderTargetView.Get(), reinterpret_cast<const float*>(&Colors::Black));
	pContext->ClearDepthStencilView(m_pDepthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	
	UINT strides = sizeof(VertexPosTex);
	UINT offset = 0;
	//Fade Effect
	if (m_FadeUsed)
	{
		if (m_FadeType == FadeType::normal)
		{
			//m_BasicEffect.SetUseDeferred(false);
			m_pFadeRender_old->Begin(Colors::Black);
			m_pScenes[m_FadeAmount <= 0.5 ? m_SceneNumber : m_NextScene]->DrawScene(true);
			m_pScenes[m_FadeAmount <= 0.5 ? m_SceneNumber : m_NextScene]->DrawScene(false);
			m_pFadeRender_old->End();

			m_FadeEffect.SetRenderDefault(pContext);
			m_FadeEffect.SetFadeAmount(m_FadeAmount <= 0.5 ? 1 - m_FadeAmount * 2 : m_FadeAmount * 2 - 1);
			m_FadeEffect.SetTexture(m_pFadeRender_old->GetOutputTexture());
			m_FadeEffect.SetWorldViewProjMatrix(XMMatrixIdentity());
			m_FadeEffect.Apply(pContext);

			pContext->IASetVertexBuffers(0, 1, m_ScreenMesh->m_pVertexBuffer.GetAddressOf(), &strides, &offset);
			pContext->IASetIndexBuffer(m_ScreenMesh->m_pIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
			pContext->DrawIndexed(6, 0, 0);

			m_FadeEffect.SetTexture(nullptr);
			m_FadeEffect.Apply(pContext);
		}
		else
		{
			m_pFadeRender_old->Begin(Colors::Black);
			m_pScenes[m_SceneNumber]->DrawScene(true);
			m_pScenes[m_SceneNumber]->DrawScene(false);
			m_pFadeRender_old->End();
			m_pFadeRender_new->Begin(Colors::Black);
			m_pScenes[m_NextScene]->DrawScene(true);
			m_pScenes[m_SceneNumber]->DrawScene(false);
			m_pFadeRender_new->End();

			switch (m_FadeType)
			{
			case FadeType::curl:
				m_FadeEffect.SetRenderCurl(pContext);
				break;
			case FadeType::Grid:
				m_FadeEffect.SetRenderGrid(pContext);
				break;
			default:
				break;
			}

			m_FadeEffect.SetFadeAmount(m_FadeAmount);
			m_FadeEffect.SetTexture(m_pFadeRender_new->GetOutputTexture());
			m_FadeEffect.SetNextTexture(m_pFadeRender_old->GetOutputTexture());
			m_FadeEffect.SetWorldViewProjMatrix(XMMatrixIdentity());
			m_FadeEffect.Apply(pContext);

			pContext->IASetVertexBuffers(0, 1, m_ScreenMesh->m_pVertexBuffer.GetAddressOf(), &strides, &offset);
			pContext->IASetIndexBuffer(m_ScreenMesh->m_pIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
			pContext->DrawIndexed(6, 0, 0);

			m_FadeEffect.SetTexture(nullptr);
			m_FadeEffect.Apply(pContext);
		}
	}
	//Normal Draw
	else
	{
		m_pScenes[m_SceneNumber]->DrawScene(true);
	}
#pragma endregion
	ImGui::Render();
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
	HR(m_Contexts.pSwapChain->Present(0, 0));
}

bool Game::InitResource()
{
	if (!m_BasicEffect.InitAll(m_Contexts.pd3dDevice.Get()))
		return false;
	if (!m_SkyEffect.InitAll(m_Contexts.pd3dDevice.Get()))
		return false;
	m_SkyEffect.SetScreenSize(m_ClientWidth, m_ClientHeight);
	if (!m_FadeEffect.InitAll(m_Contexts.pd3dDevice.Get()))
		return false;
	m_FadeEffect.SetScreenSize(m_ClientWidth, m_ClientHeight);
	if (!m_ShadowEffect.InitAll(m_Contexts.pd3dDevice.Get()))
		return false;
	if (!m_SSAOEffect.InitAll(m_Contexts.pd3dDevice.Get()))
		return false;
	/*if (!m_SkyLightEffect.InitAll(m_Contexts.pd3dDevice.Get()))
		return false;
	m_SkyLightEffect.SetScreenSize(m_ClientWidth, m_ClientHeight);*/
	if (!m_DebugEffect.InitAll(m_Contexts.pd3dDevice.Get()))
		return false;
	m_DebugEffect.SetScreenSize(m_ClientWidth, m_ClientHeight);
	if (!m_DeferredEffect.InitAll(m_Contexts.pd3dDevice.Get()))
		return false;
	//if (!m_DeferredEffect.InitAll(m_Contexts.pd3dDevice.Get()))
		//return false;
	/*if (!m_AtomospereEffect.InitAll(D3DAppGet::pDevice()))
		return false;*/
	//Audio初期化
	//CAudio::InitMaster();

	//Scene追加
	m_pScenes.push_back(make_unique<STitle>());
	//m_pScenes.push_back(make_unique<SGame>());
	//m_pScenes.push_back(make_unique<SEnd>());

	if (!m_pScenes[m_SceneNumber]->Init())
		return false;
	//屏幕Mesh
	m_ScreenMesh = make_unique<CGeometry>();
	m_ScreenMesh->SetBuffer(Geometry::Create2DShow());

	//RTT初始化
	m_pFadeRender_old = make_unique<TextureRender>();
	HR(m_pFadeRender_old->InitResource(m_ClientWidth, m_ClientHeight));
	m_pFadeRender_new = make_unique<TextureRender>();
	HR(m_pFadeRender_new->InitResource(m_ClientWidth, m_ClientHeight));
	return true;
}

bool Game::LoadScene(int sceneNumber)
{
	if (sceneNumber >= 0)
	{
		if (!m_pScenes[sceneNumber]->Init())
			return false;
		return true;
	}
	return false;
	
}

bool Game::SetScene()
{
	//m_pScenes[m_SceneNumber]->放棄
	m_SceneNumber = m_NextScene;
	return true;
}

Scene* Game::GetActiveScene()
{
	return m_pScenes[m_SceneNumber].get();
}

//OITRender* Game::GetOITRender()
//{
//	return m_OITRender.get();
//}
