#include "Effects.h"
#include "d3dUtil.h"
#include "EffectHelper.h"	
#include "DXTrace.h"
#include "Vertex.h"
using namespace DirectX;

//
// BasicEffect::Impl 需要先于BasicEffect的定义
//

class BasicEffect::Impl
{

public:
	// 必须显式指定
	Impl()  {}
	~Impl() = default;

public:

	ComPtr<ID3D11InputLayout> m_pVertexPosSizeLayout;			// 点精灵输入布局
	ComPtr<ID3D11InputLayout> m_pVertexPosTexLayout;			
	ComPtr<ID3D11InputLayout> m_pVertexPosNormalTexLayout;		// 3D顶点输入布局
	ComPtr<ID3D11InputLayout> m_pBonesVertexPosNormalTexLayout;	// 骨骼顶点输入布局
	ComPtr<ID3D11InputLayout> m_pPosInputLayout;				// 单顶点

	ComPtr<ID3D11InputLayout> m_pVertexLayout;		// 法线贴图3D顶点输入布局
	ComPtr<ID3D11InputLayout> m_pBoneLayout;	// 法线贴图骨骼顶点输入布局

	ComPtr<ID3D11InputLayout> m_pVertexInstancingLayout;		// 实例化法线贴图3D顶点输入布局

	std::unique_ptr<EffectHelper> m_pEffectHelper;

	std::shared_ptr<IEffectPass> m_pCurrEffectPass;

	XMFLOAT4X4 m_World{}, m_View{}, m_Proj{};

	DefferedPass m_DefferedPass;
	BOOL m_LockPassOnOIT = false;

	//管线控制参数
	RSFillMode m_fillMode;
	RenderType m_renderType;
	MeshType m_meshType;
	BOOL m_isTransparent;
	/*BOOL m_UseNormalMap;*/
	BOOL m_UseHeightMap;
	BOOL m_isMatCap;
	BOOL m_useSSAO;
	BOOL m_UsePBR;
};

//
// BasicEffect
//

namespace
{
	// BasicEffect单例
	static BasicEffect* g_pInstance = nullptr;
}

BasicEffect::BasicEffect()
{
	if (g_pInstance)
		throw std::exception("BasicEffect is a singleton!");
	g_pInstance = this;
	pImpl = std::make_unique<BasicEffect::Impl>();
}

BasicEffect::~BasicEffect()
{
}

BasicEffect::BasicEffect(BasicEffect&& moveFrom) noexcept
{
	pImpl.swap(moveFrom.pImpl);
}

BasicEffect& BasicEffect::operator=(BasicEffect&& moveFrom) noexcept
{
	pImpl.swap(moveFrom.pImpl);
	return *this;
}

BasicEffect& BasicEffect::Get()
{
	if (!g_pInstance)
		throw std::exception("BasicEffect needs an instance!");
	return *g_pInstance;
}


bool BasicEffect::InitAll(ID3D11Device* device)
{
	if (!device)
		return false;

	if (!RenderStates::IsInit())
		throw std::exception("RenderStates need to be initialized first!");

	pImpl->m_pEffectHelper = std::make_unique<EffectHelper>();

	ComPtr<ID3DBlob> blob;

	
	// ******************
	// 创建顶点输入布局
	//
	D3D11_INPUT_ELEMENT_DESC normalmapInstLayout[] = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 40, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "World", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1},
		{ "World", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
		{ "World", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D11_INPUT_PER_INSTANCE_DATA, 1},
		{ "World", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48, D3D11_INPUT_PER_INSTANCE_DATA, 1},
		{ "WorldInvTranspose", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 64, D3D11_INPUT_PER_INSTANCE_DATA, 1},
		{ "WorldInvTranspose", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 80, D3D11_INPUT_PER_INSTANCE_DATA, 1},
		{ "WorldInvTranspose", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 96, D3D11_INPUT_PER_INSTANCE_DATA, 1},
		{ "WorldInvTranspose", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 112, D3D11_INPUT_PER_INSTANCE_DATA, 1}
	};
	
	

	// ******************
	// 顶点着色器
	//
	
	// ******************
	// 法线3D绘制
	//
	HR(CreateShaderFromFile(L"HLSL\\Basic_Normal_VS.cso", L"HLSL\\Basic_Normal_VS.hlsl", "VS", "vs_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Basic_Normal_VS", device, blob.Get()));

	// ******************
	// 高度图3D绘制
	//
	HR(CreateShaderFromFile(L"HLSL\\Basic_HeightMap_VS.cso", L"HLSL\\Basic_HeightMap_VS.hlsl", "VS", "vs_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Basic_HeightMap_VS", device, blob.Get()));
	// 创建顶点输入布局
	HR(device->CreateInputLayout(VertexPosNormalTangentTex::inputLayout, ARRAYSIZE(VertexPosNormalTangentTex::inputLayout), blob->GetBufferPointer(),
		blob->GetBufferSize(), pImpl->m_pVertexLayout.GetAddressOf()));
	// 创建实例化顶点输入布局
	HR(device->CreateInputLayout(normalmapInstLayout, ARRAYSIZE(normalmapInstLayout), blob->GetBufferPointer(),
		blob->GetBufferSize(), pImpl->m_pVertexInstancingLayout.GetAddressOf()));
	// ******************
	// 法线骨骼动画绘制
	//
	HR(CreateShaderFromFile(L"HLSL\\Bones_Normal_VS.cso", L"HLSL\\Bones_Normal_VS.hlsl", "VS", "vs_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Bones_Normal_VS", device, blob.Get()));
	//创建骨骼顶点输入布局
	HR(device->CreateInputLayout(BoneVertexPosNormalTangentTex::inputLayout, ARRAYSIZE(BoneVertexPosNormalTangentTex::inputLayout), blob->GetBufferPointer(),
		blob->GetBufferSize(), pImpl->m_pBoneLayout.GetAddressOf()));
	// ******************
	// 域和外壳着色器
	//

	HR(CreateShaderFromFile(L"HLSL\\Basic_HeightMap_HS.cso", L"HLSL\\Basic_HeightMap_HS.hlsl", "HS", "hs_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Basic_HeightMap_HS", device, blob.Get()));

	HR(CreateShaderFromFile(L"HLSL\\Basic_HeightMap_DS.cso", L"HLSL\\Basic_HeightMap_DS.hlsl", "DS", "ds_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Basic_HeightMap_DS", device, blob.Get()));

	// ******************
	// 像素着色器
	//
	
	//有法线lit
	HR(CreateShaderFromFile(L"HLSL\\Basic_Normal_PS.cso", L"HLSL\\Basic_Normal_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Basic_Normal_PS", device, blob.Get()));

	HR(CreateShaderFromFile(L"HLSL\\Basic_MatCap_PS.cso", L"HLSL\\Basic_MatCap_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Basic_MatCap_PS", device, blob.Get()));

	HR(CreateShaderFromFile(L"HLSL\\PBR_PS.cso", L"HLSL\\PBR_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("PBR_PS", device, blob.Get()));

	//// ******************
	//// 骨骼动画绘制
	////
	//HR(CreateShaderFromFile(L"HLSL\\Bones_VS.cso", L"HLSL\\Bones_VS.hlsl", "VS", "vs_5_0", blob.ReleaseAndGetAddressOf()));
	//HR(pImpl->m_pEffectHelper->AddShader("Bones_VS", device, blob.Get()));

	

	//// ******************
	//// 常规3D绘制
	////
	//HR(CreateShaderFromFile(L"HLSL\\Basic_VS.cso", L"HLSL\\Basic_VS.hlsl", "VS", "vs_5_0", blob.ReleaseAndGetAddressOf()));
	//HR(pImpl->m_pEffectHelper->AddShader("Basic_VS", device, blob.Get()));
	

	//无法线lit
	HR(CreateShaderFromFile(L"HLSL\\Basic_PS.cso", L"HLSL\\Basic_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Basic_PS", device, blob.Get()));
	
	// ******************
	// 2D全屏绘制
	// 
	HR(CreateShaderFromFile(L"HLSL\\Basic_Screen_VS.cso", L"HLSL\\Basic_Screen_VS.hlsl", "VS", "vs_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Basic_Screen_VS", device, blob.Get()));
	// 创建顶点输入布局
	HR(device->CreateInputLayout(VertexPosTex::inputLayout, ARRAYSIZE(VertexPosTex::inputLayout), blob->GetBufferPointer(),
		blob->GetBufferSize(), pImpl->m_pVertexPosTexLayout.GetAddressOf()));

	// ******************
	// 绘制公告板
	//
	HR(CreateShaderFromFile(L"HLSL\\Billboard_VS.cso", L"HLSL\\Billboard_VS.hlsl", "VS", "vs_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Billboard_VS", device, blob.Get()));
	// 创建顶点输入布局
	HR(device->CreateInputLayout(VertexPosSize::inputLayout, ARRAYSIZE(VertexPosSize::inputLayout), blob->GetBufferPointer(),
		blob->GetBufferSize(), pImpl->m_pVertexPosSizeLayout.GetAddressOf()));

	HR(CreateShaderFromFile(L"HLSL\\Billboard_GS.cso", L"HLSL\\Billboard_GS.hlsl", "GS", "gs_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Billboard_GS", device, blob.Get()));

	HR(CreateShaderFromFile(L"HLSL\\Billboard_PS.cso", L"HLSL\\Billboard_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Billboard_PS", device, blob.Get()));

	EffectPassDesc passDesc;

	passDesc.nameVS = "Basic_Normal_VS";
	passDesc.namePS = "Basic_Normal_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("VertexBasicObject", device, &passDesc));

	passDesc.nameVS = "Basic_HeightMap_VS";
	passDesc.nameHS = "Basic_HeightMap_HS";
	passDesc.nameDS = "Basic_HeightMap_DS";
	passDesc.namePS = "Basic_Normal_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("VertexHeightMapObject", device, &passDesc));

	passDesc.nameVS = "Basic_Normal_VS";
	passDesc.nameHS = nullptr;
	passDesc.nameDS = nullptr;
	passDesc.namePS = "Basic_MatCap_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("VertexBasicMatCapObject", device, &passDesc));

	passDesc.nameVS = "Basic_HeightMap_VS";
	passDesc.nameHS = "Basic_HeightMap_HS";
	passDesc.nameDS = "Basic_HeightMap_DS";
	passDesc.namePS = "Basic_MatCap_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("VertexHeightMapMatCapObject", device, &passDesc));

	passDesc.nameVS = "Basic_Normal_VS";
	passDesc.nameHS = nullptr;
	passDesc.nameDS = nullptr;
	passDesc.namePS = "PBR_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("VertexBasicPBRObject", device, &passDesc));

	passDesc.nameVS = "Basic_HeightMap_VS";
	passDesc.nameHS = "Basic_HeightMap_HS";
	passDesc.nameDS = "Basic_HeightMap_DS";
	passDesc.namePS = "PBR_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("VertexHeightMapPBRObject", device, &passDesc));

	passDesc.nameVS = "Bones_Normal_VS";
	passDesc.namePS = "Basic_Normal_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("BonesBasicObject", device, &passDesc));

	passDesc.nameVS = "Bones_Normal_VS";
	passDesc.namePS = "Basic_MatCap_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("BonesBasicMatCapObject", device, &passDesc));

	passDesc.nameVS = "Billboard_VS";
	passDesc.nameGS = "Billboard_GS";
	passDesc.namePS = "Billboard_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("Billboard", device, &passDesc));


	// 设置调试对象名
	/*D3D11SetDebugObjectName(pImpl->m_pBonesVertexPosNormalTexLayout.Get(), "BoneVertexLayout");
	D3D11SetDebugObjectName(pImpl->m_pVertexPosNormalTexLayout.Get(), "VertexPosNormalTexLayout");
	D3D11SetDebugObjectName(pImpl->m_pVertexPosSizeLayout.Get(), "VertexPosSizeLayout");*/

	return true;
}
void BasicEffect::SetRenderBones(ID3D11DeviceContext* deviceContext)
{
	pImpl->m_meshType = BoneMesh;
}

void BasicEffect::SetFillMode(RSFillMode mode)
{
	pImpl->m_fillMode = mode;
}

void BasicEffect::SetMeshType(MeshType type)
{
	pImpl->m_meshType = type;
}

void BasicEffect::SetRenderType(RenderType type)
{
	pImpl->m_renderType = type;
}

void BasicEffect::SetIsTransparent(bool transparent)
{
	pImpl->m_isTransparent = transparent;
}

void BasicEffect::SetIsMatCap(bool isMatCap)
{
	pImpl->m_isMatCap = isMatCap;
}

void BasicEffect::SetRenderPass(ID3D11DeviceContext* deviceContext)
{
	pImpl->m_pEffectHelper->SetSamplerStateByName("g_Sam", RenderStates::SSLinearWrap.Get());
	pImpl->m_pEffectHelper->SetSamplerStateByName("g_SamShadow", RenderStates::SSShadow.Get());
	std::string passname = {};
	
	if (pImpl->m_meshType == StaticMesh)
	{
		deviceContext->IASetInputLayout(pImpl->m_pVertexLayout.Get());
		passname += "Vertex";
	}
	else
	{
		deviceContext->IASetInputLayout(pImpl->m_pBoneLayout.Get());
		passname += "Bones";
	}

	if (pImpl->m_UseHeightMap)
	{
		passname += "HeightMap";
		deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
	}
	else
	{
		passname += "Basic";
		deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	}

	
	if (pImpl->m_UsePBR)
	{
		passname += "PBR";
	}

	else if (pImpl->m_isMatCap)
	{
		passname += "MatCap";
	}
	if (pImpl->m_renderType == RenderType::RenderObject)
	{
		passname += "Object";
	}
	else
	{
		passname += "Instancing";
	}
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass(passname.c_str());
	if (pImpl->m_isTransparent)
	{
		pImpl->m_pCurrEffectPass->SetBlendState(RenderStates::BSTransparent.Get(), nullptr, 0xFFFFFFFF);
		pImpl->m_pCurrEffectPass->SetRasterizerState(pImpl->m_fillMode == Solid ? RenderStates::RSNoCull.Get() : RenderStates::RSWireframe.Get());
		pImpl->m_pCurrEffectPass->SetDepthStencilState(RenderStates::DSSNoDepthWrite.Get(), 0);
	}
	else
	{
		pImpl->m_pCurrEffectPass->SetBlendState(nullptr, nullptr, 0xFFFFFFFF);
		pImpl->m_pCurrEffectPass->SetRasterizerState(pImpl->m_fillMode == Solid ? nullptr : RenderStates::RSWireframe.Get());
		pImpl->m_pCurrEffectPass->SetDepthStencilState(pImpl->m_useSSAO ? RenderStates::DSSEqual.Get() : nullptr, 0);
	}
}

void BasicEffect::SetRenderMatCap(ID3D11DeviceContext* deviceContext)
{
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("BasicMatCap");
}

void BasicEffect::SetTextureHeight(ID3D11ShaderResourceView* textureHeight)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_HeightMap", textureHeight);
	pImpl->m_UseHeightMap = (textureHeight != nullptr);
}

void BasicEffect::SetRenderDefferedPass(ID3D11DeviceContext* deviceContext)
{
	if (pImpl->m_LockPassOnOIT)
		return;
	pImpl->m_DefferedPass = DefferedPass::G_Pass;
}

void BasicEffect::SetRenderDefferedPreL(ID3D11DeviceContext* deviceContext)
{
	if (pImpl->m_LockPassOnOIT)
		return;
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosTexLayout.Get());
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("DeferredPass2");
}

void BasicEffect::SetRenderDefferedDraw(ID3D11DeviceContext* deviceContext)
{
	if (pImpl->m_LockPassOnOIT)
		return;
	pImpl->m_DefferedPass = DefferedPass::Draw;
}

void BasicEffect::SetRenderDefferedOff(ID3D11DeviceContext* deviceContext)
{
	pImpl->m_DefferedPass = DefferedPass::None;
}

void BasicEffect::SetRenderTransparent()
{
	pImpl->m_DefferedPass = DefferedPass::Transparent;
}

void XM_CALLCONV BasicEffect::SetWorldMatrix(DirectX::FXMMATRIX W)
{
	XMStoreFloat4x4(&pImpl->m_World, W);
}

void XM_CALLCONV BasicEffect::SetViewMatrix(FXMMATRIX V)
{
	XMStoreFloat4x4(&pImpl->m_View, V);
}

void XM_CALLCONV BasicEffect::SetProjMatrix(FXMMATRIX P)
{
	XMStoreFloat4x4(&pImpl->m_Proj, P);
}

void XM_CALLCONV BasicEffect::SetTexTransform(DirectX::FXMMATRIX T)
{
	XMMATRIX texT= XMMatrixTranspose(T);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_TexTransform")->SetFloatMatrix(4, 4, (const FLOAT*)&texT);
}

void XM_CALLCONV BasicEffect::SetShadowTransformMatrix(DirectX::FXMMATRIX S)
{
	XMMATRIX shadowTransform = XMMatrixTranspose(S);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_ShadowTransform")->SetFloatMatrix(4, 4, (const FLOAT*)&shadowTransform);
}

void BasicEffect::SetDirLight(size_t pos, const DirectionalLight& dirLight)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_DirLight")->SetRaw(&dirLight, (UINT)pos * sizeof(dirLight), sizeof(dirLight));
}


void BasicEffect::SetPointLight(size_t pos, const PointLight& pointLight)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_PointLight")->SetRaw(&pointLight, (UINT)pos * sizeof(pointLight), sizeof(pointLight));
}

void BasicEffect::SetSpotLight(size_t pos, const SpotLight& spotLight)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_SpotLight")->SetRaw(&spotLight, (UINT)pos * sizeof(spotLight), sizeof(spotLight));
}

void BasicEffect::SetPointLightNumber(unsigned count)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_PointNumber")->SetUInt(count);
}

void BasicEffect::SetDirLightNumber(unsigned count)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_DirNumber")->SetUInt(count);
}

void BasicEffect::SetSpotLightNumber(unsigned count)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_SpotNumber")->SetUInt(count);
}

void BasicEffect::SetIBLStrength(float strength)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_IBLStrength")->SetFloat(strength);
}

void BasicEffect::SetMaterial(const Material& material)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_Material")->SetRaw(&material);
}

void BasicEffect::SetTextureDiffuse(ID3D11ShaderResourceView* texture)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_Tex", texture);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_TextureUsed")->SetSInt((texture != nullptr));
}

void BasicEffect::SetTextureArray(ID3D11ShaderResourceView* textures)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_TexArray", textures);
}

void BasicEffect::SetTextureCube(ID3D11ShaderResourceView* textureCube)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_TexCube", textureCube);
}

void BasicEffect::SetTextureNormal(ID3D11ShaderResourceView* texture)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_NormalTex", texture);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_UseNormalTexture")->SetSInt((texture != nullptr));
}

void BasicEffect::SetTextureShadow(ID3D11ShaderResourceView* texture)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_ShadowMap", texture);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_UseShadow")->SetUInt(texture != nullptr);
}

void BasicEffect::SetTextureSSAO(ID3D11ShaderResourceView* texture)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_SSAOMap", texture);
}

void BasicEffect::SetEnableSSAO(bool enable)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_EnableSSAO")->SetSInt(enable);
	pImpl->m_useSSAO = enable;
}

void BasicEffect::SetEyePos(const DirectX::XMFLOAT3& eyePos)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_EyePosW")->SetFloatVector(3, (FLOAT*)&eyePos);

}

void BasicEffect::SetHeightScale(float scale)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_HeightScale")->SetFloat(scale);
}

void BasicEffect::SetTessInfo(float maxTessDistance, float minTessDistance, float minTessFactor, float maxTessFactor)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_MaxTessDistance")->SetFloat(maxTessDistance);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_MinTessDistance")->SetFloat(minTessDistance);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_MinTessFactor")->SetFloat(minTessFactor);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_MaxTessFactor")->SetFloat(maxTessFactor);
}

void BasicEffect::SetAnimeFrame(float frame)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_AnimeFrame")->SetFloat(frame);
}

void BasicEffect::SetColorBlend(bool blend)
{

	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_ColorBlend")->SetUInt(blend);
}

void BasicEffect::SetColor(const DirectX::XMVECTOR& color)
{

}

void BasicEffect::SetUAVResource(LPCSTR name, ID3D11UnorderedAccessView* pUAVs, UINT initCounts)
{
	pImpl->m_pEffectHelper->SetUnorderedAccessByName(name, pUAVs, initCounts);
}

void BasicEffect::SetOITCBuffer(LPCSTR name, float value)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable(name)->SetFloat(value);
}

void BasicEffect::SetRenderOIT(BOOL isOn)
{
	pImpl->m_LockPassOnOIT = isOn;
	if (isOn)
	{
		pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("OIT");
	}
}

void BasicEffect::SetAmbientTexture(ID3D11ShaderResourceView* texture)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_Ambient", texture);
}

void BasicEffect::SetDiffuseTexture(ID3D11ShaderResourceView* texture)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_Diffuse", texture);
}

void BasicEffect::SetSpecTexture(ID3D11ShaderResourceView* texture)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_Spec", texture);
}

void BasicEffect::SetFogState(bool isOn)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_FogEnabled")->SetUInt(isOn);
}

void BasicEffect::SetFogStart(float fogStart)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_FogStart")->SetFloat(fogStart);
}

void BasicEffect::SetFogColor(DirectX::XMVECTOR fogColor)
{

}

void BasicEffect::SetFogRange(float fogRange)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_FogRange")->SetFloat(fogRange);
}

void BasicEffect::SetReflectionEnabled(bool enabled)
{
}

void BasicEffect::SetBonesEnabled(bool enabled)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_BonesAnimetionEnabled")->SetUInt(enabled);
}

void BasicEffect::SetBonesMaterial(DirectX::XMMATRIX mat[], unsigned num)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_Bones")->SetRaw(mat, 0, sizeof(XMMATRIX) * num);
}

void BasicEffect::SetUsePBR(bool usepbr)
{
	pImpl->m_UsePBR = usepbr;
}

void BasicEffect::SetMetalic(ID3D11ShaderResourceView* texture, float metalic)
{
	if (pImpl->m_UsePBR)
	{
		pImpl->m_pEffectHelper->SetShaderResourceByName("g_MetallicMap", texture);
		pImpl->m_pEffectHelper->GetConstantBufferVariable("g_UseMetallicMap")->SetUInt(texture != nullptr);
		if (!texture)
		{
			pImpl->m_pEffectHelper->GetConstantBufferVariable("g_Metallic")->SetFloat(metalic);
		}
	}
	
	
}

void BasicEffect::SetRoughless(ID3D11ShaderResourceView* texture, float roughless)
{
	if (pImpl->m_UsePBR)
	{
		pImpl->m_pEffectHelper->SetShaderResourceByName("g_MetallicMap", texture);
		pImpl->m_pEffectHelper->GetConstantBufferVariable("g_UseRoughnessMap")->SetUInt(texture != nullptr);
		if (!texture)
		{
			pImpl->m_pEffectHelper->GetConstantBufferVariable("g_Roughness")->SetFloat(roughless);
		}
	}
}

void BasicEffect::SetIBL(ID3D11ShaderResourceView* DiffuseCube, ID3D11ShaderResourceView* SpecCube, ID3D11ShaderResourceView* Lut)
{
	if (DiffuseCube && SpecCube && Lut)
	{
		pImpl->m_pEffectHelper->GetConstantBufferVariable("g_UseIBL")->SetUInt(true);
	}
	else
	{
		pImpl->m_pEffectHelper->GetConstantBufferVariable("g_UseIBL")->SetUInt(false);
	}
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_IBLDeffuse", DiffuseCube);
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_IBLSpec", SpecCube);
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_BRDFLutMap", Lut);
	
}

void BasicEffect::SetUseNormalMap(bool UseMap)
{
	
}



void BasicEffect::Apply(ID3D11DeviceContext* deviceContext)
{

	XMMATRIX W = XMLoadFloat4x4(&pImpl->m_World);
	XMMATRIX V = XMLoadFloat4x4(&pImpl->m_View);
	XMMATRIX P = XMLoadFloat4x4(&pImpl->m_Proj);

	XMMATRIX WVP = W * V * P;
	XMMATRIX WInvT = InverseTranspose(W);
	XMMATRIX VP = V * P;

	WVP = XMMatrixTranspose(WVP);
	WInvT = XMMatrixTranspose(WInvT);
	VP = XMMatrixTranspose(VP);
	W = XMMatrixTranspose(W);
	V = XMMatrixTranspose(V);
	P = XMMatrixTranspose(P);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_World")->SetFloatMatrix(4, 4, (FLOAT*)&W);

	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_View")->SetFloatMatrix(4, 4, (FLOAT*)&V);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_Proj")->SetFloatMatrix(4, 4, (FLOAT*)&P);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_WorldViewProj")->SetFloatMatrix(4, 4, (FLOAT*)&WVP);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_WorldInvTranspose")->SetFloatMatrix(4, 4, (FLOAT*)&WInvT);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_ViewProj")->SetFloatMatrix(4, 4, (FLOAT*)&VP);

	if (pImpl->m_pCurrEffectPass)
		pImpl->m_pCurrEffectPass->Apply(deviceContext);
}