#pragma once
#include "Component.h"
#include "WaveRender.h"
#include <random>
class CWave
{
public:
	CWave();
	void Update(float dt);
	void Draw();
private:
	std::unique_ptr<CpuWavesRender> m_render;
	float m_BaseTime;

	std::mt19937 m_RandEngine;									// 随机数生成器
	std::uniform_int_distribution<UINT> m_RowRange;				// 行索引范围
	std::uniform_int_distribution<UINT> m_ColRange;				// 列索引范围
	std::uniform_real_distribution<float> m_MagnitudeRange;		// 振幅范围
};