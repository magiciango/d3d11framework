#include "CStaticMesh.h"
#include "d3dApp.h"
#include "CModel.h"
#include "WICTextureLoader.h"
#include "DDSTextureLoader.h"
#include "TextureManager.h"
std::unordered_map<const char*, std::unique_ptr<StaticMeshData>> StaticMeshReader::SMLibray;
StaticMeshReader CStaticMesh::m_MeshReader;
size_t CStaticMesh::tag = typeid(CStaticMesh).hash_code();

StaticMeshData* StaticMeshReader::Load(const char* filename)
{
	if (SMLibray.find(filename) == SMLibray.end())
	{
		
		//不想用移动构造器
		SMLibray.insert(std::make_pair(filename, std::make_unique<StaticMeshData>()));
		StaticMeshData* pStaticMesh = SMLibray[filename].get();

		pStaticMesh->m_AiScene = aiImportFile(filename, aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_ConvertToLeftHanded);
		assert(pStaticMesh->m_AiScene);
		pStaticMesh->m_VertexBuffer = new ComPtr<ID3D11Buffer>[pStaticMesh->m_AiScene->mNumMeshes];
		pStaticMesh->m_IndexBuffer = new ComPtr<ID3D11Buffer>[pStaticMesh->m_AiScene->mNumMeshes];

		std::vector<MeshEntry> m_Entries;

		m_Entries.resize(pStaticMesh->m_AiScene->mNumMeshes);
		unsigned VerticesNum = 0;
		unsigned IndicesNum = 0;
		for (int i = 0; i < m_Entries.size(); i++)
		{
			m_Entries[i].MaterialIndex = pStaticMesh->m_AiScene->mMeshes[i]->mMaterialIndex;
			m_Entries[i].IndecesNum = pStaticMesh->m_AiScene->mMeshes[i]->mNumFaces * 3;
			m_Entries[i].BaseVertex = VerticesNum;
			m_Entries[i].BaseIndex = IndicesNum;

			VerticesNum += pStaticMesh->m_AiScene->mMeshes[i]->mNumVertices;
			IndicesNum += m_Entries[i].IndecesNum;
		}
		int num = 0;
		for (unsigned int m = 0; m < pStaticMesh->m_AiScene->mNumMeshes; m++)
		{
			aiMesh* mesh = pStaticMesh->m_AiScene->mMeshes[m];

			VertexPosNormalTangentTex* vertex = new VertexPosNormalTangentTex[mesh->mNumVertices];
			//頂点バッファ生成
			for (unsigned int v = 0; v < mesh->mNumVertices; v++)
			{
				vertex[v].pos = XMFLOAT3(mesh->mVertices[v].x, mesh->mVertices[v].y, mesh->mVertices[v].z);
				vertex[v].normal = XMFLOAT3(mesh->mNormals[v].x, mesh->mNormals[v].y, mesh->mNormals[v].z);
				vertex[v].tangent = XMFLOAT4(mesh->mTangents[v].x, mesh->mTangents[v].y, mesh->mTangents[v].z, 1);
				vertex[v].tex = XMFLOAT2(mesh->mTextureCoords[0][v].x, mesh->mTextureCoords[0][v].y);
				
				pStaticMesh->vMax = XMVectorMax(pStaticMesh->vMax, XMLoadFloat3(&vertex[v].pos));
				pStaticMesh->vMin = XMVectorMin(pStaticMesh->vMin, XMLoadFloat3(&vertex[v].pos));
				num++;
			}

			D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));
			bd.Usage = D3D11_USAGE_DYNAMIC;
			bd.ByteWidth = sizeof(VertexPosNormalTangentTex) * mesh->mNumVertices;
			bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

			D3D11_SUBRESOURCE_DATA sd;
			ZeroMemory(&sd, sizeof(sd));
			sd.pSysMem = vertex;

			D3DAppGet::pDevice()->CreateBuffer(&bd, &sd, pStaticMesh->m_VertexBuffer[m].GetAddressOf());
			delete[] vertex;
			// 
			//インテックスバッファ生成
			unsigned int* index = new unsigned int[mesh->mNumFaces * 3];

			for (unsigned int f = 0; f < mesh->mNumFaces; f++)
			{
				const aiFace* face = &mesh->mFaces[f];

				assert(face->mNumIndices == 3);

				index[f * 3 + 0] = face->mIndices[0];
				index[f * 3 + 1] = face->mIndices[1];
				index[f * 3 + 2] = face->mIndices[2];
			}

			//D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));
			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.ByteWidth = sizeof(unsigned int) * mesh->mNumFaces * 3;
			bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
			bd.CPUAccessFlags = 0;

			//D3D11_SUBRESOURCE_DATA sd;
			ZeroMemory(&sd, sizeof(sd));
			sd.pSysMem = index;

			D3DAppGet::pDevice()->CreateBuffer(&bd, &sd, pStaticMesh->m_IndexBuffer[m].GetAddressOf());

			delete[] index;
		
			for (unsigned int m = 0; m < pStaticMesh->m_AiScene->mNumMaterials; m++)
			{
				aiString path;
				auto LoadTexture = [&](aiTextureType type)
				{
					if (pStaticMesh->m_AiScene->mMaterials[m]->GetTexture(type, 0, &path) == AI_SUCCESS)
					{
						if (path.data[0] == '*')
						{
							//FBXファイル内から読み込み
							if (pStaticMesh->textures[path.data] == NULL)
							{
								ID3D11ShaderResourceView* texture;
								int id = atoi(&path.data[1]);

								CreateWICTextureFromMemory(D3DAppGet::pDevice(),
									(const uint8_t*)pStaticMesh->m_AiScene->mTextures[id]->pcData,
									pStaticMesh->m_AiScene->mTextures[id]->mWidth,
									NULL,
									&texture,
									NULL);

								pStaticMesh->textures[path.data] = texture;
							}
						}
						else
						{
							//外部ファイルから読み込み
							ID3D11ShaderResourceView* texture;
							const char* i = strrchr(filename, '\\');
							int num = std::strlen(filename) - std::strlen(i);
							char texFile[50] = {};
							strncpy(texFile, filename, num);
							char* texturefilename;
							strcat(texFile, "\\");
							strcat(texFile, path.data);
							wchar_t texFile_w[50] = {};
							size_t size = sizeof(texFile);
							mbstowcs(texFile_w, texFile, sizeof(texFile));
							pStaticMesh->textures[path.data] = TextureManager::Load(texFile_w);
						}
					}
					/*else
					{
						pStaticMesh->textures[path.data] = NULL;
					}*/
				};

				LoadTexture(aiTextureType_DIFFUSE);
				LoadTexture(aiTextureType_NORMALS);
				LoadTexture(aiTextureType_HEIGHT);
			}
			/*D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));
			bd.Usage = D3D11_USAGE_DYNAMIC;
			bd.ByteWidth = sizeof(Bone_Vertex) * mesh->mNumVertices;
			bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

			D3D11_SUBRESOURCE_DATA sd;
			ZeroMemory(&sd, sizeof(sd));
			sd.pSysMem = m_DeformVertex;

			Get::pDevice()->CreateBuffer(&bd, &sd, &m_BonesBuffer[m]);*/

		}
		BoundingBox::CreateFromPoints(pStaticMesh->boundingBox, pStaticMesh->vMin, pStaticMesh->vMax);
		return pStaticMesh;
	}
	else
	{
		return SMLibray[filename].get();
	}
	return nullptr;
}


bool CStaticMesh::FrustumCulling(DirectX::XMMATRIX world, DirectX::XMMATRIX view, DirectX::XMMATRIX proj)
{
	BoundingFrustum frustum;
	BoundingFrustum::CreateFromMatrix(frustum, proj);

	BoundingBox box;
	m_pData->boundingBox.Transform(box, world * view);
	return !frustum.Intersects(box);
}

CStaticMesh::CStaticMesh()
{
	m_tag = CStaticMesh::tag;
}

StaticMeshData* CStaticMesh::GetData()
{
	return m_pData;
}

void CStaticMesh::Draw(IEffect* effect, const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& view, const DirectX::XMMATRIX& proj)
{
	if (FrustumCulling(worldMatrix, view, proj))
	{
		return;
	}
	//プリミティブトイポロジ設定
	auto pContext = D3DAppGet::pContext();
	IEffectTransform* effectTransform = dynamic_cast<IEffectTransform*>(effect);
	IEffectTextureDiffuse* effectDiffuse = dynamic_cast<IEffectTextureDiffuse*>(effect);
	IEffectTextureNormal* effectNormal = dynamic_cast<IEffectTextureNormal*>(effect);
	IEffectTextureHeight* effectHeight = dynamic_cast<IEffectTextureHeight*>(effect);
	BasicEffect* effectBasic = dynamic_cast<BasicEffect*>(effect);
	if (effectBasic)
	{
		effectBasic->SetMaterial(m_Material);
		effectBasic->SetMetalic(m_UseTextureType != TextureType::AllModelTexture ? m_pData->textures["Metal"] : nullptr, m_Material.ambient.x);
		effectBasic->SetRoughless(m_UseTextureType != TextureType::AllModelTexture ? m_pData->textures["Rough"] : nullptr, m_Material.ambient.y);
		effectBasic->SetMeshType(IEffectMesh::StaticMesh);
	}
	for (unsigned int m = 0; m < m_pData->m_AiScene->mNumMeshes; m++)
	{
		aiMesh* mesh = m_pData->m_AiScene->mMeshes[m];

		aiMaterial* material = m_pData->m_AiScene->mMaterials[mesh->mMaterialIndex];

		//テクスチャ設定
		aiString path;
		if (effectDiffuse)
		{
			if (m_UseTextureType== TextureType::AllFileTexture)
			{
				effectDiffuse->SetTextureDiffuse(m_pData->textures["Albedo"]);
			}
			else
			{
				material->GetTexture(aiTextureType_DIFFUSE, 0, &path);
				effectDiffuse->SetTextureDiffuse(m_pData->textures[path.data]);
			}
		}
		if (effectNormal)
		{
			if (m_UseTextureType == TextureType::AllFileTexture)
			{
				effectNormal->SetTextureNormal(m_pData->textures["Normal"]);
			}
			else
			{
				material->GetTexture(aiTextureType_NORMALS, 0, &path);
				effectNormal->SetTextureNormal(m_pData->textures[path.data]);
			}
		}
		if (effectHeight)
		{
			effectHeight->SetTextureHeight(nullptr);
		}
		//頂点バッファ設定

		UINT stride = sizeof(VertexPosNormalTangentTex);
		UINT offset = 0;
		pContext->IASetVertexBuffers(0, 1,
			m_pData->m_VertexBuffer[m].GetAddressOf(), &stride, &offset);

		//pContext->IASetInputLayout(BoneVertexPosNormalTex::inputLayout())

		//インテックスバッファ設定
		pContext->IASetIndexBuffer(
			m_pData->m_IndexBuffer[m].Get(), DXGI_FORMAT_R32_UINT, 0);
		//if (Get::InputState().GetKeyDown(Keyboard::Space))
		effectTransform->SetWorldMatrix(worldMatrix);
		effect->SetRenderPass(D3DAppGet::pContext());
		effect->Apply(D3DAppGet::pContext());
		//ポリコン描画
		pContext->DrawIndexed(
			mesh->mNumFaces * 3, 0, 0);
	}
}

void CStaticMesh::Load(const char* filename)
{
	m_pData = m_MeshReader.Load(filename);
}

void CStaticMesh::SetMaterial(Material material)
{
	m_Material = material;
}

void CStaticMesh::SetMatCapTexture(ID3D11ShaderResourceView* texture)
{
	m_MatCapTexture = texture;
}

void CStaticMesh::SetUseTextureType(TextureType use)
{
	m_UseTextureType = use;
}

void CStaticMesh::SetPBRMap(ID3D11ShaderResourceView* albedo, ID3D11ShaderResourceView* normal, ID3D11ShaderResourceView* height, ID3D11ShaderResourceView* metal, ID3D11ShaderResourceView* rough)
{
	m_pData->textures["Albedo"] = albedo;
	m_pData->textures["Normal"] = normal;
	m_pData->textures["Height"] = height;
	m_pData->textures["Metal"] = metal;
	m_pData->textures["Rough"] = rough;
}
