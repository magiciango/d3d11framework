#include "CModel.h"

#include "d3dApp.h"
#include "d3dUtil.h"
using namespace DirectX;
void AnimationModel::Draw(DirectX::XMMATRIX world)
{
	//プリミティブトイポロジ設定
	auto pContext = D3DAppGet::pContext();
	pContext->IASetPrimitiveTopology(
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//マテリアル設定
	Material material;
	ZeroMemory(&material, sizeof(material));
	material.diffuse = XMFLOAT4(0.8, 0.8, 0.8, 1);
	material.ambient = XMFLOAT4(0.8, 0.8, 0.8, 1);
	material.specular = XMFLOAT4(0.1, 0.1, 0.1, 16);

	auto& effect = BasicEffect::Get();
	effect.SetRenderBones(pContext);
	effect.SetMaterial(material);
	for (unsigned int m = 0; m < m_AiScene->mNumMeshes; m++)
	{
		aiMesh* mesh = m_AiScene->mMeshes[m];

		aiMaterial* material = m_AiScene->mMaterials[mesh->mMaterialIndex];

		//テクスチャ設定
		aiString path;
		material->GetTexture(aiTextureType_DIFFUSE, 0, &path);
		effect.SetTextureDiffuse(m_Texture[path.data]);
		pContext->PSSetShaderResources(0, 1,
			&m_Texture[path.data]);

		//頂点バッファ設定
		
		UINT stride = sizeof(BoneVertexPosNormalTex);
		UINT offset = 0;
		pContext->IASetVertexBuffers(0, 1,
			m_VertexBuffer[m].GetAddressOf(), &stride, &offset);
		
		//pContext->IASetInputLayout(BoneVertexPosNormalTex::inputLayout())

		//インテックスバッファ設定
		pContext->IASetIndexBuffer(
			m_IndexBuffer[m].Get(), DXGI_FORMAT_R32_UINT, 0);
		//if (Get::InputState().GetKeyDown(Keyboard::Space))
		BasicEffect::Get().SetBonesEnabled(true);
		BasicEffect::Get().SetBonesMaterial(m_BoneMat, 120);
		BasicEffect::Get().SetWorldMatrix(world);
		BasicEffect::Get().SetTextureDiffuse(m_Texture[path.data]);
		BasicEffect::Get().Apply(D3DAppGet::pContext());
		//ポリコン描画
		pContext->DrawIndexed(
			mesh->mNumFaces * 3, 0, 0);
		//BasicEffect::Get().SetRenderDefault(pContext);
	}
}
void AnimationModel::Load(const char* FileName)
{
	const std::string modelPath(FileName);

	m_AiScene = aiImportFile(FileName, aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_ConvertToLeftHanded);
	assert(m_AiScene);

	m_VertexBuffer = new ComPtr<ID3D11Buffer> [m_AiScene->mNumMeshes];
	m_IndexBuffer = new ComPtr<ID3D11Buffer>[m_AiScene->mNumMeshes];
	//メッシュ情報格納
	m_Entries.resize(m_AiScene->mNumMeshes);
	unsigned VerticesNum = 0;
	unsigned IndicesNum = 0;
	for (int i = 0; i < m_Entries.size(); i++)
	{
		m_Entries[i].MaterialIndex = m_AiScene->mMeshes[i]->mMaterialIndex;
		m_Entries[i].IndecesNum = m_AiScene->mMeshes[i]->mNumFaces * 3;
		m_Entries[i].BaseVertex = VerticesNum;
		m_Entries[i].BaseIndex = IndicesNum;

		VerticesNum += m_AiScene->mMeshes[i]->mNumVertices;
		IndicesNum += m_Entries[i].IndecesNum;
	}

	//m_DeformVertex.resize(VerticesNum);

	for (unsigned int m = 0; m < m_AiScene->mNumMeshes; m++)
	{
		aiMesh* mesh = m_AiScene->mMeshes[m];

		vertex = new BoneVertexPosNormalTex[mesh->mNumVertices];
		//頂点バッファ生成
		{
			for (unsigned int v = 0; v < mesh->mNumVertices; v++)
			{
				vertex[v].pos = XMFLOAT3(mesh->mVertices[v].x, mesh->mVertices[v].y, mesh->mVertices[v].z);
				vertex[v].normal = XMFLOAT3(mesh->mNormals[v].x, mesh->mNormals[v].y, mesh->mNormals[v].z);
				vertex[v].tex = XMFLOAT2(mesh->mTextureCoords[0][v].x, mesh->mTextureCoords[0][v].y);
			}
		}

		//ボーンデータ初期化
		for (unsigned int b = 0; b < mesh->mNumBones; b++)
		{
			aiBone* bone = mesh->mBones[b];
			unsigned BoneIndex = 0;
			std::string BoneName(bone->mName.data);
			if (m_BoneMapping.find(BoneName) == m_BoneMapping.end())
			{
				BoneIndex = m_BoneNum;
				m_BoneNum++;
				BONE info;
				m_Bone.push_back(info);
			}
			else
			{
				BoneIndex = m_BoneMapping[BoneName];
			}
			m_BoneMapping[BoneName] = BoneIndex;
			for (int j = 0; j < mesh->mBones[b]->mNumWeights; j++)
			{
				unsigned VertexID = /*m_Entries[m].BaseVertex + */mesh->mBones[b]->mWeights[j].mVertexId;
				float weight = mesh->mBones[b]->mWeights[j].mWeight;
				vertex[VertexID].AddBoneData(BoneIndex, weight);
			}
			m_Bone[BoneIndex].OffsetMatrix = bone->mOffsetMatrix;
		}

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = sizeof(BoneVertexPosNormalTex) * mesh->mNumVertices;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		D3D11_SUBRESOURCE_DATA sd;
		ZeroMemory(&sd, sizeof(sd));
		sd.pSysMem = vertex;

		D3DAppGet::pDevice()->CreateBuffer(&bd, &sd, m_VertexBuffer[m].GetAddressOf());
		//delete[] vertex;
		// 
		//インテックスバッファ生成
		{
			unsigned int* index = new unsigned int[mesh->mNumFaces * 3];

			for (unsigned int f = 0; f < mesh->mNumFaces; f++)
			{
				const aiFace* face = &mesh->mFaces[f];

				assert(face->mNumIndices == 3);

				index[f * 3 + 0] = face->mIndices[0];
				index[f * 3 + 1] = face->mIndices[1];
				index[f * 3 + 2] = face->mIndices[2];
			}

			D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));
			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.ByteWidth = sizeof(unsigned int) * mesh->mNumFaces * 3;
			bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
			bd.CPUAccessFlags = 0;

			D3D11_SUBRESOURCE_DATA sd;
			ZeroMemory(&sd, sizeof(sd));
			sd.pSysMem = index;

			D3DAppGet::pDevice()->CreateBuffer(&bd, &sd, m_IndexBuffer[m].GetAddressOf());

			delete[] index;
		}


		
		/*D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = sizeof(Bone_Vertex) * mesh->mNumVertices;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		D3D11_SUBRESOURCE_DATA sd;
		ZeroMemory(&sd, sizeof(sd));
		sd.pSysMem = m_DeformVertex;

		Get::pDevice()->CreateBuffer(&bd, &sd, &m_BonesBuffer[m]);*/

		
	}



	//テクスチャ読み込み
	{
		for (unsigned int m = 0; m < m_AiScene->mNumMaterials; m++)
		{
			aiString path;

			if (m_AiScene->mMaterials[m]->GetTexture(aiTextureType_DIFFUSE, 0, &path) == AI_SUCCESS)
			{
				if (path.data[0] == '*')
				{
					//FBXファイル内から読み込み
					if (m_Texture[path.data] == NULL)
					{
						ID3D11ShaderResourceView* texture;
						int id = atoi(&path.data[1]);
						
						CreateWICTextureFromMemory(D3DAppGet::pDevice(),
							(const uint8_t*)m_AiScene->mTextures[id]->pcData,
							m_AiScene->mTextures[id]->mWidth,
							NULL,
							&texture,
							NULL);

						m_Texture[path.data] = texture;
					}
				}
				else
				{
					ID3D11ShaderResourceView* texture;
					CreateWICTextureFromFile(D3DAppGet::pDevice(), L"..\\Model\\house.png", nullptr, &texture);
					m_Texture[path.data] = texture;
					//外部ファイルから読み込み
				}
			}
			else
			{
				m_Texture[path.data] = NULL;
			}
		}
	}
}



void AnimationModel::LoadAnimation(const char* FileName, const char* AnimationName)
{

	m_Animation[AnimationName] = aiImportFile(FileName, aiProcess_ConvertToLeftHanded);
	assert(m_Animation[AnimationName]);

}


void AnimationModel::CreateBone(aiNode* node)
{
	/*BONE bone;

	m_BoneMapping[node->mName.C_Str()] = bone;

	for (unsigned int n = 0; n < node->mNumChildren; n++)
	{
		CreateBone(node->mChildren[n]);
	}*/

}


void AnimationModel::Unload()
{
	for (unsigned int m = 0; m < m_AiScene->mNumMeshes; m++)
	{
		m_VertexBuffer[m]->Release();
		m_IndexBuffer[m]->Release();
	}

	delete[] m_VertexBuffer;
	delete[] m_IndexBuffer;

	//delete[] m_DeformVertex;


	for (std::pair<std::string, ID3D11ShaderResourceView*>pair : m_Texture)
	{
		pair.second->Release();
	}



	aiReleaseImport(m_AiScene);


	for (std::pair<const std::string, const aiScene*> pair : m_Animation)
	{
		aiReleaseImport(pair.second);
	}

}





void AnimationModel::Update(const char* AnimationName, int Frame)
{
	if (!m_Animation[AnimationName]->HasAnimations())
		return;

	//アニメーションデータからボーンマトリクス算出
	aiAnimation* animation = m_Animation[AnimationName]->mAnimations[0];


	for (unsigned int c = 0; c < animation->mNumChannels; c++)
	{
		aiNodeAnim* nodeAnim = animation->mChannels[c];
		BONE* bone = &m_Bone[m_BoneMapping[nodeAnim->mNodeName.C_Str()]]; 

		int f;
		f = Frame % nodeAnim->mNumRotationKeys;//簡易実装
		aiQuaternion rot = nodeAnim->mRotationKeys[f].mValue;

		f = Frame % nodeAnim->mNumPositionKeys;//簡易実装
		aiVector3D pos = nodeAnim->mPositionKeys[f].mValue;

		bone->AnimationMatrix = aiMatrix4x4(aiVector3D(1.0f, 1.0f, 1.0f), rot, pos);
	}

	//再帰的にボーンマトリクスを更新
	UpdateBoneMatrix(m_AiScene->mRootNode, aiMatrix4x4());

	for (int i = 0; i < std::min(120, (int)(m_Bone.size())); i++)
	{
		XMFLOAT4 a, b, c, d;
		a = { m_Bone[i].Matrix.a1,m_Bone[i].Matrix.b1,m_Bone[i].Matrix.c1,m_Bone[i].Matrix.d1 };
		b = { m_Bone[i].Matrix.a2,m_Bone[i].Matrix.b2,m_Bone[i].Matrix.c2,m_Bone[i].Matrix.d2 };
		c = { m_Bone[i].Matrix.a3,m_Bone[i].Matrix.b3,m_Bone[i].Matrix.c3,m_Bone[i].Matrix.d3 };
		d = { m_Bone[i].Matrix.a4,m_Bone[i].Matrix.b4,m_Bone[i].Matrix.c4,m_Bone[i].Matrix.d4 };
		m_BoneMat[i] = { XMLoadFloat4(&a), XMLoadFloat4(&b), XMLoadFloat4(&c), XMLoadFloat4(&d) };
	}
	
}
	

void AnimationModel::UpdateBoneMatrix(aiNode* node, aiMatrix4x4 matrix)
{
	BONE* bone = &m_Bone[m_BoneMapping[node->mName.C_Str()]];

	//マトリクスの乗算順番に注意
	aiMatrix4x4 worldMatrix;

	worldMatrix = matrix;
	worldMatrix *= bone->AnimationMatrix;

	bone->Matrix = worldMatrix;
	bone->Matrix *= bone->OffsetMatrix;

	for (unsigned int n = 0; n < node->mNumChildren; n++)
	{
		UpdateBoneMatrix(node->mChildren[n], worldMatrix);
	}
}
