#include "WaveRender.h"
#include "Geometry.h"
#include "d3dUtil.h"
#include "d3dApp.h"
#include "Effects.h"

#pragma warning(disable: 26812)

using namespace DirectX;
using namespace Microsoft::WRL;

void WavesRender::SetMaterial(const Material& material)
{
	m_Material = material;
}

Transform& WavesRender::GetTransform()
{
	return m_Transform;
}

const Transform& WavesRender::GetTransform() const
{
	return m_Transform;
}

UINT WavesRender::RowCount() const
{
	return m_NumRows;
}

UINT WavesRender::ColumnCount() const
{
	return m_NumCols;
}

void WavesRender::Init(UINT rows, UINT cols, float texU, float texV,
	float timeStep, float spatialStep, float waveSpeed, float damping, float flowSpeedX, float flowSpeedY)
{
	m_NumRows = rows;
	m_NumCols = cols;

	m_TexU = texU;
	m_TexV = texV;
	m_TexOffset = XMFLOAT2();

	m_VertexCount = rows * cols;
	m_IndexCount = 6 * (rows - 1) * (cols - 1);

	m_TimeStep = timeStep;
	m_SpatialStep = spatialStep;
	m_FlowSpeedX = flowSpeedX;
	m_FlowSpeedY = flowSpeedY;
	m_AccumulateTime = 0.0f;

	float d = damping * timeStep + 2.0f;
	float e = (waveSpeed * waveSpeed) * (timeStep * timeStep) / (spatialStep * spatialStep);
	m_K1 = (damping * timeStep - 2.0f) / d;
	m_K2 = (4.0f - 8.0f * e) / d;
	m_K3 = (2.0f * e) / d;
}

HRESULT CpuWavesRender::InitResource(ID3D11Device* device, const std::wstring& texFileName,
	UINT rows, UINT cols, float texU, float texV, float timeStep, float spatialStep, float waveSpeed, float damping,
	float flowSpeedX, float flowSpeedY)
{
	// 防止重复初始化造成内存泄漏
	m_pVertexBuffer.Reset();
	m_pIndexBuffer.Reset();
	m_pTextureDiffuse.Reset();

	// 初始化水波数据
	Init(rows, cols, texU, texV, timeStep, spatialStep, waveSpeed, damping, flowSpeedX, flowSpeedY);

	m_isUpdated = false;

	// 顶点行(列)数 - 1 = 网格行(列)数
	auto meshData = Geometry::CreateTerrain<VertexPosNormalTex, DWORD>(XMFLOAT2((cols - 1) * spatialStep, (rows - 1) * spatialStep),
		XMUINT2(cols - 1, rows - 1));

	HRESULT hr;

	// 创建动态顶点缓冲区
	hr = CreateVertexBuffer(device, meshData.vertexVec.data(), (UINT)meshData.vertexVec.size() * sizeof(VertexPosNormalTex),
		m_pVertexBuffer.GetAddressOf(), true);
	if (FAILED(hr))
		return hr;
	// 创建索引缓冲区
	hr = CreateIndexBuffer(device, meshData.indexVec.data(), (UINT)meshData.indexVec.size() * sizeof(DWORD),
		m_pIndexBuffer.GetAddressOf());
	if (FAILED(hr))
		return hr;

	// 取出顶点数据
	m_Vertices.swap(meshData.vertexVec);
	// 保持与顶点数目一致
	m_PrevSolution.resize(m_Vertices.size());
	// 顶点位置复制到Prev
	for (size_t i = 0; i < m_NumRows; ++i)
	{
		for (size_t j = 0; j < m_NumCols; ++j)
		{
			m_PrevSolution[i * (size_t)m_NumCols + j].pos = m_Vertices[i * (size_t)m_NumCols + j].pos;
		}
	}

	// 读取纹理
	if (texFileName.size() > 4)
	{
		if (texFileName.substr(texFileName.size() - 3, 3) == L"dds")
		{
			hr = CreateDDSTextureFromFile(device, texFileName.c_str(), nullptr,
				m_pTextureDiffuse.GetAddressOf());
		}
		else
		{
			hr = CreateWICTextureFromFile(device, texFileName.c_str(), nullptr,
				m_pTextureDiffuse.GetAddressOf());
		}
	}
	return hr;
}

void CpuWavesRender::Update(float dt)
{
	m_AccumulateTime += dt;
	m_TexOffset.x += m_FlowSpeedX * dt;
	m_TexOffset.y += m_FlowSpeedY * dt;

	// 仅仅在累积时间大于时间步长时才更新
	if (m_AccumulateTime > m_TimeStep)
	{
		m_isUpdated = true;
		// 仅仅对内部顶点进行更新
		for (size_t i = 1; i < m_NumRows - 1; ++i)
		{
			for (size_t j = 1; j < m_NumCols - 1; ++j)
			{
				// 在这次更新之后，我们将丢弃掉上一次模拟的数据。
				// 因此我们将运算的结果保存到Prev[i][j]的位置上。
				// 注意我们能够使用这种原址更新是因为Prev[i][j]
				// 的数据仅在当前计算Next[i][j]的时候才用到
				m_PrevSolution[i * m_NumCols + j].pos.y =
					m_K1 * m_PrevSolution[i * m_NumCols + j].pos.y +
					m_K2 * m_Vertices[i * m_NumCols + j].pos.y +
					m_K3 * (m_Vertices[(i + 1) * m_NumCols + j].pos.y +
						m_Vertices[(i - 1) * m_NumCols + j].pos.y +
						m_Vertices[i * m_NumCols + j + 1].pos.y +
						m_Vertices[i * m_NumCols + j - 1].pos.y);
			}
		}

		// 由于把下一次模拟的结果写到了上一次模拟的缓冲区内，
		// 我们需要将下一次模拟的结果与当前模拟的结果交换
		for (size_t i = 1; i < m_NumRows - 1; ++i)
		{
			for (size_t j = 1; j < m_NumCols - 1; ++j)
			{
				std::swap(m_PrevSolution[i * m_NumCols + j].pos, m_Vertices[i * m_NumCols + j].pos);
			}
		}

		m_AccumulateTime = 0.0f;	// 重置时间

		// 使用有限差分法计算法向量
		for (size_t i = 1; i < m_NumRows - 1; ++i)
		{
			for (size_t j = 1; j < m_NumCols - 1; ++j)
			{
				float left = m_Vertices[i * m_NumCols + j - 1].pos.y;
				float right = m_Vertices[i * m_NumCols + j + 1].pos.y;
				float top = m_Vertices[(i - 1) * m_NumCols + j].pos.y;
				float bottom = m_Vertices[(i + 1) * m_NumCols + j].pos.y;
				m_Vertices[i * m_NumCols + j].normal = XMFLOAT3(-right + left, 2.0f * m_SpatialStep, bottom - top);
				XMVECTOR nVec = XMVector3Normalize(XMLoadFloat3(&m_Vertices[i * m_NumCols + j].normal));
				XMStoreFloat3(&m_Vertices[i * m_NumCols + j].normal, nVec);
			}
		}
	}
}

void CpuWavesRender::Disturb(UINT i, UINT j, float magnitude)
{
	// 不要对边界处激起波浪
	assert(i > 1 && i < m_NumRows - 2);
	assert(j > 1 && j < m_NumCols - 2);

	float halfMag = 0.5f * magnitude;

	// 对顶点[i][j]及其相邻顶点修改高度值
	size_t curr = i * (size_t)m_NumCols + j;
	m_Vertices[curr].pos.y += magnitude;
	m_Vertices[curr - 1].pos.y += halfMag;
	m_Vertices[curr + 1].pos.y += halfMag;
	m_Vertices[curr - m_NumCols].pos.y += halfMag;
	m_Vertices[curr + m_NumCols].pos.y += halfMag;

	m_isUpdated = true;
}

void CpuWavesRender::Draw()
{
	auto deviceContext = D3DAppGet::pContext();
	auto& effect = BasicEffect::Get();
	// 更新动态顶点缓冲区的数据
	if (m_isUpdated)
	{
		m_isUpdated = false;
		D3D11_MAPPED_SUBRESOURCE mappedData;
		deviceContext->Map(m_pVertexBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedData);
		memcpy_s(mappedData.pData, m_VertexCount * sizeof(VertexPosNormalTex),
			m_Vertices.data(), m_VertexCount * sizeof(VertexPosNormalTex));
		deviceContext->Unmap(m_pVertexBuffer.Get(), 0);
	}

	UINT strides[1] = { sizeof(VertexPosNormalTex) };
	UINT offsets[1] = { 0 };
	deviceContext->IASetVertexBuffers(0, 1, m_pVertexBuffer.GetAddressOf(), strides, offsets);
	deviceContext->IASetIndexBuffer(m_pIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
	//effect.SetReflectionEnabled(false);
	effect.SetMaterial(m_Material);
	effect.SetTextureDiffuse(m_pTextureDiffuse.Get());
	effect.SetWorldMatrix(m_Transform.GetLocalToWorldMatrixXM());
	effect.SetTexTransform(XMMatrixScaling(m_TexU, m_TexV, 1.0f) * XMMatrixTranslationFromVector(XMLoadFloat2(&m_TexOffset)));
	effect.Apply(deviceContext);
	deviceContext->DrawIndexed(m_IndexCount, 0, 0);

	effect.SetTexTransform(XMMatrixIdentity());
}
