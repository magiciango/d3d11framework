#ifndef SKYRENDER_H
#define SKYRENDER_H

#include <vector>
#include <string>
#include "Effects.h"
#include "Camera.h"
#include "CGeometry.h"
#include "TextureRender.h"

#define IradianceMapSize	64
#define PrefilterMapSize	128
#define LUTMapSize			512
class SkyRender
{
public:
	template<class T>
	using ComPtr = Microsoft::WRL::ComPtr<T>;

	SkyRender() = default;
	~SkyRender() = default;
	// 不允许拷贝，允许移动
	SkyRender(const SkyRender&) = delete;
	SkyRender& operator=(const SkyRender&) = delete;
	SkyRender(SkyRender&&) = default;
	SkyRender& operator=(SkyRender&&) = default;


	// 需要提供完整的天空盒贴图 或者 已经创建好的天空盒纹理.dds文件
	HRESULT InitResource(ID3D11Device* device,
		ID3D11DeviceContext* deviceContext,
		const std::wstring& cubemapFilename,
		float skySphereRadius,		// 天空球半径
		bool generateMips = false);	// 默认不为静态天空盒生成mipmaps

	// 需要提供天空盒的六张正方形贴图
	HRESULT InitResource(ID3D11Device* device,
		ID3D11DeviceContext* deviceContext,
		const std::vector<std::wstring>& cubemapFilenames,
		float skySphereRadius,		// 天空球半径
		bool generateMips = false);	// 默认不为静态天空盒生成mipmaps

	ID3D11ShaderResourceView* GetTextureCube();
	ID3D11ShaderResourceView* GetDiffuseCube();
	ID3D11ShaderResourceView* GetSpecCube();
	ID3D11ShaderResourceView* GetLutTexture();

	void Update(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, Camera* camera);

	void Draw(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, Camera* camera);
	void DrawSkyBox(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, Camera* camera, ID3D11ShaderResourceView* textureCube);
	// 设置调试对象名
	void SetDebugObjectName(const std::string& name);

protected:
	HRESULT InitResource(ID3D11Device* device, float skySphereRadius);
	
	// 缓存当前渲染目标视图
	void Cache(ID3D11DeviceContext* deviceContext);
	// 指定天空盒某一面开始绘制，需要先调用Cache方法
	void BeginCapture(ID3D11DeviceContext* deviceContext, SkyEffect& effect, const DirectX::XMFLOAT3& pos,
		D3D11_TEXTURECUBE_FACE face, float nearZ = 1e-3f, float farZ = 1e3f);
	void IBLStuff(ID3D11Device* device, ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, Camera* camera);
	// 恢复渲染目标视图及摄像机，并绑定当前动态天空盒
	void Restore(ID3D11DeviceContext* deviceContext);
private:
	
public:
	ComPtr<ID3D11Buffer> m_pSphereVertexBuffer;
	ComPtr<ID3D11Buffer> m_pSphereIndexBuffer;
	UINT m_SphereIndexCount = 0;

	ComPtr<ID3D11Buffer> m_pQuadVertexBuffer;
	ComPtr<ID3D11Buffer> m_pQuadIndexBuffer;
	UINT m_QuadIndexCount = 0;
	

	ComPtr<ID3D11ShaderResourceView> m_pTextureHDRSRV;

	//IBL
	ComPtr<ID3D11RenderTargetView>		m_pCacheRTV;		        // 临时缓存的后备缓冲区
	ComPtr<ID3D11DepthStencilView>		m_pCacheDSV;		        // 临时缓存的深度/模板缓冲区

	std::unique_ptr<FirstPersonCamera>					m_pCamera;				    // 捕获当前天空盒其中一面的摄像机

	ComPtr<ID3D11ShaderResourceView>	m_pDiffuseMapSRV;		// 动态天空盒对应的着色器资源视图
	ComPtr<ID3D11RenderTargetView>		m_pDiffuseMapRTVs[6];	// 动态天空盒每个面对应的渲染目标视图

	ComPtr<ID3D11ShaderResourceView>	m_pPrefilterSRV;		// 动态天空盒对应的着色器资源视图
	ComPtr<ID3D11RenderTargetView>		m_pPrefilterRTVs[30];	// 动态天空盒每个面对应的渲染目标视图
	unsigned MipLevels = 5;
	std::unique_ptr<TextureRender> m_pBRDFLutRender;

	bool m_GenerateIBL = false;
	//CGeometry CubeMesh;
};


class DynamicSkyRender :public SkyRender
{
    template<class T>
    using ComPtr = Microsoft::WRL::ComPtr<T>;

    /*DynamicSkyRender() = default;
    ~DynamicSkyRender() = default;*/
    // 不允许拷贝，允许移动
   /* DynamicSkyRender(const DynamicSkyRender&) = delete;
    DynamicSkyRender& operator=(const DynamicSkyRender&) = delete;*/
    /*DynamicSkyRender(DynamicSkyRender&&) = default;
    DynamicSkyRender& operator=(DynamicSkyRender&&) = default;*/
public:
    //std::map<wchar_t, float> m_ParameterMap;
    void Init();
    void Update(float dt);
    void Draw(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, Camera* camera);
    void DrawDynamicSkyBox(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, Camera* camera);
    DirectionalLight GetSunLight();
private:
    HRESULT InitDynamicResource(ID3D11Device* device, float skySphereRadius);
    //id 0-5:DiffuseMap  6-35:SpecMapMipMap
    void IBLCreateTexCube(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, int id);
    void IBLStepDiffuse(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, int id, bool isAPass);
    void IBLStepSpecular(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, int id, bool isAPass);
    // 缓存当前渲染目标视图
    //void Cache(ID3D11DeviceContext* deviceContext);
    //// 指定天空盒某一面开始绘制，需要先调用Cache方法
    //void BeginCapture(ID3D11DeviceContext* deviceContext, SkyEffect& effect, const DirectX::XMFLOAT3& pos,
    //    D3D11_TEXTURECUBE_FACE face, float nearZ = 1e-3f, float farZ = 1e3f);

    //// 恢复渲染目标视图及摄像机，并绑定当前动态天空盒
    //void Restore(ID3D11DeviceContext* deviceContext);

    void BlendIBL(ID3D11DeviceContext* deviceContext);
public:
#pragma region RayMarchingData

    float m_WaveIter;
    float m_NormalIter;
    float m_AuroraIter;
    //normal
    XMFLOAT3 m_CameraPos;
    float m_Time;
    float m_Exposure;
    float m_Gamma;
    //X m_rot;

    //wave
    float m_WaterDepth;
    float m_WaveSpeed;
    float m_WavePhase;
    float m_WaveWeight;

    //atomosphere
    float m_SunIntensity;

    float m_PlantRadius;
    float m_AtomosphereRadius;
    float m_kMie;
    float m_shRlh;

    XMFLOAT3 m_kRlh;
    float m_shMie;

    float m_g;
    //sun
    XMFLOAT3 m_SunPos;
    float m_SunTimeScale;

    float m_SunRadius;
    //stars
    float m_StarsDensity;
    //aurora

    float m_AuroraDensity;

    //DrawState
    bool m_UseAurora;
    bool m_UseSea;

#pragma endregion

    //sun dir light
    DirectionalLight m_SunLight;
    XMFLOAT2 m_NDCSunPos;

    //IBL
    //ComPtr<ID3D11RenderTargetView>		m_pCacheRTV;		        // 临时缓存的后备缓冲区
    //ComPtr<ID3D11DepthStencilView>		m_pCacheDSV;		        // 临时缓存的深度/模板缓冲区

    //std::unique_ptr<FirstPersonCamera>					m_pCamera;				    // 捕获当前天空盒其中一面的摄像机
    //动态天空盒的立方体贴图
    ComPtr<ID3D11ShaderResourceView>	m_pSkyBoxCubeSRV;
    ComPtr<ID3D11RenderTargetView>		m_pSkyBoxCubeRTVs[6];

    ComPtr<ID3D11ShaderResourceView>	m_pDiffuseArraySRV;		// 动态天空盒对应的着色器资源视图
    ComPtr<ID3D11RenderTargetView>		m_pDiffuseArrayRTVs[12];	// 动态天空盒每个面对应的渲染目标视图

    //ComPtr<ID3D11ShaderResourceView>	m_pDiffuseCubeSRV;		// 动态天空盒对应的着色器资源视图
    //ComPtr<ID3D11RenderTargetView>		m_pDiffuseCubeRTVs[6];

    //ComPtr<ID3D11ShaderResourceView>	m_pPrefilterCubeSRV;		// 动态天空盒对应的着色器资源视图
    //ComPtr<ID3D11RenderTargetView>		m_pPrefilterCubeRTVs[30];	// 动态天空盒每个面对应的渲染目标视图

    ComPtr<ID3D11ShaderResourceView>	m_pPrefilterArraySRV;		// 动态天空盒对应的着色器资源视图
    ComPtr<ID3D11RenderTargetView>		m_pPrefilterArrayRTVs[60];



    enum IBLRenderType
    {
        CreateCubeA,
        DiffuseA,
        SpecularA,
        CreateCubeB,
        DiffuseB,
        SpecularB
    };
    float m_IBLTimer;
    float m_IBLLerp = 1;
    bool m_LerpPlus = false;
    IBLRenderType m_IBLRenderType = CreateCubeA;
    int m_IBLID = 0;
    bool UpdateIBL;
    float m_IBLStep = 0.14;
};

#endif
