#pragma once
#include "Keyboard.h"
#include "Mouse.h"
using namespace DirectX;

enum class MouseButton
{
	LeftButton,
	RightButton,
	MiddleButton
};
class Input
{
public:
	void Init(HWND windows);
	void Update();

	bool GetKeyDown(Keyboard::Keys keys);
	bool GetKeyPressed(Keyboard::Keys keys);
	bool GetKeyRelease(Keyboard::Keys keys);
	int GetMouseMoveX();
	int GetMouseMoveY();
	int GetMousePosX();
	int GetMousePosY();
	float GetMouseScroll();
	bool GetButtonDown(MouseButton button);
	bool GetButtonPressed(MouseButton button);
	bool GetButtonRelease(MouseButton button);
	void SetMouseMode(bool isAbsolute, int x = 0, int y = 0);
	void SetMousePos(int x, int y);
	void ResetScroll();
	const Mouse* GetMouse();
	const Keyboard* GetKeyboard();
private:
	std::unique_ptr<DirectX::Mouse> m_pMouse;					// 鼠标
	DirectX::Mouse::ButtonStateTracker m_MouseTracker;			// 鼠标状态追踪器
	std::unique_ptr<DirectX::Keyboard> m_pKeyboard;				// 键盘
	DirectX::Keyboard::KeyboardStateTracker m_KeyboardTracker;	// 键盘状态追踪器
	int move_x = 0;
	int move_y = 0;
	int pos_x = 0;
	int pos_y = 0;
};