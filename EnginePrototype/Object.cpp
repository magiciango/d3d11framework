#include "Object.h"
#include <vector>
Actor::Actor()
	:m_isDestory(false),
	m_Transform{},
	m_Active(true),
	m_Children{},
	m_Parent(nullptr)
{
}

Actor::~Actor()
{
	//ObjList::Instance()->DeleteObj(this);
	if (auto collider = GetComponent<CCollider>())
	{
		CCollider::DeleteCollider(collider);
	}
}

void Actor::AddComponent(Component* component)
{
	m_ComponentMap.insert(std::pair< size_t, std::unique_ptr<Component> >(component->m_tag, std::unique_ptr<Component>(component)));
	component->SetOwner(this);
	//m_CmpList.push_back(component);
}


Transform& Actor::GetTransform()
{
	return m_Transform;
}

const Transform& Actor::GetTransform() const
{
	return m_Transform;
}

void Actor::SetActive(bool active)
{
	m_Active = active;
}

const bool Actor::GetActive() const
{
	return m_Active;
}

void Actor::AddTag(ActorType tag)
{
	for (auto& i : m_ActorTag)
	{
		if (i == tag)
			return;
	}
	m_ActorTag.push_back(tag);
}

bool Actor::FindTag(ActorType tag)
{
	for (auto& i : m_ActorTag)
	{
		if (i == tag)
			return true;
	}
	return false;
}

void Actor::Update(float dt)
{
	for (auto& i : m_ComponentMap)
	{
		i.second->Update(dt);
	}
}

void Actor::Draw(IEffect* effect, const DirectX::XMMATRIX& V, const DirectX::XMMATRIX& P)
{
	for (auto& i : m_ComponentMap)
	{
		i.second->Draw(effect, m_Transform.GetLocalToWorldMatrixXM(), V, P);
	}
}

void Actor::AttachToActor(Actor* parent)
{
	m_Parent = parent;
	Actor* obj = m_Parent;
	XMMATRIX mat = XMMatrixIdentity();// = GetTransform().GetLocalToWorldMatrixXM();
	while (obj != nullptr)
	{
		mat = obj->GetTransform().GetWorldToLocalMatrixXM() * mat;
		obj = obj->m_Parent;
	}
	mat = GetTransform().GetLocalToWorldMatrixXM() * mat;
	XMVECTOR pos, scale, rotQuat;
	XMMatrixDecompose(&scale, &rotQuat, &pos, mat);
	XMFLOAT3 vec;
	XMStoreFloat3(&vec, pos);
	
	m_Transform.SetPosition(vec);
	XMStoreFloat3(&vec, scale);
	m_Transform.SetScale(vec);
	m_Transform.SetRotation(0, 0, 0);
	m_Transform.Rotate(rotQuat);

	m_Parent->AddChild(this);
}

void Actor::AddChild(Actor* child)
{
	m_Children.push_back(child);

	if (child->m_Parent == nullptr)
		child->AttachToActor(this);
}

void Actor::UnAttach()
{
	if (m_Parent)
	{
		for (auto i = m_Parent->m_Children.begin(); i != m_Parent->m_Children.end(); ++i)
		{
			if ((*i) == this)
			{
				XMMATRIX mat = GetParentTransformMatrixXM();
				mat = GetTransform().GetLocalToWorldMatrixXM() * mat;

				XMVECTOR pos, scale, rotQuat;
				XMMatrixDecompose(&scale, &rotQuat, &pos, mat);
				XMFLOAT3 vec;
				XMStoreFloat3(&vec, pos);

				m_Transform.SetPosition(vec);
				XMStoreFloat3(&vec, scale);
				m_Transform.SetScale(vec);
				m_Transform.SetRotation(0, 0, 0);
				m_Transform.Rotate(rotQuat);

				m_Parent->m_Children.erase(i);
				m_Parent = nullptr;
				return;
			}
		}
	}
	
}

Actor* Actor::GetChild(int id)
{
	if (id > m_Children.size())
		return nullptr;
	return m_Children[id];
}

XMMATRIX Actor::GetParentTransformMatrixXM()
{
	Actor* obj = m_Parent;
	XMMATRIX mat = XMMatrixIdentity();
	while (obj != nullptr)
	{
		mat = mat * obj->GetTransform().GetLocalToWorldMatrixXM();
		obj = obj->m_Parent;
	}
	return mat;
}

XMFLOAT4X4 Actor::GetWorldTransformMatrix()
{
	XMFLOAT4X4 mat;
	XMStoreFloat4x4(&mat, GetWorldTransformMatrixXM());
	return mat;
}

XMMATRIX Actor::GetWorldTransformMatrixXM()
{
	XMMATRIX mat = GetTransform().GetLocalToWorldMatrixXM() 
		* GetParentTransformMatrixXM();
	return mat;
}

XMFLOAT3 Actor::GetWorldPosition()
{
	XMFLOAT3 out;
	XMStoreFloat3(&out, XMVector3Transform(GetTransform().GetPositionXM(), GetParentTransformMatrixXM()));
	return out;
}

XMFLOAT3 Actor::GetWorldForwardAxis()
{
	XMFLOAT3 out;
	XMVECTOR vec;
	XMMATRIX mat = GetParentTransformMatrixXM();
	mat.r[3] = { 0.0f,0.0f,0.0f,1.0f };		//删去平移影响
	vec = XMVector3Transform(GetTransform().GetForwardAxisXM(), mat);
	vec = XMVector3Normalize(vec);
	XMStoreFloat3(&out, vec);
	return out;
}

XMFLOAT3 Actor::GetWorldUpAxis()
{
	XMFLOAT3 out;
	XMVECTOR vec;
	XMMATRIX mat = GetParentTransformMatrixXM();
	mat.r[3] = { 0.0f,0.0f,0.0f,1.0f };		//删去平移影响
	vec = XMVector3Transform(GetTransform().GetUpAxisXM(), mat);
	vec = XMVector3Normalize(vec);
	XMStoreFloat3(&out, vec);
	return out;
}

XMFLOAT3 Actor::GetWorldRightAxis()
{
	XMFLOAT3 out;
	XMVECTOR vec;
	XMMATRIX mat = GetParentTransformMatrixXM();
	mat.r[3] = { 0.0f,0.0f,0.0f,1.0f };		//删去平移影响
	vec = XMVector3Transform(GetTransform().GetRightAxisXM(), mat);
	vec = XMVector3Normalize(vec);
	XMStoreFloat3(&out, vec);
	return out;
}
//
//ObjectManager* ObjectManager::self = nullptr;
ObjectManager::ObjectManager()
{
	m_OpaqueObj.clear();
}
//ObjectManager* ObjectManager::Instance()
//{
//	if (self == nullptr)
//		self = new ObjectManager();
//	return self;
//}
void ObjectManager::AddOpaqueObj(Actor* obj)
{
	m_OpaqueObj.push_back(obj);
}
void ObjectManager::AddTransparentObj(Actor* obj)
{
	m_TransparentObj.push_back(obj);
}

void ObjectManager::DeleteObj(Actor* obj)
{
	obj->m_isDestory = true;
}

void ObjectManager::Clear()
{
	m_OpaqueObj.clear();
	m_TransparentObj.clear();
}
