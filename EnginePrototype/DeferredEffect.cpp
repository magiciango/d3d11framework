#include "Effects.h"
#include "d3dUtil.h"
#include "EffectHelper.h"	// 必须晚于Effects.h和d3dUtil.h包含
#include "DXTrace.h"
#include "Vertex.h"
using namespace DirectX;

class DeferredEffect::Impl
{
public:
	// 必须显式指定
	Impl() {}
	~Impl() = default;
public:

	ComPtr<ID3D11InputLayout> m_pVertexPosTexLayout;

	std::unique_ptr<EffectHelper> m_pEffectHelper;

	std::shared_ptr<IEffectPass> m_pCurrEffectPass;
};

namespace
{
	// BasicEffect单例
	static DeferredEffect* g_pInstance = nullptr;
}

DeferredEffect::DeferredEffect()
{
	if (g_pInstance)
		throw std::exception("BasicEffect is a singleton!");
	g_pInstance = this;
	pImpl = std::make_unique<DeferredEffect::Impl>();
}

DeferredEffect::~DeferredEffect()
{
}

DeferredEffect::DeferredEffect(DeferredEffect&& moveFrom) noexcept
{
	pImpl.swap(moveFrom.pImpl);
}

DeferredEffect& DeferredEffect::operator=(DeferredEffect&& moveFrom) noexcept
{
	pImpl.swap(moveFrom.pImpl);
	return *this;
}

DeferredEffect& DeferredEffect::Get()
{
	if (!g_pInstance)
		throw std::exception("BasicEffect needs an instance!");
	return *g_pInstance;
}

bool DeferredEffect::InitAll(ID3D11Device* device)
{
	if (!device)
		return false;

	if (!RenderStates::IsInit())
		throw std::exception("RenderStates need to be initialized first!");

	pImpl->m_pEffectHelper = std::make_unique<EffectHelper>();

	ComPtr<ID3DBlob> blob;

	HR(CreateShaderFromFile(L"HLSL\\ClusterBuild_CS.cso", L"HLSL\\ClusterBuild_CS.hlsl", "CS", "cs_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("ClusterBuild_CS", device, blob.Get()));

	EffectPassDesc passDesc;

	passDesc.nameCS = "ClusterBuild_CS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("ClusterBuild", device, &passDesc));

	return true;
}

void DeferredEffect::SetFrustumData(float nearPlane, float farPlane, float screenWidth, float ScreenHeight, DirectX::FXMMATRIX V, DirectX::FXMMATRIX InvP)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_NearPlane")->SetFloat(nearPlane);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_FarPlane")->SetFloat(farPlane);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_ScreenHeight")->SetFloat(ScreenHeight);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_ScreenWidth")->SetFloat(screenWidth);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_View")->SetFloatMatrix(4, 4, (FLOAT*)&V);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_ProjInv")->SetFloatMatrix(4, 4, (FLOAT*)&InvP);
}

void DeferredEffect::SetTileSizes(DirectX::XMFLOAT4 tilesizes)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_TileSizes")->SetFloatVector(4, (FLOAT*)&tilesizes);
}

void DeferredEffect::SetRenderClusterBuild(ID3D11DeviceContext* deviceContext)
{
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("ClusterBuild");
}

void DeferredEffect::SetUav(ID3D11UnorderedAccessView* uav)
{
	pImpl->m_pEffectHelper->SetUnorderedAccessByName("g_Output", uav, 1);
}

void DeferredEffect::Apply(ID3D11DeviceContext* deviceContext)
{
	pImpl->m_pCurrEffectPass->Apply(deviceContext);
}
