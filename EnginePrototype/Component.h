#pragma once
#include<iostream>

#include "Effects.h"
#include "Geometry.h"
#include "Transform.h"

class Actor;
enum ComponentType
{
	C_Geometry,
	C_NONE
};

class Component
{
public:
	size_t m_tag = 0;
	Component() = default;
	Component(Actor* obj)
		:m_GameObject(obj)
	{};

	Component(Component&& moveFrom) = default;	//智能指针转换，启用移动构造器

	void SetOwner(Actor* obj)
	{
		m_GameObject = obj;
	}
	Actor* GetGameObject()
	{
		return m_GameObject;
	}
	virtual void Update(float dt) {};
	//virtual void Draw(const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& view, const DirectX::XMMATRIX& proj) {};
	virtual void Draw(IEffect* effect, const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& view, const DirectX::XMMATRIX& proj) {};
protected:
	Actor* m_GameObject = nullptr;
};