#pragma once
#include "Component.h"
#include "Camera.h"
#include <DirectXCollision.h>
#include <list>

using namespace DirectX;
enum ColliderLayer
{
	collider_default=0,
	collider_player,
	collider_bullet,
	collider_wall,
	collider_enemy,
	collider_none,
	COLLIDERLAYER_MAX,
};
class CCollider
	:public Component
{
public:
	static size_t tag;
	CCollider(Actor* obj, ColliderLayer layer = collider_default);
	CCollider(ColliderLayer layer = collider_default);
	BoundingBox GetBoundingBox();
	void SetBoundingBox(BoundingBox& box);
	BoundingOrientedBox GetBoundingOrientedBox();
	ColliderLayer GetLayer();
	void SetLayer(ColliderLayer layer);

	bool FrustumCulling(const DirectX::FXMMATRIX View, const DirectX::CXMMATRIX Proj);
private:
	BoundingBox m_LocalBox;		//只记录形状，仅在Get中更新位置信息
	ColliderLayer m_ColliderLayer;

	//static manager
public:
	static void AddCollider(CCollider* collider);
	static void DeleteCollider(CCollider* collider);
	/*碰撞处理
	* collider 需要处理的collider
	* usingLayer 能够碰撞的碰撞体layer
	* ppOutObj 返回“首个”被碰撞物体
	*/
	static ContainmentType Contains(CCollider& collider,
		std::initializer_list<ColliderLayer> usingLayer,
		Actor** ppOutObj = nullptr);
	static void SetDegree(bool AABB = true);
	static std::list<CCollider*>& GetColliderList(ColliderLayer usingLayer);

private:
	static std::vector<std::list<CCollider*>> m_pColliderList;	//八叉树？
	static bool m_UseAABB;
};


class CRay
	:public Component
{
public:
	CRay();
	CRay(const DirectX::XMFLOAT3& origin, const DirectX::XMFLOAT3& direction);
	bool Hit(const DirectX::BoundingBox& box, float* pOutDist = nullptr, float maxDist = FLT_MAX);
	bool Hit(const DirectX::BoundingOrientedBox& box, float* pOutDist = nullptr, float maxDist = FLT_MAX);
	bool Hit(const DirectX::BoundingSphere& sphere, float* pOutDist = nullptr, float maxDist = FLT_MAX);
	bool Hit(std::initializer_list<ColliderLayer> usingLayer, float* pOutDist = nullptr, Actor** ppOutObj=nullptr,float maxDist = FLT_MAX);
	void SetOrigin(const DirectX::XMFLOAT3& origin);
	void SetDirection(const DirectX::XMFLOAT3& direction);
	void SetRay(const DirectX::XMFLOAT3& origin, const DirectX::XMFLOAT3& direction);
	static CRay ScreenToRay(const Camera& camera, float screenX, float screenY);
public:
	DirectX::XMFLOAT3 m_Origin;		// 射线原点
	DirectX::XMFLOAT3 m_Direction;	// 单位方向向量
};