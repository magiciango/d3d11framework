#include "SGame.h"
bool SGame::m_Over = false;
int SGame::m_Score = 0;
Actor* SGame::m_pPlayer = nullptr;
int SGame::UpdateScene(float dt)
{
	static int ra = 10;
	Input& input = D3DAppGet::InputState();
	auto cam3rd = std::dynamic_pointer_cast<ThirdPersonCamera>(m_pCamera);
	cam3rd->SetTarget(m_pPlayer->GetTransform().GetPosition());
	//cam3rd->SetRotationX(m_pPlayer->GetTransform().GetRotation().x+1);
	//cam3rd->SetRotationY(m_pPlayer->GetTransform().GetRotation().y);
	cam3rd->RotateX(input.GetMouseMoveY() * dt * 1.25f);
	cam3rd->RotateY(input.GetMouseMoveX() * dt * 1.25f);
	cam3rd->Approach(-input.GetMouseScroll() / 120.0f * 1.0f);

	m_gameTime += dt;

	//Scene::UpdateScene(dt);
	
	if (m_gameTime >= ra)
	{
		CreateEnemyA({ (float)(rand() % 200 - 100),(float)(rand() % 200 - 100),0 }, { 0,0,0 }, { 1,1,1 });
		m_gameTime = 0;
		if (ra > 1)
			ra -= 0.1;
	}

	if (m_Over)
	{
		m_Over = false;
		m_Score = 0;
		m_BGMAudio.GetComponent<CAudio>()->Uninit();
		return 2;
	}
	return -1;
}

void SGame::DrawScene(bool isOpaquePass)
{
	auto& basicEffect = BasicEffect::Get();
	//basicEffect.SetRenderDefault(D3DAppGet::pContext());
	basicEffect.SetAnimeFrame(-1);
	basicEffect.SetViewMatrix(m_pCamera->GetViewXM());
	basicEffect.SetEyePos(m_pCamera->GetPosition());

	//ObjectManager::Instance()->Draw();

	SkyEffect::Get().SetRenderDefault(D3DAppGet::pContext());
	m_SkyBox.Draw(*m_pCamera);


	ComPtr<ID2D1RenderTarget> renderTraget = D3DAppGet::AllContexts().pd2dRenderTarget;
	if (renderTraget != nullptr)
	{
		renderTraget->BeginDraw();
		std::wstring text = L"SCORE:";
		text += std::to_wstring(m_Score);
		
		renderTraget->DrawTextW(text.c_str(), (UINT32)text.length(), m_pTextFormat.Get(),
			D2D1_RECT_F{ 0,0,1000,200 }, m_pColorBrush.Get());
		//renderTraget->DrawRoundedRectangle(D2D1_ROUNDED_RECT{ 0,0,1000,200 }, m_pColorBrush.Get());
		float hp = ((IParts*)m_pPlayer)->HP;
		m_pColorBrush.Get()->SetColor(D2D1_COLOR_F{ 1.0f - (float)hp / 200.0f,(float)hp / 200.0f,0,1 });
		renderTraget->DrawRectangle(D2D1_RECT_F{ 10,50,hp*3,55 }, m_pColorBrush.Get(), 20);
		HR(renderTraget->EndDraw());
	}
}

bool SGame::InitResource()
{
	// ******************
	//ゲームオブジェクト
	//
	//ObjectManager::Instance()->Clear();

	auto player = new ACore(true);
	m_pPlayer = player;
	auto shooter = new AShooter({ 2,0,0 });
	shooter->AttachToActor(player);
	shooter->GetComponent<CShoot>()->SetShootType(ShootType::shoot_tri);
	shooter = new AShooter({ -2,0,0 });
	shooter->AttachToActor(player);
	shooter->GetComponent<CShoot>()->SetShootType(ShootType::shoot_tri);

	CreateEnemyA({ 5,0,0 }, { 0,0,0 }, { 1,1,1 });
	

	//auto field = new AField();

	m_SkyBox.InitResource(L"..\\Texture\\sky_box.png",5);

	// ******************
	// BGM用コンポーネントを追加
	//
	auto audio = new CAudio(&m_BGMAudio);
	audio->Load(L"..\\Asset\\Audio\\StageBgm.wav");
	m_BGMAudio.AddComponent(audio);
	audio->Play(true);
	

	// ******************
	// カメラ
	//
	auto camera = std::shared_ptr<ThirdPersonCamera>(new ThirdPersonCamera);
	m_pCamera = camera;
	camera->SetViewPort(0.0f, 0.0f, (float)D3DAppGet::Width(), (float)D3DAppGet::Height());
	camera->SetDistance(5.0f);
	camera->SetDistanceMinMax(6.0f, 30.0f);
	camera->SetRotationX(XM_PIDIV2);
	camera->SetFrustum(XM_PI / 3, D3DAppGet::AspectRatio(), 0.5f, 1000.0f);

	m_pColorBrush.Reset();
	HR(D3DAppGet::AllContexts().pd2dRenderTarget->CreateSolidColorBrush(
		D2D1::ColorF(D2D1::ColorF::White),
		m_pColorBrush.GetAddressOf()));
	HR(D3DAppGet::AllContexts().pdwriteFactory->CreateTextFormat(L"宋体", nullptr, DWRITE_FONT_WEIGHT_NORMAL,
		DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 25, L"zh-cn",
		m_pTextFormat.GetAddressOf()));
	return true;
}

void SGame::GameOver()
{
	m_Over = true;
}

void SGame::AddScore(int score)
{
	m_Score += score;
}

int SGame::GetScore()
{
	return m_Score;
}

XMFLOAT3 SGame::GetPlayerPos()
{
	return m_pPlayer->GetTransform().GetPosition();
}

void SGame::CreateEnemyA(const XMFLOAT3& pos, const XMFLOAT3& rot, const XMFLOAT3& sca)
{
	auto enemyCore = new ACore(false, pos);
	auto shooterParts = new AShooter({ pos.x - 2,pos.y,pos.z });
	shooterParts->AttachToActor(enemyCore);
	shooterParts->m_isPlayer = false;
	shooterParts->GetComponent<CCollider>()->SetLayer(collider_enemy);
	shooterParts->GetComponent<CShoot>()->SetShootType(ShootType::shoot_xzCircle);

	shooterParts = new AShooter({ pos.x + 2,pos.y,pos.z });
	shooterParts->AttachToActor(enemyCore);
	shooterParts->m_isPlayer = false;
	shooterParts->GetComponent<CCollider>()->SetLayer(collider_enemy);

	auto shooterParts2 = new AShooter({ pos.x + 2,pos.y+2,pos.z });
	shooterParts2->AttachToActor(shooterParts);
	shooterParts2->m_isPlayer = false;
	shooterParts2->GetComponent<CCollider>()->SetLayer(collider_enemy);

	shooterParts2 = new AShooter({ pos.x + 2,pos.y-2,pos.z });
	shooterParts2->AttachToActor(shooterParts);
	shooterParts2->m_isPlayer = false;
	shooterParts2->GetComponent<CCollider>()->SetLayer(collider_enemy);
}

bool SGame::Init()
{
	
	InitResource();

	BasicEffect& effect = BasicEffect::Get();
	effect.SetTextureCube(m_SkyBox.GetTextureCube());

	effect.SetFogStart(15);
	effect.SetFogRange(75);

	effect.SetFogColor(Colors::Black);
	effect.SetFogState(true);
	
	effect.SetViewMatrix(m_pCamera->GetViewXM());
	effect.SetEyePos(m_pCamera->GetPosition());
	effect.SetProjMatrix(m_pCamera->GetProjXM());

	
	// 環境光
	DirectionalLight dirLight;
	dirLight.ambient = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	dirLight.diffuse = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	dirLight.specular = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	dirLight.direction = XMFLOAT3(0.0f, -1.0f, 0.3f);
	BasicEffect::Get().SetDirLight(0, dirLight);
	// ポイントライト
	PointLight pointLight;
	pointLight.position = XMFLOAT3(0.0f, 10.0f, -10.0f);
	pointLight.ambient = XMFLOAT4(0.3f, 0.3f, 0.3f, 1.0f);
	pointLight.diffuse = XMFLOAT4(0.6f, 0.6f, 0.6f, 1.0f);
	pointLight.specular = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);
	pointLight.att = XMFLOAT3(0.0f, 0.1f, 0.0f);
	pointLight.range = 200.0f;
	BasicEffect::Get().SetPointLight(0, pointLight);

	D3DAppGet::InputState().SetMouseMode(false);
	return true;
}
