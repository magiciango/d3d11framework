#include "Input.h"

#define AbsoluteMode	(m_pMouse->GetState().positionMode == DirectX::Mouse::MODE_ABSOLUTE)
void Input::Init(HWND windows)
{
	m_pMouse = std::make_unique<DirectX::Mouse>();
	m_pKeyboard = std::make_unique<DirectX::Keyboard>();
	m_pMouse->SetWindow(windows);
	m_pMouse->SetMode(DirectX::Mouse::MODE_ABSOLUTE);
}

void Input::Update()
{
	Mouse::State mouseState = m_pMouse->GetState();
	Mouse::State lastMouseState = m_MouseTracker.GetLastState();
	if (AbsoluteMode)
	{
		move_x = mouseState.x - lastMouseState.x;
		move_y = mouseState.y - lastMouseState.y;
		pos_x = mouseState.x;
		pos_y = mouseState.y;
	}
	else
	{
		move_x = mouseState.x;
		move_y = mouseState.y;
		pos_x += mouseState.x;
		pos_y += mouseState.y;
	}
	m_MouseTracker.Update(mouseState);
	Keyboard::State keyState = m_pKeyboard->GetState();
	m_KeyboardTracker.Update(keyState);
}

bool Input::GetKeyDown(Keyboard::Keys keys)
{
	return m_pKeyboard->GetState().IsKeyDown(keys);
}

bool Input::GetKeyPressed(Keyboard::Keys keys)
{
	return m_KeyboardTracker.IsKeyPressed(keys);
}

bool Input::GetKeyRelease(Keyboard::Keys keys)
{
	return m_KeyboardTracker.IsKeyReleased(keys);
}

int Input::GetMouseMoveX()
{
	return move_x;
}

int Input::GetMouseMoveY()
{
	return move_y;
}

int Input::GetMousePosX()
{
	return pos_x;
}

int Input::GetMousePosY()
{
	return pos_y;
}

float Input::GetMouseScroll()
{
	return m_pMouse->GetState().scrollWheelValue;
}

bool Input::GetButtonDown(MouseButton button)
{
	switch (button)
	{
	case MouseButton::LeftButton:
		return m_pMouse->GetState().leftButton;
		break;
	case MouseButton::RightButton:
		return m_pMouse->GetState().rightButton;
		break;
	case MouseButton::MiddleButton:
		return m_pMouse->GetState().middleButton;
		break;
	default:
		break;
	}
	return false;
}

bool Input::GetButtonPressed(MouseButton button)
{
	switch (button)
	{
	case MouseButton::LeftButton:
		return m_MouseTracker.leftButton == m_MouseTracker.PRESSED;
		break;
	case MouseButton::RightButton:
		return m_MouseTracker.rightButton == m_MouseTracker.PRESSED;
		break;
	case MouseButton::MiddleButton:
		return m_MouseTracker.middleButton == m_MouseTracker.PRESSED;
		break;
	default:
		break;
	}
	return false;
}

bool Input::GetButtonRelease(MouseButton button)
{
	switch (button)
	{
	case MouseButton::LeftButton:
		return m_MouseTracker.leftButton == m_MouseTracker.RELEASED;
		break;
	case MouseButton::RightButton:
		return m_MouseTracker.rightButton == m_MouseTracker.RELEASED;
		break;
	case MouseButton::MiddleButton:
		return m_MouseTracker.middleButton == m_MouseTracker.RELEASED;
		break;
	default:
		break;
	}
	return false;
}

void Input::SetMouseMode(bool isAbsolute,int x,int y)
{
	m_pMouse->SetMode(isAbsolute ? DirectX::Mouse::MODE_ABSOLUTE : DirectX::Mouse::MODE_RELATIVE);
	if (!isAbsolute)
	{
		pos_x = x;
		pos_y = y;
	}
}

void Input::SetMousePos(int x, int y)
{
	if (!AbsoluteMode)
	{
		pos_x = x;
		pos_y = y;
	}
}

void Input::ResetScroll()
{
	m_pMouse->ResetScrollWheelValue();
}

const Mouse* Input::GetMouse()
{
	return m_pMouse.get();
}

const Keyboard* Input::GetKeyboard()
{
	return m_pKeyboard.get();
}
