#pragma once
#include "Scene.h"
#include "DynamicSkyBox.h"
class SEnd 
	:public Scene
{
	bool Init() override;
	int UpdateScene(float dt) override;
	void OnResize() override;
	void DrawScene(bool isOpaquePass) override;
	bool InitResource() override;

private:
	template <class T>
	using ComPtr = Microsoft::WRL::ComPtr<T>;
	//Direct2D用
	//**************************
	ComPtr<ID2D1SolidColorBrush> m_pColorBrush;				  
	ComPtr<IDWriteFont> m_pFont;								
	ComPtr<IDWriteTextFormat> m_pTextFormat;					

	Actor* m_pPlayer;
	Actor  m_BGMAudio;
	//AAtmospheric* SkyBox;

	SkyBox m_SkyBox;
	CGeometry m_ScreenMesh;
	float m_Time = 0;
	float m_posX;
	float m_posY;
	XMFLOAT3 sunPos;

	DynamicSkyBox m_DBox;
};