#include "SkyRender.h"
#include "imGui/imgui.h"
#include "d3dUtil.h"
#include "DXTrace.h"

#define TextureCubeSize		1280
using namespace DirectX;
void DynamicSkyRender::Init()
{
	m_WaveIter = 13;
	m_NormalIter = 38;
	m_AuroraIter = 20;
	m_Time = 0;
	m_Gamma = 2.2;
	m_Exposure = 1.0;
	//wave
	m_WaterDepth = 2.1;
	m_WaveSpeed = 2.0;
	m_WavePhase = 6.0;
	m_WaveWeight = 1.0;
	m_SunIntensity = 22.0;

	m_PlantRadius = 6371;
	m_AtomosphereRadius = 6471;
	m_kMie = 21;
	m_shRlh = 8;

	m_kRlh = XMFLOAT3(5.5, 13.0, 22.4);
	m_shMie = 1.2;

	m_g = 0.758;
	
	m_SunPos = XMFLOAT3(0, 1, 0);
	m_SunTimeScale = 0.3;
	m_SunRadius = 12288;
	
	m_StarsDensity = 2;;
	
	m_UseAurora = false;
	m_UseSea = false;
	m_AuroraDensity = 1.2;

	m_IBLTimer = 0;
	m_IBLID = 0;
	UpdateIBL = false;
	InitDynamicResource(D3DAppGet::pDevice(), 50000);
}

void DynamicSkyRender::Update(float dt)
{
	static bool auto_sun = true;
	m_Time += dt;
	m_IBLTimer += dt;
	if (m_LerpPlus)
		m_IBLLerp += dt;
	else
		m_IBLLerp -= dt;
	ImGui::Begin("SkyBox");
	//if (ImGui::CollapsingHeader("RayMarchingIter"))
	//{
	//	ImGui::SliderFloat("Wave", &m_WaveIter, 2, 30);
	//	ImGui::SliderFloat("Normal", &m_NormalIter, 6, 60);
	//	ImGui::SliderFloat("Aurora", &m_AuroraIter, 10, 50);
	//}
	if (ImGui::CollapsingHeader("PostProcess"))
	{
		ImGui::InputFloat("m_Exposure", &m_Exposure);
		ImGui::InputFloat("m_Gamma", &m_Gamma);
		ImGui::Text("Time:%f", m_Time);
	}

	if (ImGui::CollapsingHeader("Wave"))
	{
		ImGui::Checkbox("UseSea", &m_UseSea);
		ImGui::InputFloat("m_WaterDepth", &m_WaterDepth, 0, 3);
		ImGui::InputFloat("m_WaveSpeed", &m_WaveSpeed, 0, 3);
		ImGui::InputFloat("m_WavePhase", &m_WavePhase, 0, 3);
		ImGui::InputFloat("m_WaveWeight", &m_WaveWeight, 0, 3);
	}
	if (ImGui::CollapsingHeader("atomosphere"))
	{
		ImGui::InputFloat("m_SunIntensity", &m_SunIntensity);
		ImGui::InputFloat("m_PlantRadius", &m_PlantRadius);
		ImGui::InputFloat("m_AtomosphereRadius", &m_AtomosphereRadius);
		ImGui::InputFloat("m_kMie", &m_kMie);
		ImGui::InputFloat("m_shRlh", &m_shRlh);
		ImGui::InputFloat3("m_kRlh", (float*)&m_kRlh);
		ImGui::InputFloat("m_shMie", &m_shMie);
		ImGui::InputFloat("m_g", &m_g);
	}
	if (ImGui::CollapsingHeader("sun"))
	{

		ImGui::Checkbox("auto sun", &auto_sun);
		if (auto_sun)
		{
			ImGui::SliderFloat("SunVelocity", &m_SunTimeScale, 0, 2);
			ImGui::Text("SunPos:%f,%f,%f", m_SunPos.x, m_SunPos.y, m_SunPos.z);
		}

		else
			ImGui::SliderFloat3("m_SunPos", (float*)&m_SunPos, -1.0f, 1.0f);
		ImGui::InputFloat("m_SunRadius", &m_SunRadius);
	}
	if (ImGui::CollapsingHeader("stars"))
	{
		ImGui::InputFloat("m_StarsDensity", &m_StarsDensity);
	}
	if (ImGui::CollapsingHeader("aurora"))
	{
		ImGui::Checkbox("m_UseAurora", &m_UseAurora);
		if (m_UseAurora)
			ImGui::InputFloat("m_AuroraDensity", &m_AuroraDensity);
	}
	ImGui::End();
	if (auto_sun)
	{
		m_SunPos.x = sinf(m_Time * m_SunTimeScale * 0.3);
		m_SunPos.y = cosf(m_Time * m_SunTimeScale * 0.3);
		m_SunPos.z = 0;
	}
	XMVECTOR _sunposXM = XMLoadFloat3(&m_SunPos);
	_sunposXM = XMVector3Normalize(_sunposXM);
	XMStoreFloat3(&m_SunPos, _sunposXM);

	if (m_IBLTimer >= m_IBLStep)
	{
		m_IBLTimer -= m_IBLStep;
		UpdateIBL = true;
	}
}

void DynamicSkyRender::Draw(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, Camera* camera)
{
	static bool firstDraw = true;
	if (firstDraw)
	{
		for (int i = 0; i < 6; i++)
		{
			IBLCreateTexCube(deviceContext, skyEffect, i);
			IBLStepDiffuse(deviceContext, skyEffect, m_IBLID, true);
			IBLStepDiffuse(deviceContext, skyEffect, m_IBLID, false);
			for (int i = 0; i < 5; i++)
			{
				IBLStepSpecular(deviceContext, skyEffect, i, true);
				IBLStepSpecular(deviceContext, skyEffect, i, false);
			}
		}
		firstDraw = false;
	}
	if (UpdateIBL)
	{
		UpdateIBL = false;
		switch (m_IBLRenderType)
		{
		case DynamicSkyRender::CreateCubeA:
			for (int i = 0; i < 6; i++)
			{
				IBLCreateTexCube(deviceContext, skyEffect, i);
			}
			m_IBLLerp = 1;
			m_LerpPlus = false;
			m_IBLID = 0;
			m_IBLRenderType = DiffuseA;
			break;
		case DynamicSkyRender::DiffuseA:
			IBLStepDiffuse(deviceContext, skyEffect, m_IBLID, true);
			m_IBLRenderType = SpecularA;
			break;
		case DynamicSkyRender::SpecularA:
			IBLStepSpecular(deviceContext, skyEffect, m_IBLID, true);
			m_IBLID++;
			if (m_IBLID >= 5)
			{
				m_IBLID = 0;
				m_IBLRenderType = CreateCubeB;
			}
			break;
		case DynamicSkyRender::CreateCubeB:
			for (int i = 0; i < 6; i++)
			{
				IBLCreateTexCube(deviceContext, skyEffect, i);
			}
			m_IBLLerp = 0;
			m_LerpPlus = true;
			m_IBLID = 0;
			m_IBLRenderType = DiffuseB;
			break;
		case DynamicSkyRender::DiffuseB:
			IBLStepDiffuse(deviceContext, skyEffect, m_IBLID, false);
			m_IBLRenderType = SpecularB;
			//m_IBLID++;
			//if (m_IBLID >= 6)
			//{
			//	m_IBLID = 0;
			//	m_IBLRenderType = SpecularB;
			//}
			break;
		case DynamicSkyRender::SpecularB:
			IBLStepSpecular(deviceContext, skyEffect, m_IBLID, false);
			m_IBLID++;
			if (m_IBLID >= 5)
			{
				m_IBLID = 0;
				m_IBLRenderType = CreateCubeA;
			}
			break;
		default:
			break;
		}
		//for (int i = 0; i < 6; i++)
		//{
		//	IBLCreateTexCube(deviceContext, skyEffect, i);
		//}
		//m_pTextureHDRSRV = m_pSkyBoxCubeSRV;
		////IBLStuff(D3DAppGet::pDevice(), deviceContext, skyEffect, camera);
		//
		//for (int i = 0; i < 36; i++)
		//{
		//	IBLStepDiffuse(deviceContext, skyEffect, i);
		//}
		
		
		/*m_IBLID++;
		if (m_IBLID > 35)
			m_IBLID -= 35;*/
	}
	BlendIBL(D3DAppGet::pContext());
	D3D11_VIEWPORT vi = camera->GetViewPort();
	D3DAppGet::pContext()->RSSetViewports(1, &vi);
	skyEffect.SetUseCube(false);
	skyEffect.SetRenderDefault(deviceContext);
	//m_pCamera->SetViewPort(0.0f, 0.0f, static_cast<float>(TextureCubeSize), static_cast<float>(TextureCubeSize));
	//m_pCamera->SetFrustum(XM_PIDIV2, 1.0f, 0.001, 5000);
	//skyEffect.SetRenderIradiance(deviceContext);
	//DrawSkyBox(deviceContext, skyEffect, m_pCamera.get(), m_pSkyBoxCubeSRV.Get());
	//DrawSkyBox(deviceContext, skyEffect, camera, m_pPrefilterSRV.Get());
	skyEffect.SetScreenSize(D3DAppGet::Width(), D3DAppGet::Height());
	DrawDynamicSkyBox(deviceContext, skyEffect, camera);
}

void DynamicSkyRender::DrawDynamicSkyBox(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, Camera* camera)
{
	skyEffect.SetRenderAtomosphere(deviceContext);
	XMMATRIX V = camera->GetViewXM();
	V.r[3] = g_XMIdentityR3;
	skyEffect.SetViewMatrix(V);
	skyEffect.SetCameraPos(camera->GetPosition());
	skyEffect.SetTime(m_Time);
	skyEffect.SetFloatByName(m_WaveIter, "g_WaveIter");
	skyEffect.SetFloatByName(m_NormalIter, "g_NormalIter");
	skyEffect.SetFloatByName(m_AuroraIter, "g_AuroraIter");
	skyEffect.SetFloatByName(m_WaterDepth, "g_WaterDepth");
	skyEffect.SetFloatByName(m_WaveSpeed, "g_WaveSpeed");
	skyEffect.SetFloatByName(m_WavePhase, "g_WavePhase");
	skyEffect.SetFloatByName(m_WaveWeight, "g_WaveWeight");
	skyEffect.SetFloatByName(m_SunIntensity, "g_SunIntensity");
	skyEffect.SetFloatByName(m_PlantRadius, "g_PlantRadius");
	skyEffect.SetFloatByName(m_AtomosphereRadius, "g_AtomosphereRadius");
	skyEffect.SetFloatByName(m_kMie, "g_kMie");
	skyEffect.SetFloatByName(m_shRlh, "g_shRlh");
	skyEffect.SetkRlh(m_kRlh);
	skyEffect.SetFloatByName(m_shMie, "g_shMie");
	skyEffect.SetFloatByName(m_g, "g_g");
	skyEffect.SetSunPos(m_SunPos);
	skyEffect.SetFloatByName(m_SunRadius, "g_SunRadius");
	skyEffect.SetFloatByName(m_StarsDensity, "g_StarsDensity");
	skyEffect.SetFloatByName(m_UseAurora, "g_UseAurora");
	skyEffect.SetFloatByName(m_UseSea, "g_UseSea");
	skyEffect.SetFloatByName(m_AuroraDensity, "g_AuroraDensity");
	skyEffect.Apply(deviceContext);
	UINT strides = sizeof(VertexPosTex);
	UINT offset = 0;
	deviceContext->IASetVertexBuffers(0, 1, m_pQuadVertexBuffer.GetAddressOf(), &strides, &offset);
	deviceContext->IASetIndexBuffer(m_pQuadIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
	deviceContext->DrawIndexed(6, 0, 0);
	XMVECTOR vector = XMLoadFloat3(&m_SunPos);
	XMVector3Transform(vector, camera->GetViewProjXM());
}

DirectionalLight DynamicSkyRender::GetSunLight()
{
	m_SunLight.ambient = { 1.0f,1.0f,1.0f,1.0f };
	m_SunLight.diffuse = { 0.5f,0.5f,0.4f,1.0f };
	m_SunLight.specular = { 0.8f,0.8f,0.7f,1.0f };
	if (m_SunPos.y < 0)
	{
		m_SunLight.diffuse = { 0.4f,0.4f,0.4f,0.4f };
		m_SunLight.specular = { 0.4f,0.4f,0.4f,1.0f };
	}
	m_SunLight.direction = { -m_SunPos.x,-m_SunPos.y,-m_SunPos.z };
	return m_SunLight;
}

HRESULT DynamicSkyRender::InitDynamicResource(ID3D11Device* device, float skySphereRadius)
{
	HR(InitResource(device, skySphereRadius));

#pragma region TextureCube
	// Create TexArray
	ComPtr<ID3D11Texture2D> texCube;

	D3D11_TEXTURE2D_DESC texDesc;
	texDesc.Width = TextureCubeSize;
	texDesc.Height = TextureCubeSize;
	texDesc.MipLevels = 0;
	texDesc.ArraySize = 6;													//double ArraySize
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;					//Create Texture Array not Texture Cube
	HR(device->CreateTexture2D(&texDesc, nullptr, texCube.GetAddressOf()));

	// Create RTV
	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	rtvDesc.Format = texDesc.Format;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
	rtvDesc.Texture2DArray.MipSlice = 0;
	rtvDesc.Texture2DArray.ArraySize = -1;

	// Create RTV for Every Element
	for (int i = 0; i < 6; ++i)
	{
		rtvDesc.Texture2DArray.FirstArraySlice = i;
		HR(device->CreateRenderTargetView(texCube.Get(), &rtvDesc,
			m_pSkyBoxCubeRTVs[i].GetAddressOf()));
	}

	// Create SRV
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.Format = texDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
	srvDesc.TextureCube.MostDetailedMip = 0;
	srvDesc.TextureCube.MipLevels = -1;

	HR(device->CreateShaderResourceView(texCube.Get(), &srvDesc,
		m_pSkyBoxCubeSRV.GetAddressOf()));
#pragma endregion

#pragma region IBLDiffuseArray

	// Create TexArray
	ComPtr<ID3D11Texture2D> texDiffuse;

	//D3D11_TEXTURE2D_DESC texDesc;
	texDesc.Width = IradianceMapSize;
	texDesc.Height = IradianceMapSize;
	texDesc.MipLevels = 0;
	texDesc.ArraySize = 12;													//double ArraySize
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;					//Create Texture Array not Texture Cube
	HR(device->CreateTexture2D(&texDesc, nullptr, texDiffuse.GetAddressOf()));

	// Create RTV
	//D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	rtvDesc.Format = texDesc.Format;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
	rtvDesc.Texture2DArray.MipSlice = 0;
	rtvDesc.Texture2DArray.ArraySize = 1;

	// Create RTV for Every Element
	for (int i = 0; i < 12; ++i)
	{
		rtvDesc.Texture2DArray.FirstArraySlice = i;
		HR(device->CreateRenderTargetView(texDiffuse.Get(), &rtvDesc,
			m_pDiffuseArrayRTVs[i].GetAddressOf()));
	}

	// Create SRV
	//D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.Format = texDesc.Format;
	//srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	srvDesc.Texture2DArray.ArraySize = 12;
	srvDesc.Texture2DArray.FirstArraySlice = 0;
	srvDesc.Texture2DArray.MipLevels = 1;
	srvDesc.Texture2DArray.MostDetailedMip = 0;
	//srvDesc.TextureCube.MostDetailedMip = 0;
	//srvDesc.TextureCube.MipLevels = -1;	// use All MipMap

	HR(device->CreateShaderResourceView(texDiffuse.Get(), &srvDesc,
		m_pDiffuseArraySRV.GetAddressOf()));
#pragma endregion

#pragma region PrefilterArray
	// Create TextureArray
	ComPtr<ID3D11Texture2D> PrefilterArray;

	texDesc.Width = PrefilterMapSize;
	texDesc.Height = PrefilterMapSize;
	texDesc.MipLevels = MipLevels;
	texDesc.ArraySize = 12;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;

	HR(device->CreateTexture2D(&texDesc, nullptr, PrefilterArray.GetAddressOf()));

	// Create SRV
	srvDesc.Format = texDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	srvDesc.Texture2DArray.ArraySize = 12;
	srvDesc.Texture2DArray.MipLevels = MipLevels;
	srvDesc.Texture2DArray.FirstArraySlice = 0;
	srvDesc.Texture2DArray.MostDetailedMip = 0;
	/*srvDesc.TextureCube.MostDetailedMip = 0;
	srvDesc.TextureCube.MipLevels = MipLevels;*/
	HR(device->CreateShaderResourceView(PrefilterArray.Get(), &srvDesc,
		m_pPrefilterArraySRV.GetAddressOf()));
	// Loop For Every MipMap
	for (int mip = 0; mip < MipLevels; mip++)
	{
		// Create RTV
		rtvDesc.Format = texDesc.Format;
		rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
		rtvDesc.Texture2DArray.MipSlice = mip;
		rtvDesc.Texture2DArray.ArraySize = 1;

		// RTV for Every Elements
		for (int i = 0; i < 6; ++i)
		{
			rtvDesc.Texture2DArray.FirstArraySlice = i;
			HR(device->CreateRenderTargetView(PrefilterArray.Get(), &rtvDesc,
				m_pPrefilterArrayRTVs[mip * 6 + i].GetAddressOf()));

			rtvDesc.Texture2DArray.FirstArraySlice = i + 6;
			HR(device->CreateRenderTargetView(PrefilterArray.Get(), &rtvDesc,
				m_pPrefilterArrayRTVs[mip * 6 + i + 30].GetAddressOf()));
		}
	}
#pragma endregion
	return S_OK;
}

void DynamicSkyRender::IBLCreateTexCube(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, int id)
{
	m_pCamera->SetViewPort(0.0f, 0.0f, static_cast<float>(TextureCubeSize), static_cast<float>(TextureCubeSize));
	SkyEffect& effect = SkyEffect::Get();
	effect.SetScreenSize(TextureCubeSize, TextureCubeSize);
	// Render TextureCube
	Cache(deviceContext);
	BeginCapture(deviceContext, skyEffect, XMFLOAT3(0, 5, -15), static_cast<D3D11_TEXTURECUBE_FACE>(id), 0.0001, 5.0f);
	// Clear Buffer
	deviceContext->ClearRenderTargetView(m_pSkyBoxCubeRTVs[id].Get(), reinterpret_cast<const float*>(&Colors::Red));
	deviceContext->OMSetRenderTargets(1, m_pSkyBoxCubeRTVs[id].GetAddressOf(), nullptr);
	DrawDynamicSkyBox(deviceContext, skyEffect, m_pCamera.get());
	Restore(deviceContext);
}

void DynamicSkyRender::IBLStepDiffuse(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, int id, bool isAPass)
{
#pragma region Diffuse IBL
	// Init ViewPort
	m_pCamera->SetViewPort(0.0f, 0.0f, static_cast<float>(IradianceMapSize), static_cast<float>(IradianceMapSize));

	// Render TextureCube
	Cache(deviceContext);
	for (int i = 0; i < 6; i++)
	{
		BeginCapture(deviceContext, skyEffect, XMFLOAT3(0, 0, 0), static_cast<D3D11_TEXTURECUBE_FACE>(i));
		// Clear Buffer
		deviceContext->ClearRenderTargetView(m_pDiffuseArrayRTVs[i + (isAPass ? 0 : 6)].Get(), reinterpret_cast<const float*>(&Colors::Black));
		deviceContext->OMSetRenderTargets(1, m_pDiffuseArrayRTVs[i + (isAPass ? 0 : 6)].GetAddressOf(), nullptr);
		skyEffect.SetRenderIradiance(deviceContext);
		DrawSkyBox(deviceContext, skyEffect, m_pCamera.get(), m_pSkyBoxCubeSRV.Get());
	}
	Restore(deviceContext);
}

void DynamicSkyRender::IBLStepSpecular(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, int miplevel, bool isAPass)
{
	int mip = miplevel;

	unsigned mipWidth = PrefilterMapSize * pow(0.5, mip);
	unsigned mipHeight = PrefilterMapSize * pow(0.5, mip);

	// Init ViewPort
	m_pCamera->SetViewPort(0.0f, 0.0f, static_cast<float>(mipWidth), static_cast<float>(mipHeight));
	float roughness = (float)mip / (float)(MipLevels - 1);

	// Render TextureCube
	Cache(deviceContext);
	for (int i = 0; i < 6; i++)
	{
		BeginCapture(deviceContext, skyEffect, XMFLOAT3(0, 5, -15), static_cast<D3D11_TEXTURECUBE_FACE>(i));

		// Clear Buffer
		deviceContext->ClearRenderTargetView(m_pPrefilterArrayRTVs[miplevel * 6 + i + (isAPass ? 0 : 30)].Get(), reinterpret_cast<const float*>(&Colors::Black));
		// SetRT
		deviceContext->OMSetRenderTargets(1, m_pPrefilterArrayRTVs[miplevel * 6 + i + (isAPass ? 0 : 30)].GetAddressOf(), nullptr);
		skyEffect.SetRenderPrefilter(deviceContext, roughness);
		DrawSkyBox(deviceContext, skyEffect, m_pCamera.get(), m_pSkyBoxCubeSRV.Get());
	}
	
		
	Restore(deviceContext);
}

void DynamicSkyRender::BlendIBL(ID3D11DeviceContext* deviceContext)
{
	auto CaluLerp = [](float first, bool isPlus, float step, int num)
	{
		float lerp;
		lerp = first + (isPlus ? -step * num : step * num);
		if (lerp > 1)
			lerp = 2 - lerp;
		if (lerp < 0)
			lerp = -lerp;
		return lerp;
	};
	Cache(deviceContext);

	ID3D11RenderTargetView* view[6] = { nullptr };
	for (int i = 0; i < 6; i++)
	{
		view[i] = m_pDiffuseMapRTVs[i].Get();
		deviceContext->ClearRenderTargetView(view[i], reinterpret_cast<const float*>(&Colors::Black));
	}
	m_pCamera->SetViewPort(0, 0, IradianceMapSize, IradianceMapSize);
	deviceContext->RSSetViewports(1, &m_pCamera->GetViewPort());
	deviceContext->OMSetRenderTargets(6, view, nullptr);
	auto& effect = SkyEffect::Get();
	effect.SetRenderBlend(deviceContext);
	effect.SetBlendResources(m_pDiffuseArraySRV.Get(), CaluLerp(m_IBLLerp, m_LerpPlus, m_IBLStep, 1), 0, 0);
	effect.Apply(deviceContext);

	UINT strides = sizeof(VertexPosTex);
	UINT offset = 0;
	deviceContext->IASetVertexBuffers(0, 1, m_pQuadVertexBuffer.GetAddressOf(), &strides, &offset);
	deviceContext->IASetIndexBuffer(m_pQuadIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
	deviceContext->DrawIndexed(6, 0, 0);

	for (int mip = 0; mip < MipLevels; mip++)
	{
		ID3D11RenderTargetView* view[6] = { nullptr };
		for (int i = 0; i < 6; i++)
		{
			view[i] = m_pPrefilterRTVs[mip * 6 + i].Get();
			deviceContext->ClearRenderTargetView(view[i], reinterpret_cast<const float*>(&Colors::Black));
		}
		unsigned mipWidth = PrefilterMapSize * pow(0.5, mip);
		unsigned mipHeight = PrefilterMapSize * pow(0.5, mip);
		m_pCamera->SetViewPort(0, 0, mipWidth, mipHeight);
		deviceContext->RSSetViewports(1, &m_pCamera->GetViewPort());

		deviceContext->OMSetRenderTargets(6, view, nullptr);
		auto& effect = SkyEffect::Get();

		effect.SetRenderBlend(deviceContext);
		effect.SetBlendResources(m_pPrefilterArraySRV.Get(), CaluLerp(m_IBLLerp, m_LerpPlus, m_IBLStep, mip + 1), mip, 0.0f);
		effect.Apply(deviceContext);

		UINT strides = sizeof(VertexPosTex);
		UINT offset = 0;
		deviceContext->IASetVertexBuffers(0, 1, m_pQuadVertexBuffer.GetAddressOf(), &strides, &offset);
		deviceContext->IASetIndexBuffer(m_pQuadIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
		deviceContext->DrawIndexed(6, 0, 0);
	}
	Restore(deviceContext);
}
