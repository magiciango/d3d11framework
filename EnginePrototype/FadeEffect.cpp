#include "Effects.h"
#include "d3dUtil.h"
#include "EffectHelper.h"	// 必须晚于Effects.h和d3dUtil.h包含
#include "DXTrace.h"
#include "Vertex.h"
using namespace DirectX;


//
// FadeEffect::Impl 需要先于ScreenFadeEffect的定义
//

class FadeEffect::Impl
{
public:
	// 必须显式指定
	Impl() : m_IsDirty() {}
	~Impl() = default;

public:
	BOOL m_IsDirty;										    // 是否有值变更

	ComPtr<ID3D11InputLayout> m_pVertexPosTexLayout;
	std::unique_ptr<EffectHelper> m_pEffectHelper;

	std::shared_ptr<IEffectPass> m_pCurrEffectPass;
};



//
// FadeEffect
//

namespace
{
	// ScreenFadeEffect单例
	static FadeEffect* g_pInstance = nullptr;
}

FadeEffect::FadeEffect()
{
	if (g_pInstance)
		throw std::exception("ScreenFadeEffect is a singleton!");
	g_pInstance = this;
	pImpl = std::make_unique<FadeEffect::Impl>();
}

FadeEffect::~FadeEffect()
{
}

FadeEffect::FadeEffect(FadeEffect&& moveFrom) noexcept
{
	pImpl.swap(moveFrom.pImpl);
}

FadeEffect& FadeEffect::operator=(FadeEffect&& moveFrom) noexcept
{
	pImpl.swap(moveFrom.pImpl);
	return *this;
}

FadeEffect& FadeEffect::Get()
{
	if (!g_pInstance)
		throw std::exception("ScreenFadeEffect needs an instance!");
	return *g_pInstance;
}

bool FadeEffect::InitAll(ID3D11Device* device)
{
	if (!device)
		return false;

	//if (!pImpl->m_pCBuffers.empty())
	//	return true;

	if (!RenderStates::IsInit())
		throw std::exception("RenderStates need to be initialized first!");
	pImpl->m_pEffectHelper = std::make_unique<EffectHelper>();
	ComPtr<ID3DBlob> blob;

	// ******************
	// 创建顶点着色器
	//

	HR(CreateShaderFromFile(L"HLSL\\Basic_Screen_VS.cso", L"HLSL\\Basic_Screen_VS.hlsl", "VS", "vs_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("FadeEffect_VS",device,blob.Get()));
	// 创建顶点布局
	HR(device->CreateInputLayout(VertexPosTex::inputLayout, ARRAYSIZE(VertexPosTex::inputLayout),
		blob->GetBufferPointer(), blob->GetBufferSize(), pImpl->m_pVertexPosTexLayout.GetAddressOf()));

	// ******************
	// 创建像素着色器
	//
	//普通像素着色器
	HR(CreateShaderFromFile(L"HLSL\\FadeEffect_PS.cso", L"HLSL\\FadeEffect_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("FadeEffect_PS", device, blob.Get()));
	//翻页像素着色器
	HR(CreateShaderFromFile(L"HLSL\\FadeCurlEffect_PS.cso", L"HLSL\\FadeCurlEffect_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("FadeCurlEffect_PS", device, blob.Get()));
	//网格像素着色器
	HR(CreateShaderFromFile(L"HLSL\\FadeEffect_Grid_PS.cso", L"HLSL\\FadeEffect_Grid_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("FadeGridEffect_PS", device, blob.Get()));

	/*pImpl->m_pCBuffers.assign({
		&pImpl->m_CBFrame,
		&pImpl->m_CBRarely
		});*/

	//// 创建常量缓冲区
	//for (auto& pBuffer : pImpl->m_pCBuffers)
	//{
	//	HR(pBuffer->CreateBuffer(device));
	//}

	//// 设置调试对象名
	//D3D11SetDebugObjectName(pImpl->m_pVertexPosTexLayout.Get(), "ScreenFadeEffect.VertexPosTexLayout");
	//D3D11SetDebugObjectName(pImpl->m_pCBuffers[0]->cBuffer.Get(), "ScreenFadeEffect.CBFrame");
	//D3D11SetDebugObjectName(pImpl->m_pCBuffers[1]->cBuffer.Get(), "ScreenFadeEffect.CBRarely");
	//D3D11SetDebugObjectName(pImpl->m_pScreenFadeVS.Get(), "ScreenFadeEffect.ScreenFade_VS");
	//D3D11SetDebugObjectName(pImpl->m_pScreenFadePS.Get(), "ScreenFadeEffect.ScreenFade_PS");

	EffectPassDesc passDesc;
	passDesc.nameVS = "FadeEffect_VS";
	passDesc.namePS = "FadeEffect_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("Fade", device, &passDesc));
	passDesc.nameVS = "FadeEffect_VS";
	passDesc.namePS = "FadeCurlEffect_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("Curl", device, &passDesc));
	passDesc.nameVS = "FadeEffect_VS";
	passDesc.namePS = "FadeGridEffect_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("Grid", device, &passDesc));


	D3D11SetDebugObjectName(pImpl->m_pVertexPosTexLayout.Get(), "ScreenFadeEffect.VertexPosTexLayout");
	pImpl->m_pEffectHelper->SetDebugObjectName("FadeEffect");
	return true;
}

void FadeEffect::SetRenderDefault(ID3D11DeviceContext* deviceContext)
{
	/*deviceContext->IASetInputLayout(pImpl->m_pVertexPosTexLayout.Get());
	deviceContext->VSSetShader(pImpl->m_pScreenFadeVS.Get(), nullptr, 0);
	deviceContext->PSSetShader(pImpl->m_pScreenFadePS.Get(), nullptr, 0);

	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	deviceContext->GSSetShader(nullptr, nullptr, 0);
	deviceContext->RSSetState(nullptr);

	deviceContext->PSSetSamplers(0, 1, RenderStates::SSLinearWrap.GetAddressOf());
	deviceContext->OMSetDepthStencilState(nullptr, 0);
	deviceContext->OMSetBlendState(nullptr, nullptr, 0xFFFFFFFF);*/
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosTexLayout.Get());
	pImpl->m_pEffectHelper->SetSamplerStateByName("g_Sam", RenderStates::SSLinearWrap.Get());
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("Fade");
}

void FadeEffect::SetRenderCurl(ID3D11DeviceContext* deviceContext)
{
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosTexLayout.Get());
	pImpl->m_pEffectHelper->SetSamplerStateByName("g_Sam", RenderStates::SSLinearWrap.Get());
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("Curl");
}

void FadeEffect::SetRenderGrid(ID3D11DeviceContext* deviceContext)
{
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosTexLayout.Get());
	pImpl->m_pEffectHelper->SetSamplerStateByName("g_Sam", RenderStates::SSLinearWrap.Get());
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("Grid");
}

void XM_CALLCONV FadeEffect::SetWorldViewProjMatrix(DirectX::FXMMATRIX W, DirectX::CXMMATRIX V, DirectX::CXMMATRIX P)
{
	/*auto& cBuffer = pImpl->m_CBRarely;
	cBuffer.data.worldViewProj = XMMatrixTranspose(W * V * P);
	pImpl->m_IsDirty = cBuffer.isDirty = true;*/
}

void XM_CALLCONV FadeEffect::SetWorldViewProjMatrix(DirectX::FXMMATRIX WVP)
{
	//pImpl->m_pEffectHelper->GetConstantBufferVariable("g_WorldViewProj")->SetFloatMatrix(4, 4, (FLOAT*)&WVP);
}

void FadeEffect::SetFadeAmount(float fadeAmount)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_FadeAmount")->SetFloat(fadeAmount);
}

void FadeEffect::SetScreenSize(float width, float height)
{
	FLOAT size[2] = { width,height };
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_ScreenSize")->SetFloatVector(2, size);
}

void FadeEffect::SetMousePosition(float x, float y)
{
	FLOAT pos[2] = { x,y };
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_MousePosition")->SetFloatVector(2, pos);
}


void FadeEffect::SetTexture(ID3D11ShaderResourceView* texture)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_Tex", texture);
}

void FadeEffect::SetNextTexture(ID3D11ShaderResourceView* texture)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_NextTex", texture);
}

void FadeEffect::Apply(ID3D11DeviceContext* deviceContext)
{
	if (pImpl->m_pCurrEffectPass)
		pImpl->m_pCurrEffectPass->Apply(deviceContext);
}
