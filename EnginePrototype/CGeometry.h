#pragma once

#include "Component.h"
#include "d3dApp.h"
#include <DirectXCollision.h>
class CGeometry :public Component
{
	template <class T>
	using ComPtr = Microsoft::WRL::ComPtr<T>;
public:
	CGeometry();
	CGeometry(Actor* obj);
	static size_t tag;

	template<class VertexType, class IndexType>
	void SetBuffer(const Geometry::MeshData<VertexType, IndexType>& meshData);
	void SetTexture(ID3D11ShaderResourceView* texture);
	void SetTextureNormal(ID3D11ShaderResourceView* texture);
	void SetTextureHeight(ID3D11ShaderResourceView* texture);
	void SetTextureMetal(ID3D11ShaderResourceView* texture);
	void SetTextureRough(ID3D11ShaderResourceView* texture);
	void SetMaterial(const Material& material);
	
	bool FrustumCulling(DirectX::XMMATRIX world, DirectX::XMMATRIX view, DirectX::XMMATRIX proj);
	/*void LoadModel(std::string path);*/
	void Update(float dt) override;
	//void Draw(const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& view, const DirectX::XMMATRIX& proj) override;
	void Draw(IEffect* effect, const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& view, const DirectX::XMMATRIX& proj) override;

	DirectX::BoundingBox GetBoundingBox() { return boundingBox; };
	Material GetMatrial() { return m_Material; };
public:
	Material m_Material;
	ID3D11ShaderResourceView* m_pTexture;
	ID3D11ShaderResourceView* m_pNormalTexture;
	ID3D11ShaderResourceView* m_pDepthTexture;
	ID3D11ShaderResourceView* m_pMetallicTexture;
	ID3D11ShaderResourceView* m_pRoughTexture;

	ComPtr<ID3D11Buffer> m_pVertexBuffer;				
	ComPtr<ID3D11Buffer> m_pIndexBuffer;				
	UINT m_VertexStride;								
	UINT m_IndexCount;

	DirectX::XMVECTOR vMin;
	DirectX::XMVECTOR vMax;
	DirectX::BoundingBox boundingBox;

	bool m_choose = false;
	float m_HeightScale = 0.15f;
	bool m_UseMatCap = false;
	float m_a = 1;
	float m_b = 25;
	int m_c = 1;
	int m_d = 5;
};

template<class VertexType, class IndexType>
inline void CGeometry::SetBuffer(const Geometry::MeshData<VertexType, IndexType>& meshData)
{
	// 释放旧资源
	m_pVertexBuffer.Reset();
	m_pIndexBuffer.Reset();

	ID3D11Device* pDevice = D3DAppGet::pDevice();
	// 检查D3D设备
	if (pDevice == nullptr)
		return;

	// 设置顶点缓冲区描述
	m_VertexStride = sizeof(VertexType);
	D3D11_BUFFER_DESC vbd;
	ZeroMemory(&vbd, sizeof(vbd));
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.ByteWidth = (UINT)meshData.vertexVec.size() * m_VertexStride;
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	for (auto& i : meshData.vertexVec)
	{
		vMax = XMVectorMax(vMax, XMLoadFloat3(&i.pos));
		vMin = XMVectorMin(vMin, XMLoadFloat3(&i.pos));
	}
	BoundingBox::CreateFromPoints(boundingBox, vMin, vMax);
	// 新建顶点缓冲区
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = meshData.vertexVec.data();
	pDevice->CreateBuffer(&vbd, &InitData, m_pVertexBuffer.GetAddressOf());

	// 设置索引缓冲区描述
	m_IndexCount = (UINT)meshData.indexVec.size();
	D3D11_BUFFER_DESC ibd;
	ZeroMemory(&ibd, sizeof(ibd));
	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.ByteWidth = m_IndexCount * sizeof(IndexType);
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	// 新建索引缓冲区
	InitData.pSysMem = meshData.indexVec.data();
	pDevice->CreateBuffer(&ibd, &InitData, m_pIndexBuffer.GetAddressOf());
}