#pragma once

#include <xaudio2.h>
#include "Component.h"


class CAudio : public Component
{
private:
	static IXAudio2*				m_Xaudio;
	static IXAudio2MasteringVoice*	m_MasteringVoice;

	IXAudio2SourceVoice*	m_SourceVoice;
	BYTE*					m_SoundData;

	int						m_Length;
	int						m_PlayLength;
public:
	CAudio(Actor* obj);
	static size_t tag;

	static void InitMaster();
	static void UninitMaster();

	void Init() {};
	void Uninit();
	void Update() {};
	void Draw() {};

	void Load(const wchar_t *FileName);
	void Play(bool Loop = false);


};

