#pragma once

#include "CAudio.h"
#include "CBillboard.h"
#include "CCollider.h"
#include "CGeometry.h"
#include "CMove.h"
#include "CStaticMesh.h"
#include<list>
#include<map>
#include<set>
#include "d3dUtil.h"
#include "DXTrace.h"
enum class ActorType
{
	tag_default=0,
	//物体性质
	tag_player,
	tag_playerBullet,
	tag_enemy,
	tag_enemyBullet,
	tag_scene_obj,
	//图层
	tag_opaque,
	tag_transparent,
	tag_UI,
	//碰撞层

};
using namespace std;
class Actor
{
public:
	template <class T>
	using ComPtr = Microsoft::WRL::ComPtr<T>;
public:
	Actor();
	~Actor();
	Actor(Actor&& moveFrom) = default;
	std::vector<ActorType> m_ActorTag;

	template<class T>
	T* GetComponent();

	void AddComponent(Component* component);

	Transform& GetTransform();
	const Transform& GetTransform() const;

	void SetActive(bool active = true);
	const bool GetActive() const;

	void AddTag(ActorType tag);
	bool FindTag(ActorType tag);

	virtual void Init() {};
	virtual void Update(float dt);
	virtual void Draw(IEffect* effect, const DirectX::XMMATRIX& V, const DirectX::XMMATRIX& P);
	//virtual void Action() {};

	bool m_isDestory;
protected:
	std::map<size_t, std::unique_ptr<Component>> m_ComponentMap;
	Transform m_Transform;
	bool m_Active = true;

	//UE4 USceneComponent为基础的父子构造//todo:Component化
public:
	virtual void AttachToActor(Actor* parent);
	void AddChild(Actor* child);
	virtual void UnAttach();
	Actor* GetChild(int id);
	XMMATRIX GetParentTransformMatrixXM();
	XMFLOAT4X4 GetWorldTransformMatrix();
	XMMATRIX GetWorldTransformMatrixXM();
	XMFLOAT3 GetWorldPosition();
	XMFLOAT3 GetWorldForwardAxis();
	XMFLOAT3 GetWorldUpAxis();
	XMFLOAT3 GetWorldRightAxis();
public:
	std::vector<Actor*> m_Children;
	Actor* m_Parent = nullptr;
};

struct IParts
	:public Actor
{
	int HP;
	bool m_isPlayer;
	float DamageCD = 0;
	virtual void Damage(int damage) {
		if (DamageCD <= 0)
		{
			HP -= damage;
			DamageCD = 0.1;
		}
	}
	virtual void Action() = 0;
	virtual void Break() = 0;
};

template<class T>
inline T* Actor::GetComponent()
{
	if (m_ComponentMap.count(T::tag))
	{
		return (T*)m_ComponentMap[T::tag].get();
	}
	return nullptr;

}

class ObjectManager
{
public:
	//std::set<Actor*> set;
	//static ObjectManager* self;
	ObjectManager();
public:
	std::list<Actor*> m_OpaqueObj;
	std::list<Actor*> m_TransparentObj;
	//static ObjectManager* Instance();
	void AddOpaqueObj(Actor* obj);
	void AddTransparentObj(Actor* obj);
	void DeleteObj(Actor* obj);
	void Clear();
	void Update(float dt)
	{
		for (auto i = m_OpaqueObj.begin(); i != m_OpaqueObj.end(); i++)
		{
			if ((*i)->m_isDestory)
			{
				delete(*i);
  				m_OpaqueObj.erase(i++);
				if (i == m_OpaqueObj.end())
					break;
			}
			else if ((*i)->GetActive())
			{
				(*i)->Update(dt);
			}
				
		}
		for (auto i = m_TransparentObj.begin(); i != m_TransparentObj.end(); i++)
		{
			if ((*i)->m_isDestory)
			{
				delete(*i);
				m_OpaqueObj.erase(i++);
				if (i == m_OpaqueObj.end())
					break;
			}
			else if ((*i)->GetActive())
			{
				(*i)->Update(dt);
			}

		}
	}
	void DrawOpaque(IEffect* effect, const DirectX::XMMATRIX& V, const DirectX::XMMATRIX& P)
	{
		for (auto& i : m_OpaqueObj)
		{
			if (i->GetActive())
				i->Draw(effect,V, P);
		}
	}
	void DrawTransparent(IEffect* effect,const DirectX::XMMATRIX& V, const DirectX::XMMATRIX& P)
	{
		for (auto& i : m_TransparentObj)
		{
			if (i->GetActive())
				i->Draw(effect, V, P);
		}
	}
};

#define AllObj	ObjectManager::Instance()->list
#define TObj	ObjectManager::Instance()->tList