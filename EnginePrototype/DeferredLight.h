#pragma once
#include "TextureRender.h"
#include "Geometry.h"
class DeferredLight
{
    template <class T>
    using ComPtr = Microsoft::WRL::ComPtr<T>;
public:
    enum class Output
    {
        Position,
        Normal,
        Ambient,
        Diffuse,
        Spec
    };
    HRESULT InitAll(int texWidth, int texHeight, bool generateMips = false, ID3D11Device* device = D3DAppGet::pDevice());
    // 开始对当前纹理进行渲染
    void Begin(ID3D11DeviceContext* deviceContext = D3DAppGet::pContext());
    // 结束GBuffer渲染，进入Lpass
    void EndGPass(ID3D11DeviceContext* deviceContext = D3DAppGet::pContext());
    void EndLPass(ID3D11DeviceContext* deviceContext = D3DAppGet::pContext());
    // 获取渲染好的纹理的着色器资源视图
    // 引用数不增加，仅用于传参
    ID3D11ShaderResourceView* GetOutputTexture(Output type);
    //ID3D11Texture2D* GetOutputTexture2D(Output type);
private:
    std::unique_ptr<TextureRender> m_pGeometryPass;
    std::unique_ptr<TextureRender> m_pLightPass;
};

