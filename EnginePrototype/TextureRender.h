#pragma once

#include "d3dApp.h"
#include <string>
#include <vector>
#define MaxBufferCount  3
class TextureRender
{
public:
    template<class T>
    using ComPtr = Microsoft::WRL::ComPtr<T>;

    HRESULT InitResource(int texWidth, int texHeight,       //屏幕大小
        bool UseDepth = false,                              //是否保存深度缓冲(formatR24G8)
        unsigned BufferCount = 1,                           //使用的RT个数
        DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM,    //RT format
        bool generateMips = false,                          //是否生成mipmap
        ID3D11Device* device = D3DAppGet::pDevice());       //解耦device

    // 开始对当前纹理进行渲染
    void Begin(const FLOAT backgroundColor[4], bool notChangeDSV = false, ID3D11DeviceContext* deviceContext = D3DAppGet::pContext());
    // 结束对当前纹理的渲染，还原状态
    void End(ID3D11DeviceContext* deviceContext = D3DAppGet::pContext());
    // 获取渲染好的纹理的着色器资源视图
    // 引用数不增加，仅用于传参
    ID3D11ShaderResourceView* GetOutputTexture(unsigned id = 0);
    ID3D11ShaderResourceView* GetDepthTexture();
    ID3D11Texture2D* GetOutputTexture2D(unsigned id = 0);
private:
    ComPtr<ID3D11ShaderResourceView>        m_pOutputTextureSRV[MaxBufferCount];    // 输出的纹理对应的着色器资源视图
    ComPtr<ID3D11RenderTargetView>          m_pOutputTextureRTV[MaxBufferCount];    // 输出的纹理对应的渲染目标视图
    ComPtr<ID3D11DepthStencilView>          m_pOutputTextureDSV;    // 输出纹理所用的深度/模板视图
    ComPtr<ID3D11Texture2D>                 m_pTexture[MaxBufferCount];             // 输出纹理所用的纹理贴图
    D3D11_VIEWPORT                                      m_OutputViewPort;       // 输出所用的视口

    ComPtr<ID3D11RenderTargetView>                      m_pCacheRTV;            // 临时缓存的后备缓冲区
    ComPtr<ID3D11DepthStencilView>                      m_pCacheDSV;            // 临时缓存的深度/模板缓冲区
    D3D11_VIEWPORT                                      m_CacheViewPort;        // 临时缓存的视口

    bool                                m_GenerateMips;            // 是否生成mipmap链
    bool                                m_DepthMap;                 //是否生成深度缓冲
    unsigned                                 m_BufferCount = 1;     //输出资源视图数量
};

