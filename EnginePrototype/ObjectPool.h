#pragma once
#include<queue>
#include<memory>

using namespace std;

template <class T>
class ObjectPool
{
public:
	ObjectPool();
	
	T* PopObj();
	
	void PushObj(T* obj);
private:
	T* m_Origin;
	queue<T*> m_PoolList;
};

template<class T>
inline
ObjectPool<T>::ObjectPool()
{
	m_PoolList.empty();
	/*for (int i = 0; i < 10; i++)
	{
		m_PoolList.emplace(new T(*m_Origin));
	}*/
}

template<class T>
inline
T* ObjectPool<T>::PopObj()
{
	if (m_PoolList.size() != 0)
	{
		T* p;
		p = m_PoolList.front();
		m_PoolList.pop();
		p->SetActive(true);
		return p;
	}
	else
	{
		//todo:重写GameObject复制构造器完成uniquePtr的复制构造
		//m_PoolList.push(new T(*m_Origin));
		return nullptr;
	}
}

template<class T>
inline
void ObjectPool<T>::PushObj(T* obj)
{
	obj->SetActive(false);
	m_PoolList.push(obj);
}