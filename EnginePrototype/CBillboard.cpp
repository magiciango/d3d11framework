#include "CBillboard.h"
#include "Object.h"
#include "DXTrace.h"

size_t CBillboard::tag = typeid(CBillboard).hash_code();
using namespace DirectX;
CBillboard::CBillboard(Actor* obj)
	:Component(obj)
{
	m_tag = CBillboard::tag;
}
void CBillboard::Init(ID3D11Device* device)
{
	VertexPosSize vertex;
	vertex.pos = m_GameObject->GetTransform().GetPosition();
	vertex.size = XMFLOAT2(m_GameObject->GetTransform().GetScale().x,
		m_GameObject->GetTransform().GetScale().y);

	D3D11_BUFFER_DESC vbd;
	ZeroMemory(&vbd, sizeof(vbd));
	vbd.Usage = D3D11_USAGE_DYNAMIC;
	vbd.ByteWidth = sizeof(vertex);
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &vertex;
	HR(device->CreateBuffer(&vbd, &InitData, mPointSpritesBuffer.GetAddressOf()));

	m_Material.ambient = XMFLOAT4(0.4f, 0.4f, 0.4f, 1.0f);
	m_Material.diffuse = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	m_Material.specular = XMFLOAT4(0.1f, 0.1f, 0.1f, 16.0f);
}

void CBillboard::SetTexture(ID3D11ShaderResourceView* texture)
{
	m_pTexture = texture;
}

void CBillboard::Draw()
{
	BasicEffect& effect = BasicEffect::Get();
	//effect.SetRenderBillboard(D3DAppGet::pContext(), false);
	effect.SetMaterial(m_Material);
	effect.SetAnimeFrame(m_AnimeFrame);
	effect.SetTextureArray(m_pTexture.Get());
	effect.SetTextureDiffuse(m_pTexture.Get());
	UINT stride = sizeof(VertexPosSize);
	UINT offset = 0;
	D3DAppGet::pContext()->IASetVertexBuffers(0, 1, mPointSpritesBuffer.GetAddressOf(), &stride, &offset);
	BasicEffect::Get().Apply(D3DAppGet::pContext());
	D3DAppGet::pContext()->Draw(1, 0);
}

void CBillboard::SetAnimeFrame(float frame)
{
	m_AnimeFrame = frame;
}
