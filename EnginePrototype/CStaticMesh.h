#pragma once
#include "Component.h"

#include <unordered_map>
#include <DirectXCollision.h>
#include "assimp/cimport.h"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "assimp/matrix4x4.h"

struct StaticMeshData
{
	template <class T>
	using ComPtr = Microsoft::WRL::ComPtr<T>;

	std::unordered_map<std::string, ID3D11ShaderResourceView*> textures;
	ComPtr<ID3D11Buffer>* m_VertexBuffer;
	ComPtr<ID3D11Buffer>* m_IndexBuffer;
	DirectX::XMVECTOR vMin;
	DirectX::XMVECTOR vMax;
	DirectX::BoundingBox boundingBox;
	const aiScene* m_AiScene = NULL;
};
class StaticMeshReader
{
	template <class T>
	using ComPtr = Microsoft::WRL::ComPtr<T>;
	static std::unordered_map<const char*, std::unique_ptr<StaticMeshData>> SMLibray;
public:
	StaticMeshData* Load(const char* filename);
};

class CStaticMesh :public Component
{
public:
	enum TextureType
	{
		AllModelTexture,				//juse use texture in fbx file,metal and rough with data
		AllFileTexture,					//juse use texture in another file
		MetalAndRoughFileTexture,		//use albedo,normal,height in fbx and metal rough in another file
	};

	static size_t tag;
	CStaticMesh();

	void Draw(IEffect* effect, const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& view, const DirectX::XMMATRIX& proj) override;
	void Load(const char* filename);
	void SetMaterial(Material material);
	void SetMatCapTexture(ID3D11ShaderResourceView* texture);
	void SetUseTextureType(TextureType use);
	void SetPBRMap(ID3D11ShaderResourceView* albedo, ID3D11ShaderResourceView* normal, ID3D11ShaderResourceView* height
		, ID3D11ShaderResourceView* metal, ID3D11ShaderResourceView* rough);
	DirectX::BoundingBox GetBoundingBox() { return m_pData->boundingBox; };
	Material GetMatrial() { return m_Material; };
	StaticMeshData* GetData();
	TextureType GetuseMatCap() { return m_UseTextureType; };

private:
	static StaticMeshReader m_MeshReader;
	Material m_Material;
	StaticMeshData* m_pData;
	ID3D11ShaderResourceView* m_MatCapTexture;
	TextureType m_UseTextureType;
	bool FrustumCulling(DirectX::XMMATRIX world, DirectX::XMMATRIX view, DirectX::XMMATRIX proj);
};


//template<class VertexType, class IndexType>
//inline void CStaticMesh::SetBuffer(const Geometry::MeshData<VertexType, IndexType>& meshData)
//{
//	// 释放旧资源
//	m_pVertexBuffer.Reset();
//	m_pIndexBuffer.Reset();
//
//	ID3D11Device* pDevice = D3DAppGet::pDevice();
//	// 检查D3D设备
//	if (pDevice == nullptr)
//		return;
//
//	// 设置顶点缓冲区描述
//	m_VertexStride = sizeof(VertexType);
//	D3D11_BUFFER_DESC vbd;
//	ZeroMemory(&vbd, sizeof(vbd));
//	vbd.Usage = D3D11_USAGE_IMMUTABLE;
//	vbd.ByteWidth = (UINT)meshData.vertexVec.size() * m_VertexStride;
//	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//	vbd.CPUAccessFlags = 0;
//	// 新建顶点缓冲区
//	D3D11_SUBRESOURCE_DATA InitData;
//	ZeroMemory(&InitData, sizeof(InitData));
//	InitData.pSysMem = meshData.vertexVec.data();
//	pDevice->CreateBuffer(&vbd, &InitData, m_pVertexBuffer.GetAddressOf());
//
//	// 设置索引缓冲区描述
//	m_IndexCount = (UINT)meshData.indexVec.size();
//	D3D11_BUFFER_DESC ibd;
//	ZeroMemory(&ibd, sizeof(ibd));
//	ibd.Usage = D3D11_USAGE_IMMUTABLE;
//	ibd.ByteWidth = m_IndexCount * sizeof(IndexType);
//	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
//	ibd.CPUAccessFlags = 0;
//	// 新建索引缓冲区
//	InitData.pSysMem = meshData.indexVec.data();
//	pDevice->CreateBuffer(&ibd, &InitData, m_pIndexBuffer.GetAddressOf());
//}