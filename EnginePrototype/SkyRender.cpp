#include "SkyRender.h"
#include "Geometry.h"
#include "d3dUtil.h"
#include "DXTrace.h"
#include "d3dApp.h"
#include "TextureManager.h"
#pragma warning(disable: 26812)



using namespace DirectX;
using namespace Microsoft::WRL;

HRESULT SkyRender::InitResource(ID3D11Device* device, ID3D11DeviceContext* deviceContext, const std::wstring& cubemapFilename, float skySphereRadius, bool generateMips)
{
	// 防止重复初始化造成内存泄漏
	m_pSphereIndexBuffer.Reset();
	m_pSphereVertexBuffer.Reset();
	m_pTextureHDRSRV.Reset();

	HRESULT hr;
	// 天空盒纹理加载
	if (cubemapFilename.substr(cubemapFilename.size() - 3) == L"dds")
	{
		hr = CreateDDSTextureFromFile(device,
			generateMips ? deviceContext : nullptr,
			cubemapFilename.c_str(),
			nullptr,
			m_pTextureHDRSRV.GetAddressOf());
	}
	else
	{
		hr = CreateWICTexture2DCubeFromFile(device,
			deviceContext,
			cubemapFilename,
			nullptr,
			m_pTextureHDRSRV.GetAddressOf(),
			generateMips);
	}

	if (FAILED(hr))
		return hr;

	return InitResource(device, skySphereRadius);
}

HRESULT SkyRender::InitResource(ID3D11Device* device, ID3D11DeviceContext* deviceContext, const std::vector<std::wstring>& cubemapFilenames, float skySphereRadius, bool generateMips)
{
	// 防止重复初始化造成内存泄漏
	m_pSphereIndexBuffer.Reset();
	m_pSphereVertexBuffer.Reset();
	m_pTextureHDRSRV.Reset();

	m_pDiffuseMapSRV.Reset();
	for (auto& ptr : m_pDiffuseMapRTVs)
	{
		ptr.Reset();
	}

	m_pPrefilterSRV.Reset();
	for (auto& ptr : m_pPrefilterRTVs)
	{
		ptr.Reset();
	}

	HRESULT hr;
	// 天空盒纹理加载
	hr = CreateWICTexture2DCubeFromFile(device,
		deviceContext,
		cubemapFilenames,
		nullptr,
		m_pTextureHDRSRV.GetAddressOf(),
		generateMips);
	if (FAILED(hr))
		return hr;

	return InitResource(device, skySphereRadius);
}

ID3D11ShaderResourceView* SkyRender::GetTextureCube()
{
	return m_pTextureHDRSRV.Get();
}

ID3D11ShaderResourceView* SkyRender::GetDiffuseCube()
{
	return m_pDiffuseMapSRV.Get();
}

ID3D11ShaderResourceView* SkyRender::GetSpecCube()
{
	return m_pPrefilterSRV.Get();
}

ID3D11ShaderResourceView* SkyRender::GetLutTexture()
{
	return m_pBRDFLutRender->GetOutputTexture();
}

void SkyRender::Draw(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, Camera* camera)
{
	if (!m_GenerateIBL)
	{
		IBLStuff(D3DAppGet::pDevice(), deviceContext, skyEffect, camera);
		D3D11_VIEWPORT vi = camera->GetViewPort();
		deviceContext->RSSetViewports(1, &vi);
		m_GenerateIBL = true;
	}
	skyEffect.SetUseCube(false);
	skyEffect.SetRenderDefault(deviceContext);
	DrawSkyBox(deviceContext, skyEffect, camera, m_pTextureHDRSRV.Get());
}
void SkyRender::DrawSkyBox(ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, Camera* camera, ID3D11ShaderResourceView* textureCube)
{
	UINT strides[1] = { sizeof(VertexPosTex) };
	UINT offsets[1] = { 0 };
	deviceContext->IASetVertexBuffers(0, 1, m_pSphereVertexBuffer.GetAddressOf(), strides, offsets);
	deviceContext->IASetIndexBuffer(m_pSphereIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

	// 抹除平移分量，避免摄像机移动带来天空盒抖动
	XMMATRIX view = camera->GetViewXM();
	view.r[3] = g_XMIdentityR3;

	skyEffect.SetWorldMatrix(XMMatrixIdentity());
	skyEffect.SetViewMatrix(view);
	skyEffect.SetProjMatrix(camera->GetProjXM());
	skyEffect.SetTextureCube(textureCube);
	skyEffect.Apply(deviceContext);
	deviceContext->DrawIndexed(m_SphereIndexCount, 0, 0);
}
void SkyRender::SetDebugObjectName(const std::string& name)
{
#if (defined(DEBUG) || defined(_DEBUG)) && (GRAPHICS_DEBUGGER_OBJECT_NAME)
	// 先清空可能存在的名称
	D3D11SetDebugObjectName(m_pTextureHDRSRV.Get(), nullptr);

	D3D11SetDebugObjectName(m_pTextureHDRSRV.Get(), name + ".CubeMapSRV");
	D3D11SetDebugObjectName(m_pSphereVertexBuffer.Get(), name + ".VertexBuffer");
	D3D11SetDebugObjectName(m_pSphereIndexBuffer.Get(), name + ".IndexBuffer");
#else
	UNREFERENCED_PARAMETER(name);
#endif
}

HRESULT SkyRender::InitResource(ID3D11Device* device, float skySphereRadius)
{
	HRESULT hr;
	m_pCamera = std::make_unique<FirstPersonCamera>();
	#pragma region SphereBuffer
	auto sphere = Geometry::CreateBox<VertexPosTex>(skySphereRadius);
	// Vertex Buffer
	D3D11_BUFFER_DESC vbd;
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.ByteWidth = sizeof(VertexPosTex) * (UINT)sphere.vertexVec.size();
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = sphere.vertexVec.data();

	hr = device->CreateBuffer(&vbd, &InitData, &m_pSphereVertexBuffer);

	// Index Buffer
	m_SphereIndexCount = (UINT)sphere.indexVec.size();

	D3D11_BUFFER_DESC ibd;
	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.ByteWidth = sizeof(DWORD) * m_SphereIndexCount;
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.StructureByteStride = 0;
	ibd.MiscFlags = 0;

	InitData.pSysMem = sphere.indexVec.data();
	hr= device->CreateBuffer(&ibd, &InitData, &m_pSphereIndexBuffer);
	if (FAILED(hr))
		return hr;
#pragma endregion

	#pragma region QuadBuffer
	auto quad = Geometry::Create2DShow<VertexPosTex>();

	// Vertex Buffer
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.ByteWidth = sizeof(VertexPosTex) * (UINT)quad.vertexVec.size();
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;

	InitData.pSysMem = quad.vertexVec.data();

	hr = device->CreateBuffer(&vbd, &InitData, &m_pQuadVertexBuffer);

	// Index Buffer
	m_QuadIndexCount = (UINT)quad.indexVec.size();

	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.ByteWidth = sizeof(DWORD) * m_QuadIndexCount;
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.StructureByteStride = 0;
	ibd.MiscFlags = 0;

	InitData.pSysMem = quad.indexVec.data();
	HR(device->CreateBuffer(&ibd, &InitData, &m_pQuadIndexBuffer));
#pragma endregion

	#pragma region IBLDiffuseCube

	// Create TexArray
	ComPtr<ID3D11Texture2D> texDiffuse;

	D3D11_TEXTURE2D_DESC texDesc;
	texDesc.Width = IradianceMapSize;
	texDesc.Height = IradianceMapSize;
	texDesc.MipLevels = 0;
	texDesc.ArraySize = 6;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS | D3D11_RESOURCE_MISC_TEXTURECUBE;
	HR(device->CreateTexture2D(&texDesc, nullptr, texDiffuse.GetAddressOf()));

	// Create RTV
	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	rtvDesc.Format = texDesc.Format;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
	rtvDesc.Texture2DArray.MipSlice = 0;
	rtvDesc.Texture2DArray.ArraySize = 1;

	// Create RTV for Every Element
	for (int i = 0; i < 6; ++i)
	{
		rtvDesc.Texture2DArray.FirstArraySlice = i;
		HR(device->CreateRenderTargetView(texDiffuse.Get(), &rtvDesc,
			m_pDiffuseMapRTVs[i].GetAddressOf()));
	}

	// Create SRV
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.Format = texDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
	srvDesc.TextureCube.MostDetailedMip = 0;
	srvDesc.TextureCube.MipLevels = -1;	// use All MipMap

	HR(device->CreateShaderResourceView(texDiffuse.Get(), &srvDesc,
		m_pDiffuseMapSRV.GetAddressOf()));
#pragma endregion

	#pragma region PrefilterMap
	// Create TextureArray
	ComPtr<ID3D11Texture2D> PrefilterMap;

	texDesc.Width = PrefilterMapSize;
	texDesc.Height = PrefilterMapSize;
	texDesc.MipLevels = MipLevels;
	texDesc.ArraySize = 6;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS | D3D11_RESOURCE_MISC_TEXTURECUBE;

	HR(device->CreateTexture2D(&texDesc, nullptr, PrefilterMap.GetAddressOf()));

	// Create SRV
	srvDesc.Format = texDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
	srvDesc.TextureCube.MostDetailedMip = 0;
	srvDesc.TextureCube.MipLevels = MipLevels;
	HR(device->CreateShaderResourceView(PrefilterMap.Get(), &srvDesc,
		m_pPrefilterSRV.GetAddressOf()));
	// Loop For Every MipMap
	for (int mip = 0; mip < MipLevels; mip++)
	{
		// Create RTV
		rtvDesc.Format = texDesc.Format;
		rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
		rtvDesc.Texture2DArray.MipSlice = mip;
		rtvDesc.Texture2DArray.ArraySize = 1;

		// RTV for Every Elements
		for (int i = 0; i < 6; ++i)
		{
			rtvDesc.Texture2DArray.FirstArraySlice = i;
			HR(device->CreateRenderTargetView(PrefilterMap.Get(), &rtvDesc,
				m_pPrefilterRTVs[mip * 6 + i].GetAddressOf()));
		}
	}

#pragma endregion

	m_pBRDFLutRender = std::make_unique<TextureRender>();
	m_pBRDFLutRender->InitResource(LUTMapSize, LUTMapSize);
	m_GenerateIBL = false;
	return S_OK;
}

void SkyRender::IBLStuff(ID3D11Device* device, ID3D11DeviceContext* deviceContext, SkyEffect& skyEffect, Camera* camera)
{
#pragma region Diffuse IBL

	// Init ViewPort
	m_pCamera->SetViewPort(0.0f, 0.0f, static_cast<float>(IradianceMapSize), static_cast<float>(IradianceMapSize));

	// Render TextureCube
	Cache(deviceContext);
	for (int i = 0; i < 6; i++) {
		BeginCapture(deviceContext, skyEffect, XMFLOAT3(0, 0, 0), static_cast<D3D11_TEXTURECUBE_FACE>(i));
		// Clear Buffer
		deviceContext->ClearRenderTargetView(m_pDiffuseMapRTVs[i].Get(), reinterpret_cast<const float*>(&Colors::Black));
		deviceContext->OMSetRenderTargets(1, m_pDiffuseMapRTVs[i].GetAddressOf(), nullptr);
		skyEffect.SetRenderIradiance(deviceContext);
		DrawSkyBox(deviceContext, skyEffect, m_pCamera.get(), m_pTextureHDRSRV.Get());
	}
	Restore(deviceContext);
#pragma endregion

#pragma region PrefilterMap
	//MipMap
	for (int mip = 0; mip < MipLevels; mip++)
	{
		unsigned mipWidth = PrefilterMapSize * pow(0.5, mip);
		unsigned mipHeight = PrefilterMapSize * pow(0.5, mip);

		// Init ViewPort
		m_pCamera->SetViewPort(0.0f, 0.0f, static_cast<float>(mipWidth), static_cast<float>(mipHeight));
		float roughness = (float)mip / (float)(MipLevels - 1);

		// Render TextureCube
		Cache(deviceContext);
		for (int i = 0; i < 6; i++) {

			BeginCapture(deviceContext, skyEffect, XMFLOAT3(0, 0, 0), static_cast<D3D11_TEXTURECUBE_FACE>(i));
			skyEffect.SetRenderPrefilter(deviceContext, roughness);
			// Clear Buffer
			deviceContext->ClearRenderTargetView(m_pPrefilterRTVs[mip * 6 + i].Get(), reinterpret_cast<const float*>(&Colors::Black));
			// SetRT
			deviceContext->OMSetRenderTargets(1, m_pPrefilterRTVs[mip * 6 + i].GetAddressOf(), nullptr);
			DrawSkyBox(deviceContext, skyEffect, m_pCamera.get(), m_pTextureHDRSRV.Get());
		}
		Restore(deviceContext);
	}

	
#pragma endregion

#pragma region BRDFLut
	
	m_pBRDFLutRender->Begin(Colors::Black, deviceContext);
	{
		
		skyEffect.SetRenderLut(deviceContext);
		CGeometry test;
		test.SetBuffer(Geometry::Create2DShow<VertexPosTex>());
		test.Draw(&skyEffect, XMMatrixIdentity(), XMMatrixIdentity(), XMMatrixIdentity());
	}
	m_pBRDFLutRender->End(deviceContext);
#pragma endregion
}


void SkyRender::Cache(ID3D11DeviceContext* deviceContext)
{
	deviceContext->OMGetRenderTargets(1, m_pCacheRTV.GetAddressOf(), m_pCacheDSV.GetAddressOf());
}

void SkyRender::BeginCapture(ID3D11DeviceContext* deviceContext, SkyEffect& effect, const XMFLOAT3& pos,
	D3D11_TEXTURECUBE_FACE face, float nearZ, float farZ)
{
	static XMFLOAT3 ups[6] = {
		{ 0.0f, 1.0f, 0.0f },	// +X
		{ 0.0f, 1.0f, 0.0f },	// -X
		{ 0.0f, 0.0f, -1.0f },	// +Y
		{ 0.0f, 0.0f, 1.0f },	// -Y
		{ 0.0f, 1.0f, 0.0f },	// +Z
		{ 0.0f, 1.0f, 0.0f }	// -Z
	};

	static XMFLOAT3 looks[6] = {
		{ 1.0f, 0.0f, 0.0f },	// +X
		{ -1.0f, 0.0f, 0.0f },	// -X
		{ 0.0f, 1.0f, 0.0f },	// +Y
		{ 0.0f, -1.0f, 0.0f },	// -Y
		{ 0.0f, 0.0f, 1.0f },	// +Z
		{ 0.0f, 0.0f, -1.0f },	// -Z
	};

	// SkyBox TexCube Camera
	m_pCamera->LookTo(pos, looks[face], ups[face]);

	m_pCamera->SetFrustum(XM_PIDIV2, 1.0f, nearZ, farZ);
	
	// Set ViewPort
	D3D11_VIEWPORT viewPort = m_pCamera->GetViewPort();
	deviceContext->RSSetViewports(1, &viewPort);
	
}

void SkyRender::Restore(ID3D11DeviceContext* deviceContext)
{
	deviceContext->OMSetRenderTargets(1, m_pCacheRTV.GetAddressOf(), m_pCacheDSV.Get());

	m_pCacheDSV.Reset();
	m_pCacheRTV.Reset();
}

