#include "CGeometry.h"
#include "Object.h"

size_t CGeometry::tag = typeid(CGeometry).hash_code();
CGeometry::CGeometry()
	:Component()
{
	m_tag = CGeometry::tag;
}
CGeometry::CGeometry(Actor* obj)
	:Component(obj)
{
	m_tag = CGeometry::tag;
}


void CGeometry::SetTexture(ID3D11ShaderResourceView* texture)
{
	m_pTexture = texture;
}

void CGeometry::SetMaterial(const Material& material)
{
	m_Material = material;
}

void CGeometry::SetTextureNormal(ID3D11ShaderResourceView* texture)
{
	m_pNormalTexture = texture;
}

void CGeometry::SetTextureMetal(ID3D11ShaderResourceView* texture)
{
	m_pMetallicTexture = texture;
}

void CGeometry::SetTextureRough(ID3D11ShaderResourceView* texture)
{
	m_pRoughTexture = texture;
}

void CGeometry::SetTextureHeight(ID3D11ShaderResourceView* texture)
{
	m_pDepthTexture = texture;
}

//void CGeometry::LoadModel(std::string path)
//{
//	Assimp::Importer importer;
//	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);
//	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
//	{
//		//cout << "ERROR::ASSIMP::" << importer.GetErrorString() << endl;
//		return;
//	}
//
//}

//void CGeometry::Draw(const DirectX::XMMATRIX& W, const DirectX::XMMATRIX& V, const DirectX::XMMATRIX& P)
//{
//	ID3D11DeviceContext* context = D3DAppGet::pContext();
//	// 设置顶点/索引缓冲区
//	UINT strides = m_VertexStride;
//	UINT offsets = 0;
//	D3DAppGet::pContext()->IASetVertexBuffers(0, 1, m_pVertexBuffer.GetAddressOf(), &strides, &offsets);
//	D3DAppGet::pContext()->IASetIndexBuffer(m_pIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
//
//	// 更新数据并应用
//	BasicEffect& effect = BasicEffect::Get();
//	effect.SetRenderNormalMap(context);
//	effect.SetWorldMatrix(W);
//	effect.SetTextureDiffuse(m_pTexture.Get());
//	effect.SetMaterial(m_Material);
//	effect.Apply(D3DAppGet::pContext());
//
//	D3DAppGet::pContext()->DrawIndexed(m_IndexCount, 0, 0);
//}

void CGeometry::Draw(IEffect* effect, const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& view, const DirectX::XMMATRIX& proj)
{
	/*ID3D11Buffer* test;
	std::string name;
	test->SetPrivateData(WKPDID_D3DDebugObjectName, (UINT)name.size(), name.c_str());*/

	ID3D11DeviceContext* context = D3DAppGet::pContext();
	if (FrustumCulling(worldMatrix, view, proj))
		return;
	// 设置顶点/索引缓冲区
	UINT strides = m_VertexStride;
	UINT offsets = 0;
	D3DAppGet::pContext()->IASetVertexBuffers(0, 1, m_pVertexBuffer.GetAddressOf(), &strides, &offsets);
	D3DAppGet::pContext()->IASetIndexBuffer(m_pIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

	IEffectTransform* effectTransform = dynamic_cast<IEffectTransform*>(effect);
	IEffectTextureDiffuse* effectDiffuse = dynamic_cast<IEffectTextureDiffuse*>(effect);
	IEffectTextureNormal* effectNormal = dynamic_cast<IEffectTextureNormal*>(effect);
	IEffectTextureHeight* effectHeight = dynamic_cast<IEffectTextureHeight*>(effect);
	BasicEffect* effectBasic = dynamic_cast<BasicEffect*>(effect);
	//XMFLOAT3 transScale= { }
	// 更新数据并应用
	if(effectTransform)
		effectTransform->SetWorldMatrix(worldMatrix);
	if (effectDiffuse)
	{
		effectDiffuse->SetTextureDiffuse(m_pTexture);
	}
	if (effectHeight)
	{
		effectHeight->SetTextureHeight(m_pDepthTexture);
		effectHeight->SetHeightScale(m_HeightScale);
		effectHeight->SetTessInfo(m_a, m_b, m_c, m_d);
	}
	if (effectNormal)
	{
		effectNormal->SetTextureNormal(m_pNormalTexture);

	}
	//effectDiffuse->SetTextureDiffuse(m_pTexture.Get());
	if (effectBasic)
	{
		//effectBasic->SetRenderMatCap(context);
		effectBasic->SetIsMatCap(false);
		effectBasic->SetMeshType(IEffectMesh::StaticMesh);
		effectBasic->SetMaterial(m_Material);

		//todo:pbrtest
		effectBasic->SetUsePBR(true);
		effectBasic->SetMetalic(m_pMetallicTexture, m_Material.ambient.x);
		effectBasic->SetRoughless(m_pRoughTexture, m_Material.ambient.y);
	}
	effect->SetRenderPass(D3DAppGet::pContext());
	effect->Apply(D3DAppGet::pContext());

	D3DAppGet::pContext()->DrawIndexed(m_IndexCount, 0, 0);
	if (effectDiffuse)
	{
		effectDiffuse->SetTextureDiffuse(nullptr);
	}
	if (effectHeight)
	{
		effectHeight->SetTextureHeight(nullptr);
	}
	if (effectNormal)
	{
		effectNormal->SetTextureNormal(nullptr);
	}
	effect->Apply(D3DAppGet::pContext());
}

bool CGeometry::FrustumCulling(DirectX::XMMATRIX world, DirectX::XMMATRIX view, DirectX::XMMATRIX proj)
{
	BoundingFrustum frustum;
	BoundingFrustum::CreateFromMatrix(frustum, proj);

	BoundingBox box;
	boundingBox.Transform(box, world * view);
	return !frustum.Intersects(box);
}

void CGeometry::Update(float dt)
{
}
