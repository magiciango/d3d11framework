#include "Effects.h"
#include "d3dUtil.h"
#include "EffectHelper.h"	// 必须晚于Effects.h和d3dUtil.h包含
#include "DXTrace.h"
#include "Vertex.h"
using namespace DirectX;

# pragma warning(disable: 26812)

//
// Texture2DEffect::Impl 需要先于ShadowEffect的定义
//

class Texture2DEffect::Impl
{
public:
	// 必须显式指定
	Impl() {}
	~Impl() = default;

public:
	std::unique_ptr<EffectHelper> m_pEffectHelper;

	std::shared_ptr<IEffectPass> m_pCurrEffectPass;

	ComPtr<ID3D11InputLayout> m_pVertexPosNormalTexLayout;

	XMFLOAT4X4 m_World{}, m_View{}, m_Proj{};
};

//
// Texture2DEffect
//

namespace
{
	// DebugEffect单例
	static Texture2DEffect* g_pInstance = nullptr;
}

Texture2DEffect::Texture2DEffect()
{
	if (g_pInstance)
		throw std::exception("Texture2DEffect is a singleton!");
	g_pInstance = this;
	pImpl = std::make_unique<Texture2DEffect::Impl>();
}

Texture2DEffect::~Texture2DEffect()
{
}

Texture2DEffect::Texture2DEffect(Texture2DEffect&& moveFrom) noexcept
{
	pImpl.swap(moveFrom.pImpl);
}

Texture2DEffect& Texture2DEffect::operator=(Texture2DEffect&& moveFrom) noexcept
{
	pImpl.swap(moveFrom.pImpl);
	return *this;
}

Texture2DEffect& Texture2DEffect::Get()
{
	if (!g_pInstance)
		throw std::exception("Texture2DEffect needs an instance!");
	return *g_pInstance;
}

bool Texture2DEffect::InitAll(ID3D11Device* device)
{
	if (!device)
		return false;

	if (!RenderStates::IsInit())
		throw std::exception("RenderStates need to be initialized first!");

	pImpl->m_pEffectHelper = std::make_unique<EffectHelper>();

	ComPtr<ID3DBlob> blob;

	// ******************
	// 创建顶点着色器
	//

	HR(CreateShaderFromFile(L"HLSL\\Texture2D_VS.cso", L"HLSL\\Texture2D_VS.hlsl", "VS", "vs_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Texture2D_VS", device, blob.Get()));
	// 创建顶点布局
	HR(device->CreateInputLayout(VertexPosTex::inputLayout, ARRAYSIZE(VertexPosTex::inputLayout),
		blob->GetBufferPointer(), blob->GetBufferSize(), pImpl->m_pVertexPosNormalTexLayout.GetAddressOf()));

	// ******************
	// 创建像素着色器
	//

	HR(CreateShaderFromFile(L"HLSL\\DebugTextureRGBA_PS.cso", L"HLSL\\DebugTextureRGBA_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("DebugTextureRGBA_PS", device, blob.Get()));

	HR(CreateShaderFromFile(L"HLSL\\DebugTextureOneComp_PS.cso", L"HLSL\\DebugTextureOneComp_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("DebugTextureOneComp_PS", device, blob.Get()));

	HR(CreateShaderFromFile(L"HLSL\\DebugTextureOneCompGray_PS.cso", L"HLSL\\DebugTextureOneCompGray_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("DebugTextureOneCompGray_PS", device, blob.Get()));

	HR(CreateShaderFromFile(L"HLSL\\DebugTextureArray_PS.cso", L"HLSL\\DebugTextureArray_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("DebugTextureArray_PS", device, blob.Get()));

	HR(CreateShaderFromFile(L"HLSL\\DebugTextureCube_PS.cso", L"HLSL\\DebugTextureCube_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("DebugTextureCube_PS", device, blob.Get()));

	HR(CreateShaderFromFile(L"HLSL\\ToneMap_PS.cso", L"HLSL\\ToneMap_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("ToneMap_PS", device, blob.Get()));

	// ******************
	// 创建通道
	//
	EffectPassDesc passDesc;
	passDesc.nameVS = "Texture2D_VS";
	passDesc.namePS = "DebugTextureRGBA_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("DebugTextureRGBA", device, &passDesc));
	passDesc.nameVS = "Texture2D_VS";
	passDesc.namePS = "DebugTextureOneComp_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("DebugTextureOneComp", device, &passDesc));
	passDesc.nameVS = "Texture2D_VS";
	passDesc.namePS = "DebugTextureOneCompGray_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("DebugTextureOneCompGray", device, &passDesc));
	passDesc.nameVS = "Texture2D_VS";
	passDesc.namePS = "DebugTextureArray_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("DebugTextureArray", device, &passDesc));
	passDesc.nameVS = "Texture2D_VS";
	passDesc.namePS = "DebugTextureCube_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("DebugTextureCube", device, &passDesc));

	passDesc.nameVS = "Texture2D_VS";
	passDesc.namePS = "ToneMap_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("ToneMap", device, &passDesc));

	// 设置采样器
	pImpl->m_pEffectHelper->SetSamplerStateByName("g_Sam", RenderStates::SSLinearWrap.Get());
	
	// 设置调试对象名
	D3D11SetDebugObjectName(pImpl->m_pVertexPosNormalTexLayout.Get(), "Texture2DEffect.VertexPosNormalTexLayout");
	pImpl->m_pEffectHelper->SetDebugObjectName("Texture2DEffect");

	return true;
}

void Texture2DEffect::SetRenderDefault(ID3D11DeviceContext* deviceContext)
{
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosNormalTexLayout.Get());
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("DebugTextureRGBA");
	/*pImpl->m_pCurrEffectPass->SetDepthStencilState(RenderStates::DSSLessEqual.Get(), 0);
	pImpl->m_pCurrEffectPass->SetRasterizerState(RenderStates::RSNoCull.Get());*/
}

void Texture2DEffect::SetRenderOneComponent(ID3D11DeviceContext* deviceContext, int index)
{
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosNormalTexLayout.Get());
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("DebugTextureOneComp");
	pImpl->m_pCurrEffectPass->PSGetParamByName("index")->SetSInt(index);
	/*pImpl->m_pCurrEffectPass->SetDepthStencilState(RenderStates::DSSLessEqual.Get(), 0);
	pImpl->m_pCurrEffectPass->SetRasterizerState(RenderStates::RSNoCull.Get());*/
}

void Texture2DEffect::SetRenderOneComponentGray(ID3D11DeviceContext* deviceContext, int index)
{
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosNormalTexLayout.Get());
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("DebugTextureOneCompGray");
	pImpl->m_pCurrEffectPass->PSGetParamByName("index")->SetSInt(index);
	pImpl->m_pCurrEffectPass->SetDepthStencilState(RenderStates::DSSLessEqual.Get(), 0);
	pImpl->m_pCurrEffectPass->SetRasterizerState(RenderStates::RSNoCull.Get());
}

void Texture2DEffect::SetRenderArray(ID3D11DeviceContext* deviceContext, int index)
{
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosNormalTexLayout.Get());
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("DebugTextureArray");
	pImpl->m_pCurrEffectPass->PSGetParamByName("index")->SetSInt(index);
	pImpl->m_pCurrEffectPass->SetDepthStencilState(RenderStates::DSSLessEqual.Get(), 0);
	pImpl->m_pCurrEffectPass->SetRasterizerState(RenderStates::RSNoCull.Get());
}

void Texture2DEffect::SetRenderCube(ID3D11DeviceContext* deviceContext, int index)
{
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosNormalTexLayout.Get());
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("DebugTextureCube");
	pImpl->m_pCurrEffectPass->PSGetParamByName("index")->SetSInt(index);
	pImpl->m_pCurrEffectPass->SetDepthStencilState(RenderStates::DSSLessEqual.Get(), 0);
	pImpl->m_pCurrEffectPass->SetRasterizerState(RenderStates::RSNoCull.Get());
}

void Texture2DEffect::SetRenderToneMap(ID3D11DeviceContext* deviceContext)
{
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosNormalTexLayout.Get());
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("ToneMap");
}

void Texture2DEffect::SetTextureDiffuse(ID3D11ShaderResourceView* textureDiffuse)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_DiffuseMap", textureDiffuse);
}

void Texture2DEffect::SetTextureArray(ID3D11ShaderResourceView* textureDiffuse)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_TextureArray", textureDiffuse);
}

void Texture2DEffect::SetTextureCube(ID3D11ShaderResourceView* textureDiffuse)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_TextureCube", textureDiffuse);
}

void Texture2DEffect::SetScreenSize(float width, float height)
{
	XMFLOAT2 size(width, height);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_ScreenSize")->SetFloatVector(2, (FLOAT*)&size);
}

void Texture2DEffect::SetPosition(const DirectX::XMFLOAT2& pos)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_Pos")->SetFloatVector(2, (FLOAT*)&pos);
}

void Texture2DEffect::SetSize(const DirectX::XMFLOAT2& size)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_Size")->SetFloatVector(2, (FLOAT*)&size);
}

void Texture2DEffect::Apply(ID3D11DeviceContext* deviceContext)
{
	pImpl->m_pCurrEffectPass->Apply(deviceContext);
}

