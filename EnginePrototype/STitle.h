#pragma once
#include "Scene.h"
#include "CWave.h"
#include "ParticleRender.h"
#include "TextureManager.h"
#include "DeferredRender.h"
#include "LightManager.h"
class STitle:public Scene
{
	bool Init() override;
	int UpdateScene(float dt) override;
	void OnResize() override;
	void DrawScene(bool isOpaquePass) override;
	bool InitResource() override; 

private:
	void CreateActor(bool isStaticMesh);
	void EditMode();
	template <class T>
	using ComPtr = Microsoft::WRL::ComPtr<T>;

	//Direct2D用
	//**************************
	ComPtr<ID2D1SolidColorBrush> m_pColorBrush;				   
	ComPtr<IDWriteFont> m_pFont;								
	ComPtr<IDWriteTextFormat> m_pTextFormat;	

	Actor* m_Choose;

	SkyRender m_SkyBox;
	DynamicSkyRender m_DynamicSkyBox;

	std::unique_ptr<TextureRender> m_pShadowMap;
	std::unique_ptr<SSAORender> m_pSSAOMap;

	//DirectionalLight m_DirLights[3] = {};						// 方向光
	//DirectX::XMFLOAT3 m_OriginalLightDirs[3] = {};				// 初始光方向
	//PointLight m_PointLights[10] = {};
	//SpotLight m_SpotLights[10] = {};

	LightManager m_LightMng;
	enum DebugTexture
	{
		None,
		ShadowMap,
		SSAOMap,
	};
	std::unique_ptr<ParticleRender> m_pRain;
	std::unique_ptr<ParticleEffect> m_ParticleEffect;
	std::unique_ptr<DeferredRender> m_DeferredRender;
	DebugTexture m_DebugTexture;

	ComPtr<ID3D11Buffer> m_pQuadVertexBuffer;
	ComPtr<ID3D11Buffer> m_pQuadIndexBuffer;
	UINT m_QuadIndexCount = 0;

	bool m_UseDynamicBox = false;
	bool m_DrawSolid = true;
	bool m_UseShadow = true;
	bool m_UseSSAO = true;
	bool m_UseRain = false;
	bool m_UseToneMapping = true;
	std::unique_ptr<TextureRender> m_pPostProcessRender;
};