#include "Shadow.hlsli"

struct InstancePosNormalTex
{
    float3 PosL : POSITION;
    float3 NormalL : NORMAL;
    float2 Tex : TEXCOORD;
    matrix World : World;
};


VertexPosHTex VS(InstancePosNormalTex vIn)
{
    VertexPosHTex vOut;
    float4 posW = mul(float4(vIn.PosL, 1.0f), vIn.World);
    vOut.PosH = mul(posW, g_ViewProj);
    vOut.Tex = vIn.Tex;

    return vOut;
}
