#include "2DBase.hlsli"

VertexPosHTex VS(VertexPosTex vIn)
{
    VertexPosHTex vOut;
    vOut.PosH = float4(g_Pos / g_ScreenSize + vIn.PosL.xy * (g_Size / 2) / g_ScreenSize, 0.0, 1.0);
    //vOut.PosH = mul(float4(vIn.PosL, 1.0f), g_WorldViewProj);
    vOut.Tex = vIn.Tex;
    return vOut;
}

