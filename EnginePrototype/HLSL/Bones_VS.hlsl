#include "Common.hlsli"
#include "Basic.hlsli"

VertexPosHWNormalTex VS(BonesVertexPosNormalTex vIn)
{
    VertexPosHWNormalTex vOut;

    matrix viewProj = mul(g_View, g_Proj);
    //vector posW;// = mul(float4(vIn.PosL, 1.0f), g_World);
    matrix BoneTransform = Identity;
    BoneTransform = mul(g_Bones[vIn.ID.x], vIn.Weight.x);
    BoneTransform += mul(g_Bones[vIn.ID.y], vIn.Weight.y);
    BoneTransform += mul(g_Bones[vIn.ID.z], vIn.Weight.z);
    BoneTransform += mul(g_Bones[vIn.ID.w], vIn.Weight.w);
    //matrix mat = g_World * BoneTransform;
    vector posW= mul(float4(vIn.PosL, 1.0f),BoneTransform);
    posW = mul(posW, g_World);
    vOut.PosW = posW.xyz;
    vOut.PosH = mul(posW, viewProj);
    vOut.NormalW = mul(vIn.NormalL, (float3x3) g_WorldInvTranspose);
    vOut.Tex = vIn.Tex;
    return vOut;
}