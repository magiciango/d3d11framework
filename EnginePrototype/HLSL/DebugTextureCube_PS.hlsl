#include "2DBase.hlsli"

float4 PS(VertexPosHTex pIn, uniform int index) : SV_Target
{
    float4 color = 0;
    float2 uv = pIn.Tex * 2 - 1;
    if (index == 0)
    {
        color = g_TextureCube.Sample(g_Sam, normalize(float3(1, uv)));
    }
    if(index==1)
        color = g_TextureCube.Sample(g_Sam, normalize(float3(-1, uv)));
    if(index==2)
        color = g_TextureCube.Sample(g_Sam, normalize(float3(uv.x, 1, uv.y)));
    if(index==3)
        color = g_TextureCube.Sample(g_Sam, normalize(float3(uv.x,-1, uv.y)));
    if(index==4)
        color = g_TextureCube.Sample(g_Sam, normalize(float3(uv,1)));
    if(index==5)
        color = g_TextureCube.Sample(g_Sam, normalize(float3(uv,-1)));
    return color;
}
