#include "SkyBox.hlsli"
#include "Common.hlsli"
const float2 invAtan = float2(0.1591, 0.3183);
float2 SampleSphericalMap(float3 v)
{
    float2 uv = float2(atan2(v.x, v.z), asin(v.y));
    uv *= invAtan;
    uv += 0.5;
    return uv;
}

float3 ACESToneMapping(float3 color)
{
    const float A = 2.51f;
    const float B = 0.03f;
    const float C = 2.43f;
    const float D = 0.59f;
    const float E = 0.14f;

    color *= 1.0;
    color = (color * (A * color + B)) / (color * (C * color + D) + E);
    color = pow(color, 1. / 2.2);
    return color;
}
float4 PS(VertexPosHL pIn) : SV_Target
{
    float4 color;
    color = g_TexCube.SampleLevel(g_Sam, pIn.PosL,0);
    return color;
    //return float4(ACESToneMapping(color.xyz), 1.0f);
        
}
