#include "Basic.hlsli"

// 像素着色器
float4 PS(VertexPosHWNormalTex pIn) : SV_Target
{
    float4 texColor;// = g_TexArray.Sample(g_Sam, float3(pIn.Tex,animeFrame));
        texColor = g_Tex.Sample(g_Sam, pIn.Tex);
    clip(texColor.a - 0.05f);

    // 标准化法向量
    pIn.NormalW = normalize(pIn.NormalW);

    // 求出顶点指向眼睛的向量，以及顶点与眼睛的距离
    float3 toEyeW = normalize(g_EyePosW - pIn.PosW);
    float distToEye = distance(g_EyePosW, pIn.PosW);

    // 初始化为0 
    float4 ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 spec = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 A = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 D = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 S = float4(0.0f, 0.0f, 0.0f, 0.0f);

    [unroll]
    for (int i = 0; i < 1; ++i)
    {
        ComputeDirectionalLight(g_Material, g_DirLight[i], pIn.NormalW, toEyeW, A, D, S);
        ambient += A;
        diffuse += D;
        spec += S;
    }
    
    [unroll]
    for (i = 0; i < 60; ++i)
    {
        ComputePointLight(g_Material, g_PointLight[i], pIn.PosW, pIn.NormalW, toEyeW, A, D, S);
        //ComputePointLight_Deferred(g_PointLight[i], pIn.PosW, pIn.NormalW, toEyeW, g_Material.Specular.w, A, D, S);
        ambient += A;
        diffuse += D;
        spec += S;
    }

    //[unroll]
    //for (i = 0; i < 5; ++i)
    //{
    //    ComputeSpotLight(g_Material, g_SpotLight[i], pIn.PosW, pIn.NormalW, toEyeW, A, D, S);
    //    ambient += A;
    //    diffuse += D;
    //    spec += S;
    //}

    float4 litColor;
    if (g_ColorBlend)
    {
        litColor = (texColor + g_Color) / 2;
    }
    else
    {
        litColor = texColor;
    }
    //if (g_ReflectionEnabled)
    //{
    //    float3 incident = -toEyeW;
    //    float3 reflectionVector = reflect(incident, pIn.NormalW);
    //    float4 reflectionColor = g_TexCube.Sample(g_Sam, reflectionVector) * g_Material.Reflect.w;

    //    litColor = litColor * (ambient + diffuse + g_Material.Reflect * reflectionColor) + spec;
    //}
    //else
    //{
    //    litColor = litColor * (ambient + diffuse) + spec;
    //}
    litColor = litColor * (ambient + diffuse) + spec;
    // 雾效部分
    [flatten]
    if (g_FogEnabled)
    {
        // 限定在0.0f到1.0f范围
        float fogLerp = saturate((distToEye - g_FogStart) / g_FogRange);
        // 根据雾色和光照颜色进行线性插值
        litColor = lerp(litColor, g_FogColor, fogLerp);
    }

    litColor.a = texColor.a * g_Material.Diffuse.a;
    //return spec;
    return litColor;

}
