#include "Atomosphere.hlsli"

float noise(float t)
{
	float x, y, z;
	g_NoiseTexture.GetDimensions(0, x, y, z);
	return g_NoiseTexture.Sample(g_Sam, float2(frac(t/200), 0.0f)).x;
}

float noise(float2 t)
{
	float x, y, z;
	g_NoiseTexture.GetDimensions(0, x, y, z);
	return g_NoiseTexture.Sample(g_Sam, t / float2(x, y)).x;
}
//uv TextureUV
//pos SunPos in 2D
float3 lensflare(float2 uv, float2 pos,float z)
{
	if(z<0)
    {
        return float3(0, 0, 0);
    }
		
	
	float2 main = uv - pos;
    float2 uvd = uv * (length(uv))*1.3;

	float ang = atan2(main.x, main.y);
	float dist = length(main);
	dist = pow(dist, .1);

	float f0 = 1.0 / (length(uv - pos) * 30 + 1.0);

	//f0 = f0 + f0 * (sin(noise(sin(ang * 3 + pos.x*2) * 4.0 - cos(ang * 4 + pos.y*2)) * 18) * .1 + dist * .7);

	float f1 = max(0.01 - pow(length(uv + 1.2 * pos), 1.9), .0) * 7.0;

	float f2 = max(1.0 / (1.0 + 32.0 * pow(length(uvd + 0.8 * pos), 2.0)), .0) * 0.25;
	float f22 = max(1.0 / (1.0 + 32.0 * pow(length(uvd + 0.85 * pos), 2.0)), .0) * 0.23;
	float f23 = max(1.0 / (1.0 + 32.0 * pow(length(uvd + 0.9 * pos), 2.0)), .0) * 0.21;

	float2 uvx = lerp(uv, uvd, -0.5);

	float f4 = max(0.01 - pow(length(uvx + 0.4 * pos), 2.4), .0) * 6.0;
	float f42 = max(0.01 - pow(length(uvx + 0.45 * pos), 2.4), .0) * 5.0;
	float f43 = max(0.01 - pow(length(uvx + 0.5 * pos), 2.4), .0) * 3.0;

	uvx = lerp(uv, uvd, -0.4);

	float f5 = max(0.01 - pow(length(uvx + 0.2 * pos), 5.5), .0) * 2.0;
	float f52 = max(0.01 - pow(length(uvx + 0.4 * pos), 5.5), .0) * 2.0;
	float f53 = max(0.01 - pow(length(uvx + 0.6 * pos), 5.5), .0) * 2.0;

	uvx = lerp(uv, uvd, -0.5);

	float f6 = max(0.01 - pow(length(uvx - 0.3 * pos), 1.6), .0) * 6.0;
	float f62 = max(0.01 - pow(length(uvx - 0.325 * pos), 1.6), .0) * 3.0;
	float f63 = max(0.01 - pow(length(uvx - 0.35 * pos), 1.6), .0) * 5.0;

	float3 c = float3(0, 0, 0);

	c.r += f2+f4 + f5 + f6;
	c.g += f22+f42 + f52 + f62;
	c.b += f23+f43 + f53 + f63;
	c = c * 1.3 - float3(length(uvd) * .05, length(uvd) * .05, length(uvd) * .05);
	if (c.r < 0)
		c.r = 0;
	if (c.g < 0)
		c.g = 0;
	if (c.b < 0)
		c.b = 0;
	c += float3(f0, f0, f0);

	return c;
}

float3 cc(float3 color, float factor, float factor2)
{
	float w = color.x + color.y + color.z;
	return lerp(color, float3(w, w, w) * factor, w * factor2);
}

float4 PS(VertexPosHL pIn) : SV_TARGET
{
	float4 outColor;
	float2 uv = pIn.Tex; //fragCoord.xy / iResolution.xy - 0.5;
    uv -= float2(0.5, 0.5);
    float3 color = float3(1.4, 1.2, 1.0) * lensflare(uv, float2(pIn.PosH.x - 0.5, pIn.PosH.y), pIn.PosH.z);
	//color -= noise(uv) * 0.015;
	color = cc(color, .5, .1);
	//color = gamma(color, 1);
	outColor = float4(0, 0.3, 0.7, 1);
	outColor += float4(color, 1);
	return outColor;

}