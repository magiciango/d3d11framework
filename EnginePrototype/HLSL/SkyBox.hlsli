
TextureCube g_TexCube : register(t0);

SamplerState g_Sam : register(s0);

cbuffer CBChangesEveryFrame : register(b0)
{
    matrix g_WorldViewProj;
    bool g_DrawCube;
    float3 pad;
}

struct VertexPos
{
    float3 PosL : POSITION;
};

struct VertexPosHL
{
    float4 PosH : SV_POSITION;
    float3 PosL : POSITION;
    float2 Tex : TEXCOORD0;
};

struct VertexPosLTex
{
    float3 PosL : POSITION;
    float2 Tex : TEXCOORD0;
};

struct VertexPosTex
{
    float4 Pos : SV_Position;
    float2 Tex : TEXCOORD0;
};