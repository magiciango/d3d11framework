#include "Basic.hlsli"

float4 PS(BillboardVertex pIn) : SV_Target
{
    float4 texColor;// = g_TexArray.Sample(g_Sam, float3(pIn.Tex,animeFrame));
    //アニメションでないとTextureを使って、アニメションの場合TextureArrayを使う
    //一枚のテクスチャファイルから複数のResourceView作るやり方まだ出来てないので事前にテクスチャファイルを複数カットした

        texColor = g_Tex.Sample(g_Sam, pIn.Tex);

    //Alphaカット、黒い部分もカット(alpha情報ないテクスチャあるので)
    clip(texColor.a - 0.05f);
    clip(texColor.r + texColor.g + texColor.b - 0.1);

    //ベクトルnormalize化
    pIn.NormalW = normalize(pIn.NormalW);

    // 頂点から目のベクトルと距離
    float3 toEyeW = normalize(g_EyePosW - pIn.PosW);
    float distToEye = distance(g_EyePosW, pIn.PosW);

    // 照明(ビルボードはunlit)
    /*float4 ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 spec = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 A = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 D = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 S = float4(0.0f, 0.0f, 0.0f, 0.0f);

    [unroll]
    for (int i = 0; i < 4; ++i)
    {
        ComputeDirectionalLight(g_Material, g_DirLight[i], pIn.NormalW, toEyeW, A, D, S);
        ambient += A;
        diffuse += D;
        spec += S;
    }*/

    float4 litColor = texColor;// *(ambient + diffuse) + spec;

    // 霧効果
    [flatten]
    if (g_FogEnabled)
    {
        //霧の色とピクセルの色Lerpで合わせる
        float fogLerp = saturate((distToEye - g_FogStart) / g_FogRange);
        litColor = lerp(litColor, g_FogColor, fogLerp);
    }

    //litColor.a = texColor.a * g_Material.Diffuse.a;
    return litColor;
}