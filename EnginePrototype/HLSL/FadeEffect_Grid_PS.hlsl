#include "FadeEffect.hlsli"
#define TRANSITION_TYPE 3

float2 plane(float3 p, float3 d, float3 normal)
{
    float3 up = float3(0, 1, 0);
    float3 right = cross(up, normal);

    float dn = dot(d, normal);
    float pn = dot(p, normal);

    float3 hit = p - d / dn * pn;

    float2 uv;
    uv.x = dot(hit, right);
    uv.y = dot(hit, up);

    return uv;
}

float4 main(VertexPosHTex pIn) : SV_Target
{
    float2 xy = pIn.Tex * g_ScreenSize;
    float2 grid_width = 64;
    xy /= grid_width;
    float2 grid = floor(xy);
    xy = fmod(xy, 1.0) - 0.5;
    
    float2 mouse_grid = floor(g_MousePosition / grid_width);
    float max_offset = floor(g_ScreenSize.x / grid_width + g_ScreenSize.y / grid_width);
    float alpha = 0.0;//iMouse.x / iResolution.x;
    float offset = (abs(grid.x - mouse_grid.x) + abs(grid.y - mouse_grid.y)) * 0.1;
    float time = g_FadeAmount * (max_offset * 0.1 + 1) - offset;
    time = clamp(time, 0, 1.);
    alpha += smoothstep(0.0, 1.0, time);
    alpha += 1.0 - smoothstep(3.0, 4.0, time);
    alpha = abs(fmod(alpha, 2.0) - 1.0);
    float side = step(0.5, alpha);

    alpha = radians(alpha * 180.0);
    float4 n = float4(cos(alpha), 0, sin(alpha), -sin(alpha));
    float3 d = float3(1.0, xy.y, xy.x);
    float3 p = float3(-1.0 + n.w / 4.0, 0, 0);
    float2 uv = plane(p, d, n.xyz);

    uv += 0.5;
    if (uv.x < 0.0 || uv.y < 0.0 || uv.x > 1.0 || uv.y > 1.0)
    {
        return float4(0, 0, 0, 1);
    }

    float2 guv = grid * grid_width;
    float2 scale = grid_width / g_ScreenSize;
	//float4 c1 = g_Tex.Sample(g_Sam, guv + float2(1.0 - uv.x/1280, uv.y/720));
    float4 c1 = g_NextTex.Sample(g_Sam, (grid + float2(1 - uv.x, uv.y)) * scale);
    //texture(iChannel0, guv + float2(1.0 - uv.x, uv.y) * scale);
    float4 c2 = g_Tex.Sample(g_Sam, (grid + float2(uv.x, uv.y)) * scale);
	float4 outColor = lerp(c1, c2, side);
    //fragColor = mix(c1, c2, side);
    return outColor;
}