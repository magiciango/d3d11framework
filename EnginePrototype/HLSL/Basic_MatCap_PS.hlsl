#include "Basic.hlsli"
float3 NormalSampleToWorldSpace(float3 normalMapSample,
    float3 unitNormalW,
    float4 tangentW)
{
    // 将读取到法向量中的每个分量从[0, 1]还原到[-1, 1]
    float3 normalT = 2.0f * normalMapSample - 1.0f;

    // 构建位于世界坐标系的切线空间
    float3 N = unitNormalW;
    float3 T = normalize(tangentW.xyz - dot(tangentW.xyz, N) * N); // 施密特正交化
    float3 B = cross(N, T);

    float3x3 TBN = float3x3(T, B, N);

    // 将凹凸法向量从切线空间变换到世界坐标系
    float3 bumpedNormalW = mul(normalT, TBN);

    return bumpedNormalW;
}
float4 PS(VertexPosHWNormalTex pIn) : SV_TARGET
{
    float4 texColor; // = g_TexArray.Sample(g_Sam, float3(pIn.Tex,animeFrame));
    
    // 标准化法向量
    pIn.NormalW = normalize(pIn.NormalW);

    // 求出顶点指向眼睛的向量，以及顶点与眼睛的距离
    float3 toEyeW = normalize(g_EyePosW - pIn.PosW);
    float distToEye = distance(g_EyePosW, pIn.PosW);
    
    // 法线映射
    float3 bumpedNormalW = normalize(pIn.NormalW);
    [flatten]
    if (g_UseNormalTexture)
    {
        float3 normalMapSample = g_NormalTex.Sample(g_Sam, pIn.Tex).rgb;
        bumpedNormalW = NormalSampleToWorldSpace(normalMapSample, pIn.NormalW, pIn.TangentW);
    }
    texColor = g_Tex.Sample(g_Sam, float2(bumpedNormalW.x * 0.5 + 0.5, bumpedNormalW.y * 0.5 + 0.5));
    clip(texColor.a - 0.05f);
    float shadow[5] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };
    // 仅第一个方向光用于计算阴影
    if(g_UseShadow)
        shadow[0] = CalcShadowFactor(g_SamShadow, g_ShadowMap, pIn.ShadowPosH);
    
    float ambientAccess = 1.0f;
    if (g_EnableSSAO)
    {
        pIn.SSAOPosH /= pIn.SSAOPosH.w;
        ambientAccess = g_SSAOMap.SampleLevel(g_Sam, pIn.SSAOPosH.xy, 0.0f).r;
    }
    // 初始化为0 
    float4 ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 spec = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 A = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 D = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 S = float4(0.0f, 0.0f, 0.0f, 0.0f);

    
    //return float4(shadow[0], 0, 0, 1);
    [unroll]
    for (int i = 0; i < 3; ++i)
    {
        ComputeDirectionalLight(g_Material, g_DirLight[i], bumpedNormalW, toEyeW, A, D, S);
        ambient += ambientAccess * A;
        diffuse += shadow[i] * D;
        spec += shadow[i] * S;
    }
    [unroll]
    for (i = 0; i < 5; ++i)
    {
        ComputePointLight(g_Material, g_PointLight[i], pIn.PosW, bumpedNormalW, toEyeW, A, D, S);
        //ComputePointLight_Deferred(g_PointLight[i], pIn.PosW, pIn.NormalW, toEyeW, g_Material.Specular.w, A, D, S);
        ambient += A;
        diffuse += D;
        spec += S;
    }
    
    [unroll]
    for (i = 0; i < 5; ++i)
    {
        ComputeSpotLight(g_Material, g_SpotLight[i], pIn.PosW, pIn.NormalW, toEyeW, A, D, S);
        ambient += A;
        diffuse += D;
        spec += S;
    }

    float4 litColor;
    if (g_ColorBlend)
    {
        litColor = (texColor + g_Color) / 2;
    }
    else
    {
        litColor = texColor;
    }
    //if (g_ReflectionEnabled)
    //{
    //    float3 incident = -toEyeW;
    //    float3 reflectionVector = reflect(incident, bumpedNormalW);
    //    float4 reflectionColor = g_TexCube.Sample(g_Sam, reflectionVector) * g_Material.Reflect.w;

    //    litColor = litColor * (ambient + diffuse + g_Material.Reflect * reflectionColor) + spec;
    //}
    //else
    //{
    //    litColor = litColor * (ambient + diffuse) + spec;
    //}
    litColor = litColor * (ambient + diffuse) + spec;
    // 雾效部分
    [flatten]
    if (g_FogEnabled)
    {
        // 限定在0.0f到1.0f范围
        float fogLerp = saturate((distToEye - g_FogStart) / g_FogRange);
        // 根据雾色和光照颜色进行线性插值
        litColor = lerp(litColor, g_FogColor, fogLerp);
    }

    litColor.a = texColor.a * g_Material.Diffuse.a;
    //return spec;
    litColor = float4(ACESToneMapping(litColor.xyz), litColor.a);
    return litColor;
}