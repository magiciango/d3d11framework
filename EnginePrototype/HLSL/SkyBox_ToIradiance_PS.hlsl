#include "Common.hlsli"
#include "SkyBox.hlsli"

float4 PS(VertexPosHL pIn) : SV_Target
{
    float3 N = normalize(pIn.PosL);
    float3 irradiance = float3(0.0, 0.0, 0.0);
    float3 up = float3(0.0, 1.0, 0.0);
    float3 right = cross(up, N);
    up = cross(N, right);
 
    float sampleDelta = 0.025;
    float nrSamples = 0.0;
    for (float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta)
    {
        for (float theta = 0.0; theta < 0.5 * PI; theta += sampleDelta)
        {
		// spherical to cartesian (in tangent space)
            float3 tangentSample = float3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));
            float3 sampleVec = tangentSample.x * right + tangentSample.y * up + tangentSample.z * N;
			
            irradiance += g_TexCube.SampleLevel(g_Sam, sampleDelta, 0).xyz * cos(theta) * sin(theta);
            nrSamples += 1.0;
        }
    }
    irradiance = PI * irradiance * (1.0 / float(nrSamples));
    return float4(irradiance, 1.0f);
}