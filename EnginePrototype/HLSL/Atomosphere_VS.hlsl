#include "Atomosphere.hlsli"

float3 getRay(float2 uv)
{
    uv.y = 1 - uv.y;
    uv = uv * 2 - 1;
    uv.x *= g_ScreenSize.x / g_ScreenSize.y;
    float3 proj = normalize(float3(uv.x, uv.y, 1.0));
    float3 ray = mul(float4(proj, 1.0), g_View).xyz;
    return ray;
}

VertexRay VS(VertexPosTex vIn)
{
    VertexRay vOut;
    //vIn.PosL.z = 0.9f;
    //float4 posH = mul(float4(vIn.PosL, 1.0f), g_World * g_View * g_Proj);
    vOut.Ray = getRay(vIn.Tex);
    vOut.PosH = float4(vIn.PosL, 1.0f).xyww;
    //vOut.Tex = vIn.Tex; // / 2 + float2(0.5, 0.5);
    return vOut;
}