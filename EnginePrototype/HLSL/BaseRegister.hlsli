#include "LightHelper.hlsli"

//************************************************
//頂点構造体と計算関数
//
//************************************************
cbuffer Base : register(b11)
{
    float2 g_ScreenSize;
    float g_Gamma;
    float g_Exposure;
}

//*********************
//VertexIn
//*********************
struct VertexPosNormalTex
{
    float3 PosL : POSITION;
    float3 NormalL : NORMAL;
    float2 Tex : TEXCOORD;
};

struct VertexPosNormalTangentTex
{
    float3 PosL : POSITION;
    float3 NormalL : NORMAL;
    float4 TangentL : TANGENT;
    float2 Tex : TEXCOORD;
};

struct VertexPosTex
{
    float3 PosL : POSITION;
    float2 Tex : TEXCOORD;
};

//*********************
//PixelIn
//*********************
struct VertexPosHWNormalTex
{
    float4 PosH : SV_POSITION;
    float3 PosW : POSITION; // 在世界中的位置
    float3 NormalW : NORMAL; // 法向量在世界中的方向
    float4 TangentW : TANGENT; // 切线在世界中的方向
    float2 Tex : TEXCOORD;
    float4 ShadowPosH : TEXCOORD1;
    float4 SSAOPosH : TEXCOORD2;
};

struct PointSprite
{
    float3 PosW : POSITION;
    float2 SizeW : SIZE;
};

struct BillboardVertex
{
    float4 PosH : SV_POSITION;
    float3 PosW : POSITION;
    float3 NormalW : NORMAL;
    float2 Tex : TEXCOORD;
    uint PrimID : SV_PrimitiveID;
};

struct VertexPosHTex
{
    float4 PosH : SV_POSITION;
    float2 Tex : TEXCOORD;
};

struct BonesVertexPosNormalTex
{
    float3 PosL : POSITION;
    float3 NormalL : NORMAL;
    float2 Tex : TEXCOORD;
    float4 ID : COLOR;
    float4 Weight : WEIGHT;
};

struct BonesVertexPosNormalTangentTex
{
    float3 PosL : POSITION;
    float3 NormalL : NORMAL;
    float4 TangentL : TANGENT;
    float2 Tex : TEXCOORD;
    float4 ID : COLOR;
    float4 Weight : WEIGHT;
};

//*********************
//Hull Domain In
//*********************
struct TessVertex
{
    float3 PosW : POSITION;
    float3 NormalW : NORMAL;
    float4 TangentW : TANGENT;
    float2 Tex : TEXCOORD0;
    float TessFactor : TESS;
};

struct PatchTess
{
    float EdgeTess[3] : SV_TessFactor;
    float InsideTess : SV_InsideTessFactor;
};

struct HullOut
{
    float3 PosW : POSITION;
    float3 NormalW : NORMAL;
    float4 TangentW : TANGENT;
    float2 Tex : TEXCOORD;
};



//*********************
//Random Function
//*********************
float oct(float p)
{
    return frac(4768.1232345456 * sin(p));
}
float oct(float2 p)
{
    return frac(4768.1232345456 * sin((p.x + p.y * 43.0)));
}
float oct(float3 p)
{
    return frac(4768.1232345456 * sin((p.x + p.y * 43.0 + p.z * 137.0)));
}
float oct(float4 p)
{
    return frac(4768.1232345456 * sin((p.x + p.y * 43.0 + p.z * 137.0 + p.w * 2666.0)));
}

float achnoise(float x)
{
    float p = floor(x);
    float fr = frac(x);
    float L = p;
    float R = p + 1.0;
	
    float Lo = oct(L);
    float Ro = oct(R);
	
    return lerp(Lo, Ro, fr);
}

float achnoise(float2 x)
{
    float2 p = floor(x);
    float2 fr = frac(x);
    float2 LB = p;
    float2 LT = p + float2(0.0, 1.0);
    float2 RB = p + float2(1.0, 0.0);
    float2 RT = p + float2(1.0, 1.0);
	
    float LBo = oct(LB);
    float RBo = oct(RB);
    float LTo = oct(LT);
    float RTo = oct(RT);
	
    float noise1d1 = lerp(LBo, RBo, fr.x);
    float noise1d2 = lerp(LTo, RTo, fr.x);
	
    float noise2d = lerp(noise1d1, noise1d2, fr.y);
	
    return noise2d;
}
float achnoise(float3 x)
{
    float3 p = floor(x);
    float3 fr = frac(x);
    float3 LBZ = p + float3(0.0, 0.0, 0.0);
    float3 LTZ = p + float3(0.0, 1.0, 0.0);
    float3 RBZ = p + float3(1.0, 0.0, 0.0);
    float3 RTZ = p + float3(1.0, 1.0, 0.0);
	                   
    float3 LBF = p + float3(0.0, 0.0, 1.0);
    float3 LTF = p + float3(0.0, 1.0, 1.0);
    float3 RBF = p + float3(1.0, 0.0, 1.0);
    float3 RTF = p + float3(1.0, 1.0, 1.0);
	
    float l0candidate1 = oct(LBZ);
    float l0candidate2 = oct(RBZ);
    float l0candidate3 = oct(LTZ);
    float l0candidate4 = oct(RTZ);
	
    float l0candidate5 = oct(LBF);
    float l0candidate6 = oct(RBF);
    float l0candidate7 = oct(LTF);
    float l0candidate8 = oct(RTF);
	
    float l1candidate1 = lerp(l0candidate1, l0candidate2, fr[0]);
    float l1candidate2 = lerp(l0candidate3, l0candidate4, fr[0]);
    float l1candidate3 = lerp(l0candidate5, l0candidate6, fr[0]);
    float l1candidate4 = lerp(l0candidate7, l0candidate8, fr[0]);
	
	
    float l2candidate1 = lerp(l1candidate1, l1candidate2, fr[1]);
    float l2candidate2 = lerp(l1candidate3, l1candidate4, fr[1]);
	
	
    float l3candidate1 = lerp(l2candidate1, l2candidate2, fr[2]);
	
    return l3candidate1;
}
float achnoise(float4 x)
{
    float4 p = floor(x);
    float4 fr = frac(x);
    float4 LBZU = p + float4(0.0, 0.0, 0.0, 0.0);
    float4 LTZU = p + float4(0.0, 1.0, 0.0, 0.0);
    float4 RBZU = p + float4(1.0, 0.0, 0.0, 0.0);
    float4 RTZU = p + float4(1.0, 1.0, 0.0, 0.0);
	                 
    float4 LBFU = p + float4(0.0, 0.0, 1.0, 0.0);
    float4 LTFU = p + float4(0.0, 1.0, 1.0, 0.0);
    float4 RBFU = p + float4(1.0, 0.0, 1.0, 0.0);
    float4 RTFU = p + float4(1.0, 1.0, 1.0, 0.0);
	                 
    float4 LBZD = p + float4(0.0, 0.0, 0.0, 1.0);
    float4 LTZD = p + float4(0.0, 1.0, 0.0, 1.0);
    float4 RBZD = p + float4(1.0, 0.0, 0.0, 1.0);
    float4 RTZD = p + float4(1.0, 1.0, 0.0, 1.0);
	                 
    float4 LBFD = p + float4(0.0, 0.0, 1.0, 1.0);
    float4 LTFD = p + float4(0.0, 1.0, 1.0, 1.0);
    float4 RBFD = p + float4(1.0, 0.0, 1.0, 1.0);
    float4 RTFD = p + float4(1.0, 1.0, 1.0, 1.0);
	
    float l0candidate1 = oct(LBZU);
    float l0candidate2 = oct(RBZU);
    float l0candidate3 = oct(LTZU);
    float l0candidate4 = oct(RTZU);
	
    float l0candidate5 = oct(LBFU);
    float l0candidate6 = oct(RBFU);
    float l0candidate7 = oct(LTFU);
    float l0candidate8 = oct(RTFU);
	
    float l0candidate9 = oct(LBZD);
    float l0candidate10 = oct(RBZD);
    float l0candidate11 = oct(LTZD);
    float l0candidate12 = oct(RTZD);
	
    float l0candidate13 = oct(LBFD);
    float l0candidate14 = oct(RBFD);
    float l0candidate15 = oct(LTFD);
    float l0candidate16 = oct(RTFD);
	
    float l1candidate1 = lerp(l0candidate1, l0candidate2, fr[0]);
    float l1candidate2 = lerp(l0candidate3, l0candidate4, fr[0]);
    float l1candidate3 = lerp(l0candidate5, l0candidate6, fr[0]);
    float l1candidate4 = lerp(l0candidate7, l0candidate8, fr[0]);
    float l1candidate5 = lerp(l0candidate9, l0candidate10, fr[0]);
    float l1candidate6 = lerp(l0candidate11, l0candidate12, fr[0]);
    float l1candidate7 = lerp(l0candidate13, l0candidate14, fr[0]);
    float l1candidate8 = lerp(l0candidate15, l0candidate16, fr[0]);
	
	
    float l2candidate1 = lerp(l1candidate1, l1candidate2, fr[1]);
    float l2candidate2 = lerp(l1candidate3, l1candidate4, fr[1]);
    float l2candidate3 = lerp(l1candidate5, l1candidate6, fr[1]);
    float l2candidate4 = lerp(l1candidate7, l1candidate8, fr[1]);
	
	
    float l3candidate1 = lerp(l2candidate1, l2candidate2, fr[2]);
    float l3candidate2 = lerp(l2candidate3, l2candidate4, fr[2]);
	
    float l4candidate1 = lerp(l3candidate1, l3candidate2, fr[3]);
	
    return l4candidate1;
}

//追跡ランダム(マジックナンバーしかねぇ)
float2x2 mm2(in float a)
{
    float c = cos(a), s = sin(a);
    return float2x2(c, s, -s, c);
}
float2x2 m2 = float2x2(0.95534, 0.29552, -0.29552, 0.95534);
float tri(in float x)
{
    return clamp(abs(frac(x) - .5), 0.01, 0.49);
}
float2 tri2(in float2 p)
{
    return float2(tri(p.x) + tri(p.y), tri(p.y + tri(p.x)));
}
float triNoise2d(in float2 p, float spd, float time)
{
    float z = 1.8;
    float z2 = 2.5;
    float rz = 0.;
    p = mul(p, mm2(p.x * 0.06));
    float2 bp = p;
    for (float i = 0.; i < 5.; i++)
    {
        float2 dg = tri2(bp * 1.85) * .75;
        dg = mul(dg, mm2(time * spd));
        p -= dg / z2;

        bp *= 1.3;
        z2 *= .45;
        z *= .42;
        p *= 1.21 + (rz - 1.0) * .02;
        
        rz += tri(p.x + tri(p.y)) * z;
        p = mul(p, -m2);
    }
    return clamp(1. / pow(rz * 29., 1.3), 0., .55);
}




//*********************
//ToneMapping and Gamma
//*********************
static float gamma = 2.2;
static float resource = 5.0;
float3 aces_tonemap(float3 color)
{
    float3x3 m1 = float3x3(
        0.59719, 0.07600, 0.02840,
        0.35458, 0.90834, 0.13383,
        0.04823, 0.01566, 0.83777
	);
    float3x3 m2 = float3x3(
        1.60475, -0.10208, -0.00327,
        -0.53108, 1.10813, -0.07276,
        -0.07367, -0.00605, 1.07602
	);
    float3 v = mul(m1, color);
    float3 a = v * (v + 0.0245786) - 0.000090537;
    float3 b = v * (0.983729 * v + 0.4329510) + 0.238081;
    return pow(clamp(mul(m2, (a / b)), 0.0, 1.0), 1.0 / g_Gamma);
}
float3 Uncharted2ToneMapping(float3 color)
{
    float A = 0.15;
    float B = 0.50;
    float C = 0.10;
    float D = 0.20;
    float E = 0.02;
    float F = 0.30;
    float W = 11.2;
    float exposure = g_Exposure;
    color *= exposure;
    color = ((color * (A * color + C * B) + D * E) / (color * (A * color + B) + D * F)) - E / F;
    float white = ((W * (A * W + C * B) + D * E) / (W * (A * W + B) + D * F)) - E / F;
    color /= white;
    color = pow(color, 1. / g_Gamma);
    return color;
}
float3 linearToneMapping(float3 color)
{
    float exposure = g_Exposure;
    color = clamp(exposure * color, 0., 1.);
    color = pow(color, 1. / g_Gamma);
    return color;
}
float3 ACESToneMapping(float3 color)
{
    const float A = 2.51f;
    const float B = 0.03f;
    const float C = 2.43f;
    const float D = 0.59f;
    const float E = 0.14f;

    color *= 1.5;
    color = (color * (A * color + B)) / (color * (C * color + D) + E);
    color = pow(color, 1. / 2.2);
    return color;
}
