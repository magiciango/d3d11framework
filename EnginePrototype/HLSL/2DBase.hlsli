#include "BaseRegister.hlsli"

Texture2D g_DiffuseMap : register(t0);
Texture2DArray g_TextureArray : register(t1);
TextureCube g_TextureCube : register(t2);
SamplerState g_Sam : register(s0);

cbuffer cbtransform
{
    float2 g_Pos;
    float2 g_Size;
};
