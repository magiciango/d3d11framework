#include "Basic.hlsli"
#include "BRDF.hlsli"

float3 ComputeDirectionalLight_PBR(DirectionalLight light, float3 posW, float3 eyePos, float3 albedo, float3 normal, float roughness, float metallic, float3 F0)
{
    float3 L = normalize(-light.Direction);
    float3 V = normalize(eyePos - posW);
    float3 H = normalize(V + L);
    float3 radiance = light.Ambient;
    float3 R = reflect(-V, normal);
    
    float D = DistributionGGX(normal, H, roughness);
    float G = GeometrySmith(normal, V, L, roughness);
    //float3 fo = GetFresnelF0(albedo, metallic);
    float cosTheta = max(dot(V, H), 0.0);
    float3 F = fresnelSchlick(cosTheta, F0);
    float3 ks = F;
    float3 kd = 1.0 - ks;
    ks = ks * (1.0 - metallic);
    
    float3 dfg = D * G * F;
    float NDotL = max(dot(normal, L), 0.0f);
    float NDotV = max(dot(normal, V), 0.0f);
    float denominator = 4.0 * NDotV * NDotL;
    float3 specular = dfg / max(denominator, 0.001);
    float3 color = (kd * albedo / PI + specular) * radiance * NDotL;
    return max(color, float3(0.0f, 0.0f, 0.0f));
}
float3 ComputePointLight_PBR(PointLight light, float3 posW, float3 eyePos, float3 albedo, float3 normal, float roughness, float metallic, float3 F0)
{
    float3 L = normalize(light.Position - posW);
    float3 V = normalize(eyePos - posW);
    float3 H = normalize(V + L);
    float distance = length(light.Position - posW);
    float attenuation = 1.0f / dot(light.Att, float3(1.0f, distance, distance * distance));
    ////float att = 1.0f / dot(L.Att, float3(1.0f, d, d * d));
    //float attenuation = 1.0 / (distance * distance);
    float3 radiance = light.Ambient * attenuation;
    
    float D = DistributionGGX(normal, H, roughness);
    float G = GeometrySmith(normal, V, L, roughness);
    //float3 fo = GetFresnelF0(albedo, metallic);
    float cosTheta = max(dot(V, H), 0.0);
    float3 F = fresnelSchlick(cosTheta, F0);
    float3 ks = F;
    float3 kd = 1.0 - ks;
    ks = ks * (1.0 - metallic);
    
    float3 dfg = D * G * F;
    float NDotL = max(dot(normal, L), 0.0f);
    float NDotV = max(dot(normal, V), 0.0f);
    float denominator = 4.0 * NDotV * NDotL;
    float3 specular = dfg / max(denominator, 0.001);
    float3 color = (kd * albedo / PI + specular) * radiance * NDotL;
    return max(color, float3(0.0f, 0.0f, 0.0f));

}

float3 ComputeSpotLight_PBR(SpotLight light, float3 posW, float3 eyePos, float3 albedo, float3 normal, float roughness, float metallic, float3 F0)
{
    float3 L = normalize(light.Position - posW);
    float3 V = normalize(eyePos - posW);
    float3 H = normalize(V + L);
    float distance = length(light.Position - posW);
    float spot = pow(max(dot(-L, light.Direction), 0.0f), light.Spot);
    float attenuation = spot / dot(light.Att, float3(1.0f, distance, distance * distance));
    ////float att = 1.0f / dot(L.Att, float3(1.0f, d, d * d));
    //float attenuation = 1.0 / (distance * distance);
    float3 radiance = light.Ambient.xyz * attenuation;
    
    float D = DistributionGGX(normal, H, roughness);
    float G = GeometrySmith(normal, V, L, roughness);
    //float3 fo = GetFresnelF0(albedo, metallic);
    float cosTheta = max(dot(V, H), 0.0);
    float3 F = fresnelSchlick(cosTheta, F0);
    float3 ks = F;
    float3 kd = 1.0 - ks;
    ks = ks * (1.0 - metallic);
    
    float3 dfg = D * G * F;
    float NDotL = max(dot(normal, L), 0.0f);
    float NDotV = max(dot(normal, V), 0.0f);
    float denominator = 4.0 * NDotV * NDotL;
    float3 specular = dfg / max(denominator, 0.001);
    float3 color = (kd * albedo / PI + specular) * radiance * NDotL;
    return max(color, float3(0.0f, 0.0f, 0.0f));

}
float3 NormalSampleToWorldSpace(float3 normalMapSample,
    float3 unitNormalW,
    float4 tangentW)
{
    // 将读取到法向量中的每个分量从[0, 1]还原到[-1, 1]
    float3 normalT = 2.0f * normalMapSample - 1.0f;

    // 构建位于世界坐标系的切线空间
    float3 N = unitNormalW;
    float3 T = normalize(tangentW.xyz - dot(tangentW.xyz, N) * N); // 施密特正交化
    float3 B = cross(N, T);

    float3x3 TBN = float3x3(T, B, N);

    // 将凹凸法向量从切线空间变换到世界坐标系
    float3 bumpedNormalW = mul(normalT, TBN);
    bumpedNormalW = normalize(bumpedNormalW);
    return bumpedNormalW;
}


float4 PS(VertexPosHWNormalTex pIn) : SV_TARGET
{
    float4 texColor = float4(0.5f, 0.5f, 0.5f, 1.0f);
    if (g_TextureUsed)
        texColor = g_Tex.Sample(g_Sam, pIn.Tex);
    
    //clip(texColor.a - 0.5f);

    // 标准化法向量
    pIn.NormalW = normalize(pIn.NormalW);

    // 求出顶点指向眼睛的向量，以及顶点与眼睛的距离
    float3 toEyeW = normalize(g_EyePosW - pIn.PosW);
    float distToEye = distance(g_EyePosW, pIn.PosW);
    
    // 法线映射
    float3 bumpedNormalW = pIn.NormalW;
    [flatten]
    if (g_UseNormalTexture)
    {
        float3 normalMapSample = g_NormalTex.Sample(g_Sam, pIn.Tex).rgb;
        bumpedNormalW = NormalSampleToWorldSpace(normalMapSample, pIn.NormalW, pIn.TangentW);
    }
    
    float shadow[5] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };
    // 仅第一个方向光用于计算阴影
    if (g_UseShadow)
        shadow[0] = CalcShadowFactor(g_SamShadow, g_ShadowMap, pIn.ShadowPosH);
    
    float ambientAccess = 1.0f;
    if (g_EnableSSAO)
    {
        pIn.SSAOPosH /= pIn.SSAOPosH.w;
        ambientAccess = g_SSAOMap.SampleLevel(g_Sam, pIn.SSAOPosH.xy, 0.0f).r;
    }
    

    float3 PBRColor = 0.0f;
    float metallic, roughness;
    
    metallic = g_Metallic;
    [flatten]
    if (g_UseMetallicMap)
    {
        metallic = g_MetallicMap.Sample(g_Sam, pIn.Tex);
    }
    
    roughness = g_Roughness;
    [flatten]
    if (g_UseRoughnessMap)
    {
        roughness = g_RoughnessMap.Sample(g_Sam, pIn.Tex);
    }
    
    float3 F0 = float3(0.04f, 0.04f, 0.04f);
    F0 = lerp(F0, texColor.xyz, g_Metallic);
    
    [unroll]
    for (int i = 0; i < min(100, g_PointNumber); ++i)
    {
        PBRColor += ComputePointLight_PBR(g_PointLight[i], pIn.PosW, g_EyePosW, texColor.xyz, bumpedNormalW, roughness, metallic, F0);
    }
    
    [unroll]
    for (i = 0; i < min(20, g_DirNumber); ++i)
    {
        PBRColor += ComputeDirectionalLight_PBR(g_DirLight[i], pIn.PosW, g_EyePosW, texColor.xyz, bumpedNormalW, roughness, metallic, F0) * shadow[i];
    }
    
    
    [unroll]
    for (i = 0; i < min(60, g_SpotNumber); ++i)
    {
        PBRColor += ComputeSpotLight_PBR(g_SpotLight[i], pIn.PosW, g_EyePosW, texColor.xyz, bumpedNormalW, roughness, metallic, F0);

    }
    float3 ambientColor;
    float3 V = normalize(g_EyePosW - pIn.PosW);
    float3 R = reflect(-V, bumpedNormalW);
    float3 kS = FresnelSchlickRoughness(max(dot(bumpedNormalW, V), 0.0f), F0, roughness);
    float3 kD = float3(1.0f, 1.0f, 1.0f) - kS;
    kD *= 1.0 - metallic;

    float3 irradiance = g_IBLDeffuse.SampleLevel(g_Sam, bumpedNormalW, 0).rgb;
    float3 diffuse = texColor.xyz * irradiance;

    const float MAX_REF_LOD = 4.0f;
    float3 prefilteredColor = g_IBLSpec.SampleLevel(g_Sam, R, roughness * MAX_REF_LOD).rgb;
    //float3 prefilteredColor = g_IBLSpec.SampleLevel(g_Sam, R, roughness * g_IBLStrength * 4).rgb;
    float2 brdf = g_BRDFLutMap.Sample(g_Sam, float2(min(max(dot(bumpedNormalW, V), 0.0f), 0.99f), roughness)).rg;
    float3 specular = prefilteredColor * (kS * brdf.x + brdf.y);
    ambientColor = (kD * diffuse + specular) * ambientAccess * g_IBLStrength;
    float4 litColor = float4(ambientColor + PBRColor, 1.0f);
    return litColor;
}