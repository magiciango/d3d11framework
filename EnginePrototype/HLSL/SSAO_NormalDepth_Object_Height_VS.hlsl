#include "SSAO.hlsli"

TessVertex VS(VertexPosNormalTangentTex vIn)
{
    TessVertex vOut;

    vOut.PosW = mul(float4(vIn.PosL, 1.0f), g_World).xyz;
    vOut.NormalW = mul(vIn.NormalL, (float3x3) g_WorldInvTranspose);
    vOut.TangentW = mul(vIn.TangentL, g_World);
    vOut.Tex = vIn.Tex;
    
    float d = distance(vOut.PosW, g_EyePosW);
    
    float tess = saturate((g_MinTessDistance - d) / (g_MinTessDistance - g_MaxTessDistance));
    
    // [0, 1] --> [g_MinTessFactor, g_MaxTessFactor]
    vOut.TessFactor = g_MinTessFactor + tess * (g_MaxTessFactor - g_MinTessFactor);
    
    return vOut;
}
