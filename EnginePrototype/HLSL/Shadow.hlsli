#include "BaseRegister.hlsli"

Texture2D g_DiffuseMap : register(t0);
Texture2D g_HeightMap : register(t1);
SamplerState g_Sam : register(s0);

cbuffer CBTransform : register(b0)
{
    matrix g_World;
    matrix g_WorldInvTranspose;
    matrix g_WorldViewProj;
    matrix g_ViewProj;
    float3 g_EyePosW;
}

cbuffer CBTess : register(b1)
{
    float g_HeightScale;
    float g_MaxTessDistance;
    float g_MinTessDistance;
    float g_MinTessFactor;
    float g_MaxTessFactor;
}