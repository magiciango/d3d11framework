#include "SkyBox.hlsli"

Texture2DArray g_Input;

struct OutTexutreCube
{
    float4 Xplus : SV_Target0;
    float4 Xminus : SV_Target1;
    float4 Yplus : SV_Target2;
    float4 Yminus : SV_Target3;
    float4 Zplus : SV_Target4;
    float4 Zminus : SV_Target5;
};
cbuffer CBCommon
{
    float g_Lerp;
    uint g_MipMap;
    float g_LerpDura;
};
OutTexutreCube PS(VertexPosTex pIn)
{
    OutTexutreCube pOut;
    float2 uv = pIn.Tex;
    float alpha = g_Lerp;
    pOut.Xplus = lerp(g_Input.SampleLevel(g_Sam, float3(uv, 0), g_MipMap), g_Input.SampleLevel(g_Sam, float3(uv, 6), g_MipMap), alpha);
    pOut.Xminus = lerp(g_Input.SampleLevel(g_Sam, float3(uv, 1), g_MipMap), g_Input.SampleLevel(g_Sam, float3(uv, 7), g_MipMap), alpha);
    pOut.Yplus = lerp(g_Input.SampleLevel(g_Sam, float3(uv, 2), g_MipMap), g_Input.SampleLevel(g_Sam, float3(uv, 8), g_MipMap), alpha);
    pOut.Yminus = lerp(g_Input.SampleLevel(g_Sam, float3(uv, 3), g_MipMap), g_Input.SampleLevel(g_Sam, float3(uv, 9), g_MipMap), alpha);
    pOut.Zplus = lerp(g_Input.SampleLevel(g_Sam, float3(uv, 4), g_MipMap), g_Input.SampleLevel(g_Sam, float3(uv, 10), g_MipMap), alpha);
    pOut.Zminus = lerp(g_Input.SampleLevel(g_Sam, float3(uv, 5), g_MipMap), g_Input.SampleLevel(g_Sam, float3(uv, 11), g_MipMap), alpha);
    return pOut;

}