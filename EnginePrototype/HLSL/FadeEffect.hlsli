#include "BaseRegister.hlsli"
Texture2D g_Tex : register(t0);
Texture2D g_NextTex : register(t1);
SamplerState g_Sam : register(s0);

cbuffer CBChangesEveryFrame : register(b0)
{
    float g_FadeAmount;      // 颜色程度控制(0.0f-1.0f)
    float2 g_MousePosition;
    float padf;
    //matrix g_WorldViewProj;
}

cbuffer Transform : register(b12)
{
    matrix g_World;
    matrix g_WorldInvTranspose;
    matrix g_View;
    matrix g_Proj;
    matrix g_WorldViewProj;
    float3 g_EyePosW;
    float padT;
}