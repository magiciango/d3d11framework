#include "BaseRegister.hlsli"

Texture2D g_Tex : register(t0);

Texture2D g_DisplacementMap : register(t3);
Texture2D g_NormalTex : register(t4);
Texture2D g_HeightMap : register(t5);

Texture2D g_MetallicMap : register(t6);
Texture2D g_RoughnessMap : register(t7);

Texture2D g_ShadowMap : register(t8);
Texture2D g_SSAOMap : register(t9);

TextureCube g_IBLDeffuse : register(t2);
TextureCube g_IBLSpec : register(t10);
Texture2D g_BRDFLutMap : register(t11);

SamplerState g_Sam : register(s0);
SamplerState g_SamPointClamp : register(s1);
SamplerComparisonState g_SamShadow : register(s2);

cbuffer CBChangesEveryDrawing : register(b0)
{
    //常规
    matrix g_TexTransform;
	Material g_Material;
    float4 g_Color;
    //骨骼动画相关
    row_major matrix g_Bones[120];

    //2D帧动画相关
    float g_AnimeFrame;
    //换色相关
    int g_ColorBlend;
    int g_BonesAnimetionEnabled;
    
    bool g_TextureUsed;
    bool g_UseNormalTexture;
    
    bool g_UseMetallicMap;
    float g_Metallic;
    bool g_UseRoughnessMap;
    float g_Roughness;
    bool g_UseIBL;
    float3 g_ambientColor;
    float3 pad;
}

cbuffer CBDrawingStates : register(b1)
{
    /*int g_IsReflection;
    int g_IsShadow;*/
    //反射相关
    int g_ReflectionEnabled;
    //雾效相关
    float4 g_FogColor;
    int g_FogEnabled;
    float g_FogStart;
    float g_FogRange;
    float g_Pad2;
    //水波相关
    int g_WavesEnabled;
    float2 g_DisplacementMapTexelSize;
    float g_GridSpatialStep;
}

cbuffer CBShadow : register(b2)
{
    matrix g_ShadowTransform;
    bool g_EnableSSAO;
}

cbuffer CBTess : register(b3)
{
    float g_HeightScale;
    float g_MaxTessDistance;
    float g_MinTessDistance;
    float g_MinTessFactor;
    float g_MaxTessFactor;
}

cbuffer CBpbr : register(b4)
{
    
}

cbuffer CBTransform : register(b12)
{
    matrix g_World;
    matrix g_WorldInvTranspose;
    matrix g_View;
    matrix g_Proj;
    matrix g_ViewProj;
    matrix g_WorldViewProj;
    float3 g_EyePosW;
    bool g_UseShadow;

}

cbuffer CBLight : register(b13)
{
    DirectionalLight g_DirLight[20];
    PointLight g_PointLight[100];
    SpotLight g_SpotLight[60];
    uint g_DirNumber;
    uint g_PointNumber;
    uint g_SpotNumber;
    float g_IBLStrength;
}

//StructuredBuffer<PointLight> g_PointLights;

