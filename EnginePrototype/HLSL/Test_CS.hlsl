
RWTexture2D<float4> g_Output : register(u0);

[numthreads(16, 16, 1)]
void main( uint3 DTid : SV_DispatchThreadID )
{
    g_Output[DTid.xy] = float4(DTid.x / 256.f, DTid.y / 256.f, 0, 1);

}