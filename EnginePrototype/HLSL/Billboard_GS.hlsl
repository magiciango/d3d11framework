#include "Basic.hlsli"

//uv
static const float2 g_TexCoord[4] = { float2(0.0f, 1.0f), float2(0.0f, 0.0f), float2(1.0f, 1.0f), float2(1.0f, 0.0f) };
//ビルボード専用の頂点PointSpriteはfloat3 Positionとfloat2 Sizeって組み合ってる
[maxvertexcount(4)]
void GS(point PointSprite input[1], uint primID : SV_PrimitiveID,
    inout TriangleStream<BillboardVertex> output)
{
    //ビルボード座標系計算
    float3 up = float3(0.0f, 1.0f, 0.0f);       //UpVector変わらない
    float3 look = g_EyePosW - input[0].PosW;    //lookVector(forward)はViewから頂点のベクトルのxz平面投影
    look.y = 0.0f;                              //投影操作
    look = normalize(look);                     
    float3 right = cross(up, look);             //座標系確定

    //頂点座標
    float4 v[4];
    float3 center = input[0].PosW;
    float halfWidth = 0.5f * input[0].SizeW.x;
    float halfHeight = 0.5f * input[0].SizeW.y;
    v[0] = float4(center + halfWidth * right - halfHeight * up, 1.0f);
    v[1] = float4(center + halfWidth * right + halfHeight * up, 1.0f);
    v[2] = float4(center - halfWidth * right - halfHeight * up, 1.0f);
    v[3] = float4(center - halfWidth * right + halfHeight * up, 1.0f);

    //頂点座標をマトリックス変換してPixelShaderへ
    BillboardVertex gOut;
    matrix viewProj = mul(g_View, g_Proj);
    [unroll]
    for (int i = 0; i < 4; ++i)
    {
        gOut.PosW = v[i].xyz;
        gOut.PosH = mul(v[i], viewProj);
        gOut.NormalW = look;
        gOut.Tex = g_TexCoord[i];
        gOut.PrimID = primID;   //PrimIDは、森とか作るときIDを引数としてランダムなテクスチャを描画するつもりですが、今回は実装してない
        output.Append(gOut);
    }
}