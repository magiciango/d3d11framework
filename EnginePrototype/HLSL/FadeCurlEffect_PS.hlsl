#include "FadeEffect.hlsli"

#define PI 3.141592
const float radius = 0.1;

/// 1D function x: cylFun(t); y: normal at that point.
float2 curlFun(float t, float maxt,float3 cyl)
{
    float2 ret = float2(t, 1.0);
    if (t < cyl.z - radius)
        return ret; /// Before the curl
    if (t > cyl.z + radius)
        return float2(-1.0,-1.0); /// After the curl
    
    /// Inside the curl
    float a = asin((t - cyl.z) / radius);
    float ca = -a + PI;
    ret.x = cyl.z + ca * radius;
    ret.y = cos(ca);
    
    if (ret.x < maxt)  
        return ret; /// We see the back face

    if (t < cyl.z)
        return float2(t, 1.0); /// Front face before the curve starts
    ret.y = cos(a);
    ret.x = cyl.z + a * radius;
    return ret.x < maxt ? ret : float2(-1.0,-1.0); /// Front face curve
}


float4 main(VertexPosHTex pIn) : SV_TARGET
{
    float2 uv = pIn.Tex;                                                //uv
    float2 ur = float2(1, 1);                                           //终点
    float2 Amount = float2(g_FadeAmount, g_FadeAmount);                 //进行度
    float d = length(Amount * (1.0 + 4.0 * radius)) - 2.0 * radius;     //wtf??
    float3 cyl = float3(normalize(Amount).x, normalize(Amount).y, d);   //组合
    
    d = dot(uv, cyl.xy);
    float2 end = abs((ur - uv) / cyl.xy);
    float maxt = d + min(end.x, end.y);
    float2 cf = curlFun(d, maxt, cyl);
    float2 tuv = uv + cyl.xy * (cf.x - d);
    
    float shadow = 1.0 - smoothstep(0.0, radius * 2.0, -(d - cyl.z));
    shadow *= (smoothstep(-radius, radius, (maxt - (cf.x + 1.5 * PI * radius + radius))));
    float4 curr = g_Tex.Sample(g_Sam, tuv / ur); 
    curr = cf.y > 0.0 ? curr * cf.y * (1.0 - shadow) : (curr * 0.25 + 0.75) * (-cf.y);
    shadow = smoothstep(0.0, radius * 2.0, (d - cyl.z));
    float4 next = g_NextTex.Sample(g_Sam, uv / ur);
    float4 outColor = cf.x > 0.0 ? curr : next;
    
    return outColor;
    //return g_NextTex.Sample(g_Sam, pIn.Tex);

}