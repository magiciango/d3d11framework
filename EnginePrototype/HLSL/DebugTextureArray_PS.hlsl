#include "2DBase.hlsli"

float4 PS(VertexPosHTex pIn, uniform int index) : SV_Target
{
    return g_TextureArray.Sample(g_Sam, float3(pIn.Tex, index));
}
