#include "Basic.hlsli"

// 像素着色器(2D)
float4 PS(VertexPosHTex pIn) : SV_Target
{
    float4 color = g_Tex.Sample(g_Sam, pIn.Tex);
    clip(color.r + color.g + color.b - 0.3f);
    return color;
}