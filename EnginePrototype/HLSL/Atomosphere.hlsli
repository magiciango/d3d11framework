#include "BaseRegister.hlsli"
#include "Common.hlsli"
Texture2D g_NoiseTexture : register(t0);
SamplerState g_Sam : register(s0);

struct VertexRay
{
    float3 Ray : POSITION;
    float4 PosH : SV_Position;
};

cbuffer CBDrawStates : register(b0)
{
	float4 g_Color;
	float g_SinLength;
	float g_CosLength;
}

cbuffer CBSkyParameter : register(b1)
{
	//normal
    float3 g_CameraPos;
    float g_time;
    
    matrix g_View;
    
	//wave
    float g_WaterDepth;
    float g_WaveSpeed;
    float g_WavePhase;
    float g_WaveWeight;
	//atomosphere
    float g_SunIntensity;
    
    float g_PlantRadius;
    float g_AtomosphereRadius;
    float g_kMie;
    float g_shRlh;
    
    float3 g_kRlh;
    float g_shMie;
    
    float g_g;
    //sun
    float3 g_SunPos;
    
    float g_SunRadius;
    //stars
    float g_StarsDensity;
    //aurora
    
    float g_AuroraDensity;
}

cbuffer CBRayMarching : register(b2)
{
    float g_WaveIter;
    float g_NormalIter;
    float g_AuroraIter;
}

cbuffer CBDrawState : register(b3)
{
    bool g_UseAurora;
    bool g_UseSea;
}
struct VertexPos
{
	float3 PosL : POSITION;
};

struct VertexPosHL
{
	float4 PosH : SV_POSITION;
	float2 Tex : TEXCOORD;
};

