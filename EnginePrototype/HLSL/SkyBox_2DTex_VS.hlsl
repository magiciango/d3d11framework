#include "SkyBox.hlsli"

VertexPosTex VS(VertexPosLTex vIn)
{
    VertexPosTex vOut;
    vOut.Pos = float4(vIn.PosL.xyz, 1.0f);
    vOut.Tex = vIn.Tex;
    return vOut;

}