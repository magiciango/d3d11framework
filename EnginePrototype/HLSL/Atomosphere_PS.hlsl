#include "Atomosphere.hlsli"

struct Ray
{
    float3 o;
    float3 d;
};
struct Sphere
{
    float3 pos;
    float rad;
};
struct PSOut
{
    float4 color : SV_Target0;
    float4 sundata : SV_Target1;
};


// lower this if your GPU cries for mercy (set to 0 to remove clouds!)
#define CLOUDS_STEPS 47
#define ENABLE_SSS 1

#define DRAG_MULT 0.048
#define ITERATIONS_RAYMARCH g_WaveIter
#define ITERATIONS_NORMAL g_NormalIter
#define ITERATIONS_AURORA g_AuroraIter

#define PI 3.141592
#define iSteps 16
#define jSteps 8
//******************
//レイマッチングベース
//
float3x3 rotmat(float3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;
	
    return float3x3(oc * axis.x * axis.x + c, oc * axis.x * axis.y - axis.z * s, oc * axis.z * axis.x + axis.y * s,
	oc * axis.x * axis.y + axis.z * s, oc * axis.y * axis.y + c, oc * axis.y * axis.z - axis.x * s,
	oc * axis.z * axis.x - axis.y * s, oc * axis.y * axis.z + axis.x * s, oc * axis.z * axis.z + c);
}

float intersectPlane(float3 origin, float3 direction, float3 _point, float3 normal)
{
    return clamp(dot(_point - origin, normal) / dot(direction, normal), -1.0, 9991999.0);
}
float2 rsi(float3 r0, float3 rd, float sr)
{
    // ray-sphere intersection that assumes
    // the sphere is centered at the origin.
    // No intersection when result.x > result.y
    float a = dot(rd, rd);
    float b = 2.0 * dot(rd, r0);
    float c = dot(r0, r0) - (sr * sr);
    float d = (b * b) - 4.0 * a * c;
    if (d < 0.0)
        return float2(1e5, -1e5);
    return float2(
        (-b - sqrt(d)) / (2.0 * a),
        (-b + sqrt(d)) / (2.0 * a)
    );
}
static float minhit = 0.0;
static float maxhit = 0.0;
float rsi2(in Ray ray, in Sphere sphere)
{
    float3 oc = ray.o - sphere.pos;
    float b = 2.0 * dot(ray.d, oc);
    float c = dot(oc, oc) - sphere.rad * sphere.rad;
    float disc = b * b - 4.0 * c;
    float2 ex = float2(-b - sqrt(disc), -b + sqrt(disc)) / 2.0;
    minhit = min(ex.x, ex.y);
    maxhit = max(ex.x, ex.y);
    return lerp(ex.y, ex.x, step(0.0, ex.x));
}

//******************
//波
//
float2 wavedx(float2 position, float2 direction, float speed, float frequency, float timeshift)
{
    float x = dot(direction, position) * frequency + timeshift * speed;
    float wave = exp(sin(x) - 1.0);
    float dx = wave * cos(x);
    return float2(wave, -dx);
}
float getwaves(float2 position, int iterations)
{
    float iter = 0.0;
    float phase = g_WavePhase;
    float speed = g_WaveSpeed;
    float weight = g_WaveWeight;
    float w = 0.0;
    float ws = 0.0;
    for (int i = 0; i < iterations; i++)
    {
        float2 p = float2(sin(iter), cos(iter));
        float2 res = wavedx(position, p, speed, phase, g_time);
        position += p * res.y * weight * DRAG_MULT;
        w += res.x * weight;
        iter += 12.0;
        ws += weight;
        weight = lerp(weight, 0.0, 0.2);
        phase *= 1.18;
        speed *= 1.07;
    }
    return w / ws;
}
float raymarchwater(float3 camera, float3 start, float3 end, float depth)
{
    float3 pos = start;
    float h = 0.0;
    float hupper = depth;
    float hlower = 0.0;
    float2 zer = 0.0;
    float3 dir = normalize(end - start);
    float eps = 0.01;
    for (int i = 0; i < 318; i++)
    {
        h = getwaves(pos.xz * 0.1, ITERATIONS_RAYMARCH) * depth - depth;
        float dist_pos = distance(pos, camera);
        if (h + eps * dist_pos > pos.y)
        {
            return dist_pos;
        }
        pos += dir * (pos.y - h);
        //eps *= 1.01;
    }
    return -1.0;
}
float3 normal(float2 pos, float e, float depth)
{
    float2 ex = float2(e, 0);
    float H = getwaves(pos.xy * 0.1, ITERATIONS_NORMAL) * depth;
    float3 a = float3(pos.x, H, pos.y);
    return (cross(normalize(a - float3(pos.x - e, getwaves((pos.xy - ex.xy) * 0.1, ITERATIONS_NORMAL) * depth, pos.y)),
                           normalize(a - float3(pos.x, getwaves((pos.xy + ex.yx) * 0.1, ITERATIONS_NORMAL) * depth, pos.y + e))));
}





//******************
//大気散乱シミュレーション
//
#define MieScattCoeff 2.0
float planetradius = 6378000.1;
float3 atmosphere(float3 r, float3 r0, float3 pSun, float iSun, float rPlanet, float rAtmos, float3 kRlh, float kMie, float shRlh, float shMie, float g)
{
    // Normalize the sun and view directions.
    pSun = normalize(pSun);
    r = normalize(r);

    // Calculate the step size of the primary ray.
    float2 p = rsi(r0, r, rAtmos);
    if (p.x > p.y)
        return float3(0, 0, 0);
    p.y = min(p.y, rsi(r0, r, rPlanet).x);
    float iStepSize = (p.y - p.x) / float(iSteps);
    Ray ray = { r0, r };
    Sphere sphere = { float3(0, 0, 0), rAtmos };
    float rs = rsi2(ray, sphere);
    float3 px = r0 + r * rs;
    //shMie *= ( (pow(fbmHI(px) * (supernoise3dX(px* 0.00000669 + time * 0.001)*0.5 + 0.5) * 1.3, 3.0) * 0.8 + 0.5));
    
    // Initialize the primary ray time.
    float iTime = 0.0;

    // Initialize accumulators for Rayleigh and Mie scattering.
    float3 totalRlh = float3(0, 0, 0);
    float3 totalMie = float3(0, 0, 0);

    // Initialize optical depth accumulators for the primary ray.
    float iOdRlh = 0.0;
    float iOdMie = 0.0;

    // Calculate the Rayleigh and Mie phases.
    float mu = dot(r, pSun);
    float mumu = mu * mu;
    float gg = g * g;
    float pRlh = 3.0 / (16.0 * PI) * (1.0 + mumu);
    float pMie = 3.0 / (8.0 * PI) * ((1.0 - gg) * (mumu + 1.0)) / (pow(1.0 + gg - 2.0 * mu * g, 1.5) * (2.0 + gg));

    // Sample the primary ray.
    for (int i = 0; i < iSteps; i++)
    {

        // Calculate the primary ray sample position.
        float3 iPos = r0 + r * (iTime + iStepSize * 0.5);

        // Calculate the height of the sample.
        float iHeight = length(iPos) - rPlanet;

        // Calculate the optical depth of the Rayleigh and Mie scattering for this step.
        float odStepRlh = exp(-iHeight / shRlh) * iStepSize;
        float odStepMie = exp(-iHeight / shMie) * iStepSize;

        // Accumulate optical depth.
        iOdRlh += odStepRlh;
        iOdMie += odStepMie;

        // Calculate the step size of the secondary ray.
        float jStepSize = rsi(iPos, pSun, rAtmos).y / float(jSteps);

        // Initialize the secondary ray time.
        float jTime = 0.0;

        // Initialize optical depth accumulators for the secondary ray.
        float jOdRlh = 0.0;
        float jOdMie = 0.0;

        // Sample the secondary ray.
        for (int j = 0; j < jSteps; j++)
        {

            // Calculate the secondary ray sample position.
            float3 jPos = iPos + pSun * (jTime + jStepSize * 0.5);

            // Calculate the height of the sample.
            float jHeight = length(jPos) - rPlanet;

            // Accumulate the optical depth.
            jOdRlh += exp(-jHeight / shRlh) * jStepSize;
            jOdMie += exp(-jHeight / shMie) * jStepSize;

            // Increment the secondary ray time.
            jTime += jStepSize;
        }

        // Calculate attenuation.
        float3 attn = exp(-(kMie * (iOdMie + jOdMie) + kRlh * (iOdRlh + jOdRlh)));

        // Accumulate scattering.
        totalRlh += odStepRlh * attn;
        totalMie += odStepMie * attn;

        // Increment the primary ray time.
        iTime += iStepSize;

    }

    // Calculate and return the final color.
    return iSun * (pRlh * kRlh * totalRlh + pMie * kMie * totalMie);
}
float3 getatm(float3 ray, float3 sd)
{
    float3 color = atmosphere(
        ray, // normalized ray direction
        float3(0, 6372e3, 0), // ray origin
        sd, // position of the sun
        g_SunIntensity, // intensity of the sun
        g_PlantRadius*1e3, // radius of the planet in meters
        g_AtomosphereRadius*1e3, // radius of the atmosphere in meters
        g_kRlh*1e-6, // Rayleigh scattering coefficient
        g_kMie*1e-6, // Mie scattering coefficient
        g_shRlh*1e3, // Rayleigh scale height
        g_shMie*1e3 * MieScattCoeff, // Mie scale height
        g_g // Mie preferred scattering direction
    );
    return color;
    
}


//******************
//太陽
//
float sun(float3 ray, float3 sd)
{
    return pow(max(0.0, dot(ray, sd)), g_SunRadius) * 60;
}
float smart_inverse_dot(float dt, float coeff)
{
    return 1.0 - (1.0 / (1.0 + dt * coeff));
}
float3 getSunColorDirectly(float roughness, float3 sd)
{
    float3 sunBase = 15.0;
    float dt = max(0.0, (dot(sd, float3(0.0, 1.0, 0.0))));
    float dtx = smoothstep(-0.0, 0.1, dt);
    float dt2 = 0.9 + 0.1 * (1.0 - dt);
    float st = max(0.0, 1.0 - smart_inverse_dot(dt, 11.0));
    float3 supersundir = max(0.0, 1.0 - st * 4.0 * pow(float3(50.0 / 255.0, 111.0 / 255.0, 153.0 / 255.0), 2.4));
//    supersundir /= length(supersundir) * 1.0 + 1.0;
    return supersundir * 4.0;
    //return lerp(supersundir * 1.0, sunBase, st);
    //return  max(float3(0.3, 0.3, 0.0), (  sunBase - float3(5.5, 18.0, 20.4) *  pow(1.0 - dt, 8.0)));
}
float flare(float3 p, float3 r)
{
    return achnoise(10.0 * normalize(p - r)) / pow(distance(p, r) * 10.0, 5.0);
}

//******************
//星
//
float3 nmzHash33(float3 q)
{
    int3 p = int3(int3(q));
    p = p*int3(374761393U, 1103515245U, 668265263U) + p.zxy + p.yzx;
    p = p.yzx*(p.zxy^(p >> 3U));
    return float3(p^(p >> 16U))*(1.0/0xffffffffU);
}
float3 stars(in float3 p)
{
    float3 c = 0;
    float res = g_ScreenSize.x * 1.;
    
    for (float i = 0.; i < g_StarsDensity; i++)
    {
        float3 q = frac(p*(.15*res))-0.5;
        float3 id = floor(p*(.15*res));
        float2 rn = nmzHash33(id).xy;
        float c2 = 1.-smoothstep(0.,.6,length(q));
        c2 *= step(rn.x,.0005+i*i*0.001);
        c += c2*(lerp(float3(1.0,0.49,0.1),float3(0.75,0.9,1.),rn.y)*0.1+0.9);
        p *= 1.3;
    }
    return c*c*.8;
}

//******************
//アロラ
//
float4 aurora(float3 ro, float3 rd,float ds)
{
    float4 col = 0;
    float4 avgCol = 0;
    
    for (float i = 0.; i < ITERATIONS_AURORA; i++)
    {
        float of = 0.006 * smoothstep(0., 15., i);
        float pt = ((.8 + pow(i, 1.4) * .002) - ro.y) / (rd.y * 2. + 0.4);
        pt -= of;
        float3 bpos = ro + pt * rd;
        float2 p = bpos.zx;
        float rzt = triNoise2d(p, 0.06, g_time);
        float4 col2 = float4(0, 0, 0, rzt);
        col2.rgb = (sin(1. - float3(2.15, -.5, 1.2) + i * 0.043) * 0.5 + 0.5) * rzt;
        avgCol = lerp(avgCol, col2, .5);
        col += avgCol * exp2(-i * 0.065 - 2.5) * smoothstep(0., 5., i);
        
    }
    
    col *= (clamp(rd.y * 15. + .4, 0., 1.));
    return col * g_AuroraDensity * ds;
}


float4 main(VertexRay pIn) : SV_Target
{
    float3 sd = normalize(g_SunPos); //mul(rotmat(float3(1.0, 1.0, 0.0), time * 0.25), normalize(float3(0.0, 1.0, 0.0)));
    float3 wceil = float3(0.0, 0.0, 0.0);
    float3 wfloor = float3(0.0, -g_WaterDepth, 0.0);
    float3 orig = g_CameraPos;
    float3 ray = normalize(pIn.Ray);
    float hihit = intersectPlane(orig, ray, wceil, float3(0.0, 1.0, 0.0));
    Ray _ray = { orig, ray };
    Sphere _Sphere = { float3(-2.0, 3.0, 0.0), 1.0 };
    float spherehit = rsi2(_ray, _Sphere);
    float fff = 1.0;
    
    [flatten]
    if (ray.y >= 0.0)
    {
        float3 rray = float3(ray.x, abs(ray.y), ray.z);
        float3 C = 0.0;
        float3 atm = getatm(rray, sd);
        C = atm * 2.0 + sun(rray, sd) * getSunColorDirectly(1.0, sd);
        
        float3 atmorg = float3(0, planetradius, 0);
        Ray r = { atmorg, rray };
        C = C + flare(sd, ray);
        C *= fff * 0.5;
        //Aurora
        if (g_SunPos.y < 0)
        {
            C += stars(ray) * (max(0, 0.5 - C.r - C.g - C.b));
            if(g_UseAurora)
            {
                float4 aur = smoothstep(0., 1.5, aurora(float3(0, 0, -6.7), ray, ray.y - g_SunPos.y));
                C = C * (1 - aur.a) + aur.rgb;
            }
        }
        if (C.x > 1)
            C.x = 1;

        if (C.y > 1)
            C.y = 1;

        if (C.z > 1)
            C.z = 1;

        return float4(C, 1.0);
    }
    
    if(g_UseSea)
    {
        float lohit = intersectPlane(orig, ray, wfloor, float3(0.0, 1.0, 0.0));
        float3 hipos = orig + ray * hihit;
        float3 lopos = orig + ray * lohit;
        float dist = raymarchwater(orig, hipos, lopos, g_WaterDepth);
        float3 pos = orig + ray * dist;

        float3 N = normal(pos.xz, 0.001, g_WaterDepth);
        float2 velocity = N.xz * (1.0 - N.y);
        N = lerp(float3(0.0, 1.0, 0.0), N, 1.0 / (dist * dist * 0.01 + 1.0));
        float3 R = reflect(ray, N);
        float fresnel = (0.04 + (1.0 - 0.04) * (pow(1.0 - max(0.0, dot(-N, ray)), 5.0)));
	
        float3 C;
        C = fresnel * getatm(R, sd) * 2.0 + fresnel * sun(R, sd);
        if (g_SunPos.y < 0 && g_UseAurora)
            C += fresnel * stars(R) + fresnel * smoothstep(0., 1.5, aurora(float3(0, 0, -6.7), R, R.y - g_SunPos.y)).rgb;
      
        return float4(C, 1.0);
    }
    else
    {
        float3 rray = float3(ray.x, abs(ray.y), ray.z);
        float3 C = 0.0;
        float3 atm = getatm(rray, sd);
        C = atm * 2.0;
        
        float3 atmorg = float3(0, planetradius, 0);
        Ray r = { atmorg, rray };
        C *= fff * 0.5;
        //Aurora
        if (g_SunPos.y < 0)
        {
            C += stars(ray) * (max(0, 0.5 - C.r - C.g - C.b));
            if (g_UseAurora)
            {
                float4 aur = smoothstep(0., 1.5, aurora(float3(0, 0, -6.7), ray, ray.y - g_SunPos.y));
                C = C * (1 - aur.a) + aur.rgb;
            }
        }
      
        return float4(C, 1.0);
    }
    
}