#include "2DBase.hlsli"

float4 PS(VertexPosHTex pIn) : SV_Target
{
    float4 outColor = g_DiffuseMap.Sample(g_Sam, pIn.Tex);
    //if (outColor.x > 1)
    //    outColor.x = 1;
    //else
    //    outColor.x = 0;
    //if (outColor.y > 1)
    //    outColor.y = 1;
    //else
    //    outColor.y = 0;
    //if (outColor.z > 1)
    //    outColor.z = 1;
    //else
    //    outColor.z = 0;
    outColor.xyz = ACESToneMapping(outColor.xyz);
    return outColor;
}