
#ifndef EFFECTS_H
#define EFFECTS_H

#include <memory>
#include <string>
#include "LightHelper.h"
#include "RenderStates.h"
//****************************************************
//		interface
//****************************************************
class IEffect
{
public:
	enum RenderType { RenderObject, RenderInstance };
	enum RSFillMode { Solid, WireFrame };
	// 使用模板别名(C++11)简化类型名
	template <class T>
	using ComPtr = Microsoft::WRL::ComPtr<T>;

	IEffect() = default;
	virtual ~IEffect() = default;
	// 不允许拷贝，允许移动
	IEffect(const IEffect&) = delete;
	IEffect& operator=(const IEffect&) = delete;
	IEffect(IEffect&&) = default;
	IEffect& operator=(IEffect&&) = default;

	// 绑定渲染管线
	virtual void SetRenderPass(ID3D11DeviceContext* deviceContext) {};
	// 更新并绑定常量缓冲区
	virtual void Apply(ID3D11DeviceContext* deviceContext) = 0;
};
class IEffectTransform
{
public:
	virtual void XM_CALLCONV SetWorldMatrix(DirectX::FXMMATRIX W) = 0;
	virtual void XM_CALLCONV SetViewMatrix(DirectX::FXMMATRIX V) = 0;
	virtual void XM_CALLCONV SetProjMatrix(DirectX::FXMMATRIX P) = 0;
};
class IEffectDeferred
{
public:
	enum DefferedPass
	{
		None,
		G_Pass,
		L_Pass,
		Draw,
		Transparent,
	};
	virtual void SetRenderDefferedPass(ID3D11DeviceContext* deviceContext) = 0;
	virtual void SetRenderDefferedDraw(ID3D11DeviceContext* deviceContext) = 0;
	virtual void SetRenderDefferedOff(ID3D11DeviceContext* deviceContext) = 0;
	virtual void SetAmbientTexture(ID3D11ShaderResourceView* texture) = 0;
	virtual void SetDiffuseTexture(ID3D11ShaderResourceView* texture) = 0;
	virtual void SetSpecTexture(ID3D11ShaderResourceView* texture) = 0;
};
class IEffectTextureDiffuse
{
public:
	virtual void SetTextureDiffuse(ID3D11ShaderResourceView* textureDiffuse) = 0;
};
class IEffectMesh
{
public:
	enum MeshType { StaticMesh, BoneMesh };
	virtual void SetMeshType(MeshType type) = 0;
	virtual void SetBonesMaterial(DirectX::XMMATRIX mat[], unsigned num) = 0;
};
class IEffectTextureNormal
{
public:
	virtual void SetTextureNormal(ID3D11ShaderResourceView* textureDiffuse) = 0;
	virtual void SetUseNormalMap(bool UseMap) = 0;
};
class IEffectTextureHeight
{
public:
	virtual void SetTextureHeight(ID3D11ShaderResourceView* textureDiffuse) = 0;
	// 设置摄像机位置
	virtual void SetEyePos(const DirectX::XMFLOAT3& eyePos) = 0;
	// 设置位移幅度
	virtual void SetHeightScale(float scale) = 0;
	// 设置曲面细分信息
	virtual void SetTessInfo(float maxTessDistance, float minTessDistance, float minTessFactor, float maxTessFactor) = 0;
};

//****************************************************
//		EffectInstance
//****************************************************
class BasicEffect : public IEffect, public IEffectTransform, public IEffectDeferred, public IEffectTextureDiffuse, public IEffectTextureNormal,public IEffectMesh,public IEffectTextureHeight
{
public:
	BasicEffect();
	virtual ~BasicEffect() override;

	BasicEffect(BasicEffect&& moveFrom) noexcept;
	BasicEffect& operator=(BasicEffect&& moveFrom) noexcept;

	// 获取单例
	static BasicEffect& Get();

	// 初始化所需资源
	bool InitAll(ID3D11Device* device);


	//
	// 渲染模式的变更
	//

	//// 默认状态来绘制
	//void SetRenderDefault(ID3D11DeviceContext* deviceContext, RenderType type = RenderType::RenderObject);
	//// 公告板绘制
	//void SetRenderBillboard(ID3D11DeviceContext* deviceContext, bool enableAlphaToCoverage, RenderType type = RenderType::RenderObject);
	//骨骼模型绘制
	void SetRenderBones(ID3D11DeviceContext* deviceContext);
	////法线贴图绘制
	//void SetRenderNormalMap(ID3D11DeviceContext* deviceContext);
	////法线贴图骨骼网格体
	//void SetRenderBonesNormalMap(ID3D11DeviceContext* deviceContext);
	void SetFillMode(RSFillMode mode);
	void SetMeshType(MeshType type)override;
	void SetRenderType(RenderType type);
	void SetIsTransparent(bool transparent);
	void SetIsMatCap(bool isMatCap);
	void SetRenderPass(ID3D11DeviceContext* deviceContext)override;
	
	//材质捕捉
	void SetRenderMatCap(ID3D11DeviceContext* deviceContext);

	//
	//	IHeight
	//
	void SetTextureHeight(ID3D11ShaderResourceView* textureHeight)override;
	// 设置摄像机位置
	void SetEyePos(const DirectX::XMFLOAT3& eyePos)override;
	// 设置位移幅度
	void SetHeightScale(float scale)override;
	// 设置曲面细分信息
	void SetTessInfo(float maxTessDistance, float minTessDistance, float minTessFactor, float maxTessFactor)override;


	//延迟渲染绘制状态
	void SetRenderDefferedPass(ID3D11DeviceContext* deviceContext) override;
	void SetRenderDefferedPreL(ID3D11DeviceContext* deviceContext);
	void SetRenderDefferedDraw(ID3D11DeviceContext* deviceContext) override;
	void SetRenderDefferedOff(ID3D11DeviceContext* deviceContext) override;
	void SetRenderTransparent();

	//
	// 矩阵设置
	//

	void XM_CALLCONV SetWorldMatrix(DirectX::FXMMATRIX W) override;
	void XM_CALLCONV SetViewMatrix(DirectX::FXMMATRIX V) override;
	void XM_CALLCONV SetProjMatrix(DirectX::FXMMATRIX P) override;
	void XM_CALLCONV SetTexTransform(DirectX::FXMMATRIX T);
	void XM_CALLCONV SetShadowTransformMatrix(DirectX::FXMMATRIX S);


	//
	// 光照、材质和纹理相关设置
	//

	// 各种类型灯光允许的最大数目
	static const int maxLights = 5;

	void SetDirLight(size_t pos, const DirectionalLight& dirLight);
	void SetPointLight(size_t pos, const PointLight& pointLight);
	void SetSpotLight(size_t pos, const SpotLight& spotLight);
	void SetPointLightNumber(unsigned count);
	void SetDirLightNumber(unsigned count);
	void SetSpotLightNumber(unsigned count);
	void SetIBLStrength(float strength);

	void SetMaterial(const Material& material);

	// 贴图设置
	void SetTextureDiffuse(ID3D11ShaderResourceView* texture) override;
	void SetTextureArray(ID3D11ShaderResourceView* textures);
	void SetTextureCube(ID3D11ShaderResourceView* textureCube);
	
	//
	// INormal
	//
	void SetUseNormalMap(bool UseMap) override;
	void SetTextureNormal(ID3D11ShaderResourceView* texture) override;
	// 阴影贴图
	void SetTextureShadow(ID3D11ShaderResourceView* texture);
	// SSAO贴图
	void SetTextureSSAO(ID3D11ShaderResourceView* texture);
	void SetEnableSSAO(bool enable);

	void SetAnimeFrame(float frame = -1);
	void SetColorBlend(bool blend = false);
	void SetColor(const DirectX::XMVECTOR& color);

	void SetUAVResource(LPCSTR name, ID3D11UnorderedAccessView* pUAVs, UINT initCounts);
	void SetOITCBuffer(LPCSTR name, float value);
	void SetRenderOIT(BOOL isOn);

	//延迟渲染贴图
	void SetAmbientTexture(ID3D11ShaderResourceView* texture) override;
	void SetDiffuseTexture(ID3D11ShaderResourceView* texture) override;
	void SetSpecTexture(ID3D11ShaderResourceView* texture) override;

	//
	// 状态设置
	//

	//全局雾效设置
	void SetFogState(bool isOn);
	void SetFogStart(float fogStart);
	void SetFogColor(DirectX::XMVECTOR fogColor);
	void SetFogRange(float fogRange);

	//反射设置
	void SetReflectionEnabled(bool enabled);

	//更新骨骼矩阵
	void SetBonesEnabled(bool enabled);
	void SetBonesMaterial(DirectX::XMMATRIX mat[], unsigned num);

	//PBR相关设置
	void SetUsePBR(bool usepbr);
	void SetMetalic(ID3D11ShaderResourceView* texture, float metalic = 0.0f);
	void SetRoughless(ID3D11ShaderResourceView* texture, float roughless = 0.0f);
	void SetIBL(ID3D11ShaderResourceView* DiffuseCube, ID3D11ShaderResourceView* SpecCube, ID3D11ShaderResourceView* Lut);
	void SetTextureIBL(ID3D11ShaderResourceView* Diffuse, ID3D11ShaderResourceView* Spec, ID3D11ShaderResourceView* Lut);

	// 应用常量缓冲区和纹理资源的变更
	void Apply(ID3D11DeviceContext* deviceContext) override;

private:
	class Impl;
	std::unique_ptr<Impl> pImpl;
};

class DeferredEffect:public IEffect
{
public:
	DeferredEffect();
	virtual ~DeferredEffect() override;

	DeferredEffect(DeferredEffect&& moveFrom) noexcept;
	DeferredEffect& operator=(DeferredEffect&& moveFrom) noexcept;

	// 获取单例
	static DeferredEffect& Get();

	// 初始化所需资源
	bool InitAll(ID3D11Device* device);

	void SetFrustumData(float nearPlane, float farPlane, float screenWidth, float ScreenHeight, DirectX::FXMMATRIX V, DirectX::FXMMATRIX InvP);
	void SetTileSizes(DirectX::XMFLOAT4 tilesizes);
	void SetRenderClusterBuild(ID3D11DeviceContext* deviceContext);
	void SetUav(ID3D11UnorderedAccessView* uav);
	// 应用常量缓冲区和纹理资源的变更
	void Apply(ID3D11DeviceContext* deviceContext) override;
private:
	class Impl;
	std::unique_ptr<Impl> pImpl;
};


//class SkyLightEffect :public IEffect
//{
//public:
//	SkyLightEffect();
//	virtual ~SkyLightEffect() override;
//
//	SkyLightEffect(SkyLightEffect&& moveFrom) noexcept;
//	SkyLightEffect& operator=(SkyLightEffect&& moveFrom) noexcept;
//
//	// 获取单例
//	static SkyLightEffect& Get();
//
//	// 初始化所需资源
//	bool InitAll(ID3D11Device* device);
//
//	//
//	// 渲染模式的变更
//	//
//
//	// 默认状态来绘制
//	void SetRenderDefault(ID3D11DeviceContext* deviceContext);
//	void SetRenderWave(ID3D11DeviceContext* deviceContext);
//
//	void SetCameraPos(DirectX::XMFLOAT3 pos);
//	void SetTime(float time);
//	void SetScreenSize(float width, float height);
//	void SetSunPos(DirectX::XMFLOAT3 pos);
//	void SetkRlh(DirectX::XMFLOAT3 kRlh);
//	void SetFloatByName(float resource, LPCSTR name);
//	// 应用常量缓冲区和纹理资源的变更
//	void Apply(ID3D11DeviceContext* deviceContext) override;
//
//private:
//	class Impl;
//	std::unique_ptr<Impl> pImpl;
//};
class SkyEffect : public IEffect, public IEffectTransform
{
public:
	SkyEffect();
	virtual ~SkyEffect() override;

	SkyEffect(SkyEffect&& moveFrom) noexcept;
	SkyEffect& operator=(SkyEffect&& moveFrom) noexcept;

	// 获取单例
	static SkyEffect& Get();

	// 初始化所需资源
	bool InitAll(ID3D11Device* device);


	//
	// IEffectTransform
	//

	void XM_CALLCONV SetWorldMatrix(DirectX::FXMMATRIX W) override;
	void XM_CALLCONV SetViewMatrix(DirectX::FXMMATRIX V) override;
	void XM_CALLCONV SetProjMatrix(DirectX::FXMMATRIX P) override;

	// 
	// SkyEffect
	//

	// 默认状态来绘制
	void SetRenderDefault(ID3D11DeviceContext* deviceContext);
	void SetRenderAtomosphere(ID3D11DeviceContext* deviceContext);

	//IBL的三张贴图
	void SetRenderIradiance(ID3D11DeviceContext* deviceContext);
	void SetRenderPrefilter(ID3D11DeviceContext* deviceContext, float roughness);
	void SetRenderLut(ID3D11DeviceContext* deviceContext);

	void SetRenderBlend(ID3D11DeviceContext* deviceContext);
	void SetUseCube(bool useCube);

	void SetBlendResources(ID3D11ShaderResourceView* textrueCubeFirst, float lerpAlpha, unsigned mipmap, float LerpDura);

	void SetCameraPos(DirectX::XMFLOAT3 pos);
	void SetTime(float time);
	void SetScreenSize(float width, float height);
	void SetSunPos(DirectX::XMFLOAT3 pos);
	void SetkRlh(DirectX::XMFLOAT3 kRlh);
	void SetFloatByName(float resource, LPCSTR name);
	// 设置天空盒
	void SetTextureCube(ID3D11ShaderResourceView* textureCube);
	void SetTextureHDR(ID3D11ShaderResourceView* texture);

	//
	// IEffect
	//

	// 应用常量缓冲区和纹理资源的变更
	void Apply(ID3D11DeviceContext* deviceContext) override;

private:
	class Impl;
	std::unique_ptr<Impl> pImpl;
};
class FadeEffect : public IEffect
{
public:
	FadeEffect();
	virtual ~FadeEffect() override;

	FadeEffect(FadeEffect&& moveFrom) noexcept;
	FadeEffect& operator=(FadeEffect&& moveFrom) noexcept;

	// 获取单例
	static FadeEffect& Get();

	// 初始化ScreenFade.hlsli所需资源并初始化渲染状态
	bool InitAll(ID3D11Device* device);

	// 
	// 渲染模式的变更
	//

	// 默认状态来绘制
	void SetRenderDefault(ID3D11DeviceContext* deviceContext);
	//翻页状态绘制
	void SetRenderCurl(ID3D11DeviceContext* deviceContext);
	//网格状态绘制
	void SetRenderGrid(ID3D11DeviceContext* deviceContext);
	//
	// 矩阵设置
	//

	void XM_CALLCONV SetWorldViewProjMatrix(DirectX::FXMMATRIX W, DirectX::CXMMATRIX V, DirectX::CXMMATRIX P);
	void XM_CALLCONV SetWorldViewProjMatrix(DirectX::FXMMATRIX WVP);

	//
	// 淡入淡出设置
	//

	void SetFadeAmount(float fadeAmount);
	void SetScreenSize(float x, float y);
	void SetMousePosition(float x, float y);

	//
	// 纹理设置
	//

	void SetTexture(ID3D11ShaderResourceView* texture);
	void SetNextTexture(ID3D11ShaderResourceView* texture);

	// 应用常量缓冲区和纹理资源的变更
	void Apply(ID3D11DeviceContext* deviceContext) override;

private:
	class Impl;
	std::unique_ptr<Impl> pImpl;
};
class ShadowEffect : public IEffect, public IEffectTransform, public IEffectTextureDiffuse,public IEffectTextureHeight
{
public:
	ShadowEffect();
	virtual ~ShadowEffect() override;

	ShadowEffect(ShadowEffect&& moveFrom) noexcept;
	ShadowEffect& operator=(ShadowEffect&& moveFrom) noexcept;

	// 获取单例
	static ShadowEffect& Get();

	// 初始化所需资源
	bool InitAll(ID3D11Device* device);

	//
	// IEffectTransform
	//

	void XM_CALLCONV SetWorldMatrix(DirectX::FXMMATRIX W) override;
	void XM_CALLCONV SetViewMatrix(DirectX::FXMMATRIX V) override;
	void XM_CALLCONV SetProjMatrix(DirectX::FXMMATRIX P) override;

	//
	// IEffectTextureDiffuse
	//

	// 设置漫反射纹理
	void SetTextureDiffuse(ID3D11ShaderResourceView* textureDiffuse) override;

	// 
	// ShadowEffect
	//

	// 默认状态来绘制
	void SetRenderDefault(ID3D11DeviceContext* deviceContext);

	// Alpha裁剪绘制(处理具有透明度的物体)
	void SetRenderAlphaClip(ID3D11DeviceContext* deviceContext);

	//
	// IEffectTextureHeight
	//
	void SetTextureHeight(ID3D11ShaderResourceView* textureDiffuse)override;
	// 设置摄像机位置
	void SetEyePos(const DirectX::XMFLOAT3& eyePos)override;
	// 设置位移幅度
	void SetHeightScale(float scale)override;
	// 设置曲面细分信息
	void SetTessInfo(float maxTessDistance, float minTessDistance, float minTessFactor, float maxTessFactor)override;
	//
	// IEffect
	//
	void SetRenderPass(ID3D11DeviceContext* deviceContext)override;
	// 应用常量缓冲区和纹理资源的变更
	void Apply(ID3D11DeviceContext* deviceContext) override;

private:
	class Impl;
	std::unique_ptr<Impl> pImpl;
};
class Texture2DEffect : public IEffect, public IEffectTextureDiffuse
{
public:
	Texture2DEffect();
	virtual ~Texture2DEffect() override;

	Texture2DEffect(Texture2DEffect&& moveFrom) noexcept;
	Texture2DEffect& operator=(Texture2DEffect&& moveFrom) noexcept;

	// 获取单例
	static Texture2DEffect& Get();

	// 初始化所需资源
	bool InitAll(ID3D11Device* device);

	

	//
	// IEffectTextureDiffuse
	//

	void SetTextureDiffuse(ID3D11ShaderResourceView* textureDiffuse) override;
	void SetTextureArray(ID3D11ShaderResourceView* textureDiffuse);
	void SetTextureCube(ID3D11ShaderResourceView* textureDiffuse);

	void SetScreenSize(float width, float height);
	void SetPosition(const DirectX::XMFLOAT2& pos);
	void SetSize(const DirectX::XMFLOAT2& size);
	// 
	// Texture2DEffect
	//

	// 默认状态来绘制
	void SetRenderDefault(ID3D11DeviceContext* deviceContext);

	// 绘制单通道(0-R, 1-G, 2-B)
	void SetRenderOneComponent(ID3D11DeviceContext* deviceContext, int index);

	// 绘制单通道，但以灰度的形式呈现(0-R, 1-G, 2-B, 3-A)
	void SetRenderOneComponentGray(ID3D11DeviceContext* deviceContext, int index);

	void SetRenderArray(ID3D11DeviceContext* deviceContext, int index);

	void SetRenderCube(ID3D11DeviceContext* deviceContext, int index);
	void SetRenderToneMap(ID3D11DeviceContext* deviceContext);
	//
	// IEffect
	//

	// 应用常量缓冲区和纹理资源的变更
	void Apply(ID3D11DeviceContext* deviceContext) override;

private:
	class Impl;
	std::unique_ptr<Impl> pImpl;
};
class SSAOEffect : public IEffect, public IEffectTransform, public IEffectTextureDiffuse,public IEffectTextureHeight
{
public:

	SSAOEffect();
	virtual ~SSAOEffect() override;

	SSAOEffect(SSAOEffect&& moveFrom) noexcept;
	SSAOEffect& operator=(SSAOEffect&& moveFrom) noexcept;

	// 获取单例
	static SSAOEffect& Get();

	// 初始化所需资源
	bool InitAll(ID3D11Device* device);

	//
	// IEffectTransform
	//

	void XM_CALLCONV SetWorldMatrix(DirectX::FXMMATRIX W) override;
	void XM_CALLCONV SetViewMatrix(DirectX::FXMMATRIX V) override;
	void XM_CALLCONV SetProjMatrix(DirectX::FXMMATRIX P) override;

	//
	// IEffectTextureDiffuse
	//

	// 设置漫反射纹理
	void SetTextureDiffuse(ID3D11ShaderResourceView* textureDiffuse) override;

	// 
	// SSAOEffect
	//

	// 绘制法向量和深度贴图
	void SetRenderNormalDepth(ID3D11DeviceContext* deviceContext, RenderType type, bool enableAlphaClip = false);

	// 绘制SSAO图
	void SetRenderSSAOMap(ID3D11DeviceContext* deviceContext, int sampleCount);

	// 对SSAO图进行双边滤波
	void SetRenderBilateralBlur(ID3D11DeviceContext* deviceContext, bool horizontalBlur);


	// 设置观察空间的深度/法向量贴图
	void SetTextureNormalDepth(ID3D11ShaderResourceView* textureNormalDepth);
	// 设置随机向量纹理
	void SetTextureRandomVec(ID3D11ShaderResourceView* textureRandomVec);
	// 设置待模糊的纹理
	void SetTextureBlur(ID3D11ShaderResourceView* textureBlur);
	// 设置偏移向量
	void SetOffsetVectors(const DirectX::XMFLOAT4 offsetVectors[14]);
	// 设置视锥体远平面顶点
	void SetFrustumCorners(const DirectX::XMFLOAT4 frustumCorners[4]);
	// 设置遮蔽信息
	void SetOcclusionInfo(float radius, float fadeStart, float fadeEnd, float surfaceEpsilon);
	// 设置模糊权值
	void SetBlurWeights(const float weights[11]);
	// 设置模糊半径
	void SetBlurRadius(int radius);

	//
	// IEffectTextureHeight
	//

	void SetTextureHeight(ID3D11ShaderResourceView* textureDiffuse)override;
	// 设置摄像机位置
	void SetEyePos(const DirectX::XMFLOAT3& eyePos)override;
	// 设置位移幅度
	void SetHeightScale(float scale)override;
	// 设置曲面细分信息
	void SetTessInfo(float maxTessDistance, float minTessDistance, float minTessFactor, float maxTessFactor)override;

	//
	// IEffect
	//
	void SetRenderPass(ID3D11DeviceContext* deviceContext)override;
	// 应用常量缓冲区和纹理资源的变更
	void Apply(ID3D11DeviceContext* deviceContext) override;

private:
	class Impl;
	std::unique_ptr<Impl> pImpl;
};
class ParticleEffect : public IEffect
{
public:
	ParticleEffect();
	virtual ~ParticleEffect() override;

	ParticleEffect(ParticleEffect&& moveFrom) noexcept;
	ParticleEffect& operator=(ParticleEffect&& moveFrom) noexcept;

	// 初始化所需资源
	// 若effectPath为HLSL/Fire
	// 则会寻找文件: 
	// - HLSL/Fire_SO_VS.hlsl
	// - HLSL/Fire_SO_GS.hlsl
	// - HLSL/Fire_VS.hlsl
	// - HLSL/Fire_GS.hlsl
	// - HLSL/Fire_PS.hlsl
	bool Init(ID3D11Device* device, const std::wstring& effectPath);

	// 产生新粒子到顶点缓冲区
	void SetRenderToVertexBuffer(ID3D11DeviceContext* deviceContext);
	// 绘制粒子系统
	void SetRenderDefault(ID3D11DeviceContext* deviceContext);

	void XM_CALLCONV SetViewProjMatrix(DirectX::FXMMATRIX VP);

	void SetEyePos(const DirectX::XMFLOAT3& eyePos);

	void SetGameTime(float t);
	void SetTimeStep(float step);

	void SetEmitDir(const DirectX::XMFLOAT3& dir);
	void SetEmitPos(const DirectX::XMFLOAT3& pos);

	void SetEmitInterval(float t);
	void SetAliveTime(float t);

	void SetTextureArray(ID3D11ShaderResourceView* textureArray);
	void SetTextureRandom(ID3D11ShaderResourceView* textureRandom);

	void SetBlendState(ID3D11BlendState* blendState, const FLOAT blendFactor[4], UINT sampleMask);
	void SetDepthStencilState(ID3D11DepthStencilState* depthStencilState, UINT stencilRef);

	void SetDebugObjectName(const std::string& name);

	// 
	// IEffect
	//

	// 应用常量缓冲区和纹理资源的变更
	void Apply(ID3D11DeviceContext* deviceContext) override;

private:
	class Impl;
	std::unique_ptr<Impl> pImpl;
};

class PostProcessEffect :public IEffect
{
	PostProcessEffect();
	virtual ~PostProcessEffect() override;

	PostProcessEffect(PostProcessEffect&& moveFrom) noexcept;
	PostProcessEffect& operator=(PostProcessEffect&& moveFrom) noexcept;

	// 初始化所需资源
	bool InitAll(ID3D11Device* device);


	// 获取单例
	static PostProcessEffect& Get();

	
	// 绑定渲染管线
	virtual void SetRenderPass(ID3D11DeviceContext* deviceContext);

	// 更新并绑定常量缓冲区
	virtual void Apply(ID3D11DeviceContext* deviceContext);

private:
	class Impl;
	std::unique_ptr<Impl> pImpl;
};

#endif
