//#include "Effects.h"
//#include "d3dUtil.h"
//#include "EffectHelper.h"
//#include "DXTrace.h"
//#include "Vertex.h"
//using namespace DirectX;
//class SkyLightEffect::Impl
//{
//public:
//	// 必须显式指定
//	Impl() : m_IsDirty() {}
//	~Impl() = default;
//public:
//
//	std::unique_ptr<EffectHelper> m_pEffectHelper;
//
//	std::shared_ptr<IEffectPass> m_pCurrEffectPass;
//
//	ComPtr<ID3D11InputLayout> m_pVertexPosTexLayout;
//};
//namespace
//{
//	static SkyLightEffect* g_pInstance = nullptr;
//}
//
//SkyLightEffect::SkyLightEffect()
//{
//	if (g_pInstance)
//		throw std::exception("AtomosphereEffect is a singleton!");
//	g_pInstance = this;
//	pImpl = std::make_unique<SkyLightEffect::Impl>();
//}
//SkyLightEffect::~SkyLightEffect()
//{
//}
//
//SkyLightEffect::SkyLightEffect(SkyLightEffect&& moveFrom) noexcept
//{
//}
//
//SkyLightEffect& SkyLightEffect::operator=(SkyLightEffect&& moveFrom) noexcept
//{
//	pImpl.swap(moveFrom.pImpl);
//	return *this;
//}
//
//SkyLightEffect& SkyLightEffect::Get()
//{
//	if (!g_pInstance)
//		throw std::exception("SkyLightEffect needs an instance!");
//	return *g_pInstance;
//}
//
//bool SkyLightEffect::InitAll(ID3D11Device* device)
//{
//	if (!device)
//		return false;
//
//	if (!RenderStates::IsInit())
//		throw std::exception("RenderStates need to be initialized first!");
//
//	pImpl->m_pEffectHelper = std::make_unique<EffectHelper>();
//	ComPtr<ID3DBlob> blob;
//
//	// ******************
//	// 2D全屏绘制
//	// 
//	HR(CreateShaderFromFile(L"HLSL\\Atomosphere_VS.cso", L"HLSL\\Atomosphere_VS.hlsl", "VS", "vs_5_0", blob.ReleaseAndGetAddressOf()));
//	HR(pImpl->m_pEffectHelper->AddShader("Atomosphere_VS", device, blob.Get()));
//	// 创建顶点输入布局
//	HR(device->CreateInputLayout(VertexPosTex::inputLayout, ARRAYSIZE(VertexPosTex::inputLayout), blob->GetBufferPointer(),
//		blob->GetBufferSize(), pImpl->m_pVertexPosTexLayout.GetAddressOf()));
//
//	HR(CreateShaderFromFile(L"HLSL\\Atomo3.cso", L"HLSL\\Atomo3.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
//	HR(pImpl->m_pEffectHelper->AddShader("Atomo3", device, blob.Get()));
//
//	/*HR(CreateShaderFromFile(L"HLSL\\WaveBox_PS.cso", L"HLSL\\WaveBox_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
//	HR(pImpl->m_pEffectHelper->AddShader("WaveBox_PS", device, blob.Get()));*/
//
//	EffectPassDesc passDesc;
//	passDesc.nameVS = "Atomosphere_VS";
//	passDesc.namePS = "Atomo3";
//	HR(pImpl->m_pEffectHelper->AddEffectPass("SkyLight", device, &passDesc));
//	//pImpl->m_pEffectHelper->GetEffectPass("SkyLight")->SetDepthStencilState(RenderStates::DSSNoDepthWrite.Get(), 0);
//	pImpl->m_pEffectHelper->GetEffectPass("SkyLight")->SetDepthStencilState(RenderStates::DSSLessEqual.Get(), 0);
//
//	/*passDesc.nameVS = "Atomosphere_VS";
//	passDesc.namePS = "WaveBox_PS";
//	HR(pImpl->m_pEffectHelper->AddEffectPass("WaveBox", device, &passDesc));
//	pImpl->m_pEffectHelper->GetEffectPass("WaveBox")->SetDepthStencilState(RenderStates::DSSLessEqual.Get(), 0);*/
//
//	D3D11SetDebugObjectName(pImpl->m_pVertexPosTexLayout.Get(), "VertexPosTexLayout");
//
//	return true;
//}
//
//void SkyLightEffect::SetRenderDefault(ID3D11DeviceContext* deviceContext)
//{
//	deviceContext->IASetInputLayout(pImpl->m_pVertexPosTexLayout.Get());
//
//	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("SkyLight");
//}
//
//void SkyLightEffect::SetRenderWave(ID3D11DeviceContext* deviceContext)
//{
//	deviceContext->IASetInputLayout(pImpl->m_pVertexPosTexLayout.Get());
//
//	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("WaveBox");
//}
//
//void SkyLightEffect::SetCameraPos(DirectX::XMFLOAT3 pos)
//{
//	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_CameraPos")->SetFloatVector(3, (FLOAT*)&pos);
//}
//
//void SkyLightEffect::SetTime(float time)
//{
//	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_time")->SetFloat(time);
//}
//
//void SkyLightEffect::SetScreenSize(float width, float height)
//{
//	XMFLOAT2 size(width, height);
//	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_ScreenSize")->SetFloatVector(2, (FLOAT*)&size);
//}
//
//void SkyLightEffect::SetMousePos(float x, float y)
//{
//	XMFLOAT2 pos(x, y);
//	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_MousePosition")->SetFloatVector(2, (FLOAT*)&pos);
//}
//
//void XM_CALLCONV SkyLightEffect::SetViewRotMat(FXMMATRIX V)
//{
//	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_rot")->SetFloatMatrix(4, 4, (FLOAT*)&V);
//}
//
//void XM_CALLCONV SkyLightEffect::SetProjMat(DirectX::FXMMATRIX V)
//{
//	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_Proj")->SetFloatMatrix(4, 4, (FLOAT*)&V);
//}
//
//void SkyLightEffect::SetSunPos(DirectX::XMFLOAT3 pos)
//{
//	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_SunPos")->SetFloatVector(3, (FLOAT*)&pos);
//}
//
//void SkyLightEffect::SetkRlh(DirectX::XMFLOAT3 kRlh)
//{
//	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_kRlh")->SetFloatVector(3, (FLOAT*)&kRlh);
//}
//
//void SkyLightEffect::SetGamma(float gamma)
//{
//	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_Gamma")->SetFloat(gamma);
//}
//
//void SkyLightEffect::SetExposure(float exposure)
//{
//	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_Exposure")->SetFloat(exposure);
//}
//
//void SkyLightEffect::SetFloatByName(float resource, LPCSTR name)
//{
//	pImpl->m_pEffectHelper->GetConstantBufferVariable(name)->SetFloat(resource);
//}
//
//void SkyLightEffect::Apply(ID3D11DeviceContext* deviceContext)
//{
//	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
//	pImpl->m_pCurrEffectPass->Apply(deviceContext);
//}
