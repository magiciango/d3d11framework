#pragma once
#include "d3dApp.h"
#include <map>
class TextureManager
{
	template <class T>
	using ComPtr = Microsoft::WRL::ComPtr<T>;
public:
	static ID3D11ShaderResourceView* Load(const wchar_t* filename);
private:
	static std::map<const wchar_t*, ComPtr<ID3D11ShaderResourceView>> m_Textures;
};