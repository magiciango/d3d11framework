#include "DeferredRender.h"
#include "d3dUtil.h"
#include "DXTrace.h"
#include "Effects.h"
#include "ScreenGrab11.h"
#include "d3dApp.h"

#define CLUTER_SIZE_X	16
#define CLUTER_SIZE_Y	16
#define CLUTER_SIZE_Z	16


void DeferredRender::InitResource(ID3D11Device* device, ID3D11DeviceContext* deviceContext)
{
	D3D11_TEXTURE2D_DESC texDesc;
	texDesc.Width = 256;
	texDesc.Height = 256;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;

	HR(device->CreateTexture2D(&texDesc, nullptr, m_pTexture2D.GetAddressOf()));

	/*D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
	uavDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	uavDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
	uavDesc.Texture2D.MipSlice = 0;*/
	//HR(device->CreateUnorderedAccessView(m_pTexture2D.Get(), &uavDesc, m_pUAV.GetAddressOf()));

	/*D3D11_BUFFER_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
	desc.ByteWidth = sizeof(XMFLOAT4) * 2 * CLUTER_SIZE_X * CLUTER_SIZE_Y * CLUTER_SIZE_Z;
	desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	desc.StructureByteStride = sizeof(XMFLOAT4) * 2;
	desc.Usage = D3D11_USAGE_STAGING;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;*/
	CreateStructuredBuffer(device, nullptr, sizeof(XMFLOAT4) * 2 * CLUTER_SIZE_X * CLUTER_SIZE_Y * CLUTER_SIZE_Z, sizeof(XMFLOAT4) * 2, m_pClusterListBuffer.GetAddressOf(), false, true);
	//HR(device->CreateBuffer(&desc, nullptr, m_pClusterListBuffer.GetAddressOf()));

	D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
	//uavDesc.Format = DXGI_FORMAT_UNKNOWN;
	uavDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	//uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	uavDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
	uavDesc.Texture2D.MipSlice = 0;
	/*uavDesc.Buffer.FirstElement = 0;
	uavDesc.Buffer.Flags = 0;
	uavDesc.Buffer.NumElements = CLUTER_SIZE_X * CLUTER_SIZE_Y * CLUTER_SIZE_Z;*/
	HR(device->CreateUnorderedAccessView(m_pTexture2D.Get(), &uavDesc, m_pClusterListUAV.GetAddressOf()));
	//HR(device->CreateUnorderedAccessView(m_pClusterListBuffer.Get(), &uavDesc, m_pClusterListUAV.GetAddressOf()));
}

void DeferredRender::Draw(ID3D11Device* device, ID3D11DeviceContext* deviceContext)
{
	auto& effect = DeferredEffect::Get();
	effect.SetRenderClusterBuild(deviceContext);
	effect.SetUav(m_pUAV.Get());
	effect.Apply(deviceContext);
	deviceContext->Dispatch(16, 16, 1);
	DirectX::SaveWICTextureToFile(deviceContext, m_pTexture2D.Get(), GUID_ContainerFormatPng, L"Test.png");
}

void DeferredRender::PreClusterCompute(ID3D11DeviceContext* deviceContext,Camera* camera)
{
	m_TileSizes.x = CLUTER_SIZE_X;
	m_TileSizes.y = CLUTER_SIZE_Y;
	m_TileSizes.z = CLUTER_SIZE_Z;
	m_TileSizes.w = (float)D3DAppGet::Width() / (float)CLUTER_SIZE_X;

	m_ClusterFactor.x = (float)CLUTER_SIZE_Z / log2f(camera->GetFar() / camera->GetNear());
	m_ClusterFactor.y = -((float)CLUTER_SIZE_Z * log2f(camera->GetNear())) / log2f(camera->GetFar() / camera->GetNear());

	auto& effect = DeferredEffect::Get();
	effect.SetRenderClusterBuild(deviceContext);

	XMMATRIX P;// = camera->GetProjXM();
	P = InverseTranspose(camera->GetProjXM());
	effect.SetFrustumData(camera->GetNear(), camera->GetFar(), D3DAppGet::Width(), D3DAppGet::Height(), camera->GetViewXM(), P);
	effect.SetTileSizes(m_TileSizes);
	
	effect.Apply(deviceContext);
	deviceContext->Dispatch(CLUTER_SIZE_X, CLUTER_SIZE_Y, CLUTER_SIZE_Z);

}
