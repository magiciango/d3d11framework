#include "TextureManager.h"
#include "d3dUtil.h"
#include "DXTrace.h"

std::map<const wchar_t*, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> TextureManager::m_Textures;

ID3D11ShaderResourceView* TextureManager::Load(const wchar_t* filename)
{
	if (!filename)
		return nullptr;
	if (m_Textures[filename])
	{
		return m_Textures[filename].Get();
	}
	
	ComPtr<ID3D11ShaderResourceView> texture;
	std::pair< const wchar_t*, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> pair(filename, texture);
	m_Textures.insert(pair);
	HR(CreateWICTextureFromFile(D3DAppGet::pDevice(), filename, nullptr, m_Textures[filename].GetAddressOf()));
	return m_Textures[filename].Get();
}
