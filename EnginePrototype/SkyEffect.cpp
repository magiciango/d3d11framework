#include "Effects.h"
#include "d3dUtil.h"
#include "EffectHelper.h"	// 必须晚于Effects.h和d3dUtil.h包含
#include "DXTrace.h"
#include "Vertex.h"
using namespace DirectX;


//
// SkyEffect::Impl 需要先于SkyEffect的定义
//

class SkyEffect::Impl
{

public:
	// 必须显式指定
	Impl() {}
	~Impl() = default;

public:
	std::unique_ptr<EffectHelper> m_pEffectHelper;
	std::shared_ptr<IEffectPass> m_pCurrEffectPass;

	ComPtr<ID3D11InputLayout> m_pVertexPosLayout;
	ComPtr<ID3D11InputLayout> m_pVertexPosTexLayout;

	XMFLOAT4X4 m_World, m_View, m_Proj;
};

//
// SkyEffect
//

namespace
{
	// SkyEffect单例
	static SkyEffect* g_pInstance = nullptr;
}

SkyEffect::SkyEffect()
{
	if (g_pInstance)
		throw std::exception("BasicEffect is a singleton!");
	g_pInstance = this;
	pImpl = std::make_unique<SkyEffect::Impl>();
}

SkyEffect::~SkyEffect()
{
}

SkyEffect::SkyEffect(SkyEffect&& moveFrom) noexcept
{
	pImpl.swap(moveFrom.pImpl);
}

SkyEffect& SkyEffect::operator=(SkyEffect&& moveFrom) noexcept
{
	pImpl.swap(moveFrom.pImpl);
	return *this;
}

SkyEffect& SkyEffect::Get()
{
	if (!g_pInstance)
		throw std::exception("BasicEffect needs an instance!");
	return *g_pInstance;
}

bool SkyEffect::InitAll(ID3D11Device* device)
{
	if (!device)
		return false;

	if (!RenderStates::IsInit())
		throw std::exception("RenderStates need to be initialized first!");

	pImpl->m_pEffectHelper = std::make_unique<EffectHelper>();

	ComPtr<ID3DBlob> blob;

	// ******************
	// 创建顶点着色器
	//

	HR(CreateShaderFromFile(L"HLSL\\SkyBox_VS.cso", L"HLSL\\SkyBox_VS.hlsl", "VS", "vs_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Sky_VS", device, blob.Get()));

	// 创建顶点布局
	HR(device->CreateInputLayout(VertexPosTex::inputLayout, ARRAYSIZE(VertexPosTex::inputLayout),
		blob->GetBufferPointer(), blob->GetBufferSize(), pImpl->m_pVertexPosLayout.GetAddressOf()));

	HR(CreateShaderFromFile(L"HLSL\\Atomosphere_VS.cso", L"HLSL\\Atomosphere_VS.hlsl", "VS", "vs_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Atomosphere_VS", device, blob.Get()));

	HR(CreateShaderFromFile(L"HLSL\\SkyBox_2DTex_VS.cso", L"HLSL\\SkyBox_2DTex_VS.hlsl", "VS", "vs_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("SkyBox_2DTex_VS", device, blob.Get()));

	// 创建顶点布局
	HR(device->CreateInputLayout(VertexPosTex::inputLayout, ARRAYSIZE(VertexPosTex::inputLayout),
		blob->GetBufferPointer(), blob->GetBufferSize(), pImpl->m_pVertexPosTexLayout.GetAddressOf()));


	// ******************
	// 创建像素着色器
	//
	HR(CreateShaderFromFile(L"HLSL\\SkyBox_PS.cso", L"HLSL\\SkyBox_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Sky_PS", device, blob.Get()));

	HR(CreateShaderFromFile(L"HLSL\\Atomosphere_PS.cso", L"HLSL\\Atomosphere_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("Atomosphere_PS", device, blob.Get()));

	HR(CreateShaderFromFile(L"HLSL\\SkyBox_ToIradiance_PS.cso", L"HLSL\\SkyBox_ToIradiance_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("SkyBox_ToIradiance_PS", device, blob.Get()));

	HR(CreateShaderFromFile(L"HLSL\\SkyBox_Prefilter_PS.cso", L"HLSL\\SkyBox_Prefilter_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("SkyBox_Prefilter_PS", device, blob.Get()));

	HR(CreateShaderFromFile(L"HLSL\\SkyBox_brdfLUT_PS.cso", L"HLSL\\SkyBox_brdfLUT_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("SkyBox_brdfLUT_PS", device, blob.Get()));


	HR(CreateShaderFromFile(L"HLSL\\SkyBox_BlendIBL_PS.cso", L"HLSL\\SkyBox_BlendIBL_PS.hlsl", "PS", "ps_5_0", blob.ReleaseAndGetAddressOf()));
	HR(pImpl->m_pEffectHelper->AddShader("SkyBox_BlendIBL_PS", device, blob.Get()));
	// ******************
	// 创建通道
	//
	EffectPassDesc passDesc;
	passDesc.nameVS = "Sky_VS";
	passDesc.namePS = "Sky_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("Sky", device, &passDesc));
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("Sky");
	pImpl->m_pCurrEffectPass->SetDepthStencilState(RenderStates::DSSLessEqual.Get(), 0);
	pImpl->m_pCurrEffectPass->SetRasterizerState(RenderStates::RSNoCull.Get());

	passDesc.nameVS = "Atomosphere_VS";
	passDesc.namePS = "Atomosphere_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("Atomosphere", device, &passDesc));
	//pImpl->m_pEffectHelper->GetEffectPass("SkyLight")->SetDepthStencilState(RenderStates::DSSNoDepthWrite.Get(), 0);
	pImpl->m_pEffectHelper->GetEffectPass("Atomosphere")->SetDepthStencilState(RenderStates::DSSLessEqual.Get(), 0);

	passDesc.nameVS = "Sky_VS";
	passDesc.namePS = "SkyBox_ToIradiance_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("ToIradiance", device, &passDesc));
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("ToIradiance");
	pImpl->m_pCurrEffectPass->SetDepthStencilState(RenderStates::DSSLessEqual.Get(), 0);
	pImpl->m_pCurrEffectPass->SetRasterizerState(RenderStates::RSNoCull.Get());

	passDesc.nameVS = "Sky_VS";
	passDesc.namePS = "SkyBox_Prefilter_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("Prefilter", device, &passDesc));
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("Prefilter");
	pImpl->m_pCurrEffectPass->SetDepthStencilState(RenderStates::DSSLessEqual.Get(), 0);
	pImpl->m_pCurrEffectPass->SetRasterizerState(RenderStates::RSNoCull.Get());

	passDesc.nameVS = "SkyBox_2DTex_VS";
	passDesc.namePS = "SkyBox_brdfLUT_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("Lut", device, &passDesc));
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("Lut");
	pImpl->m_pCurrEffectPass->SetDepthStencilState(RenderStates::DSSLessEqual.Get(), 0);
	pImpl->m_pCurrEffectPass->SetRasterizerState(RenderStates::RSNoCull.Get());

	passDesc.nameVS = "SkyBox_2DTex_VS";
	passDesc.namePS = "SkyBox_BlendIBL_PS";
	HR(pImpl->m_pEffectHelper->AddEffectPass("BlendIBL", device, &passDesc));

	pImpl->m_pEffectHelper->SetSamplerStateByName("g_Sam", RenderStates::SSLinearWrap.Get());
	

	// 设置调试对象名
	D3D11SetDebugObjectName(pImpl->m_pVertexPosLayout.Get(), "SkyEffect.VertexPosLayout");
	pImpl->m_pEffectHelper->SetDebugObjectName("SkyEffect");

	return true;
}

void SkyEffect::SetRenderDefault(ID3D11DeviceContext* deviceContext)
{
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosLayout.Get());
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("Sky");
}

void SkyEffect::SetRenderAtomosphere(ID3D11DeviceContext* deviceContext)
{
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosTexLayout.Get());
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("Atomosphere");
}

void SkyEffect::SetRenderIradiance(ID3D11DeviceContext* deviceContext)
{
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosLayout.Get());
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("ToIradiance");
}

void SkyEffect::SetRenderPrefilter(ID3D11DeviceContext* deviceContext, float roughness)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_roughness")->SetFloat(roughness);
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosLayout.Get());
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("Prefilter");
}

void SkyEffect::SetRenderLut(ID3D11DeviceContext* deviceContext)
{
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosTexLayout.Get());
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("Lut");
}

void SkyEffect::SetRenderBlend(ID3D11DeviceContext* deviceContext)
{
	deviceContext->IASetInputLayout(pImpl->m_pVertexPosTexLayout.Get());
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImpl->m_pCurrEffectPass = pImpl->m_pEffectHelper->GetEffectPass("BlendIBL");
}

void XM_CALLCONV SkyEffect::SetWorldMatrix(DirectX::FXMMATRIX W)
{
	XMStoreFloat4x4(&pImpl->m_World, W);
}

void XM_CALLCONV SkyEffect::SetViewMatrix(DirectX::FXMMATRIX V)
{
	XMStoreFloat4x4(&pImpl->m_View, V);
}

void XM_CALLCONV SkyEffect::SetProjMatrix(DirectX::FXMMATRIX P)
{
	XMStoreFloat4x4(&pImpl->m_Proj, P);
}
void SkyEffect::SetUseCube(bool useCube)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_DrawCube")->SetUInt(useCube);
}
void SkyEffect::SetTextureCube(ID3D11ShaderResourceView* m_pTextureCube)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_TexCube", m_pTextureCube);
}

void SkyEffect::SetTextureHDR(ID3D11ShaderResourceView* texture)
{
	pImpl->m_pEffectHelper->SetShaderResourceByName("g_HDRTexture", texture);
}

void SkyEffect::SetBlendResources(ID3D11ShaderResourceView* textrueCubeFirst, float lerpAlpha, unsigned mipmap, float LerpDura)
{

	pImpl->m_pEffectHelper->SetShaderResourceByName("g_Input", textrueCubeFirst);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_Lerp")->SetFloat(lerpAlpha);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_MipMap")->SetUInt(mipmap);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_LerpDura")->SetFloat(lerpAlpha);
}
void SkyEffect::SetCameraPos(DirectX::XMFLOAT3 pos)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_CameraPos")->SetFloatVector(3, (FLOAT*)&pos);
}
void SkyEffect::SetTime(float time)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_time")->SetFloat(time);
}
void SkyEffect::SetScreenSize(float width, float height)
{
	XMFLOAT2 size(width, height);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_ScreenSize")->SetFloatVector(2, (FLOAT*)&size);
}
void SkyEffect::SetSunPos(DirectX::XMFLOAT3 pos)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_SunPos")->SetFloatVector(3, (FLOAT*)&pos);
}
void SkyEffect::SetkRlh(DirectX::XMFLOAT3 kRlh)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_kRlh")->SetFloatVector(3, (FLOAT*)&kRlh);
}
void SkyEffect::SetFloatByName(float resource, LPCSTR name)
{
	pImpl->m_pEffectHelper->GetConstantBufferVariable(name)->SetFloat(resource);
}

void SkyEffect::Apply(ID3D11DeviceContext* deviceContext)
{
	XMMATRIX V = XMLoadFloat4x4(&pImpl->m_View);
	XMMATRIX WVP = XMLoadFloat4x4(&pImpl->m_World) * XMLoadFloat4x4(&pImpl->m_View) * XMLoadFloat4x4(&pImpl->m_Proj);
	WVP = XMMatrixTranspose(WVP);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_WorldViewProj")->SetFloatMatrix(4, 4, (const FLOAT*)&WVP);
	pImpl->m_pEffectHelper->GetConstantBufferVariable("g_View")->SetFloatMatrix(4, 4, (const FLOAT*)&V);
	pImpl->m_pCurrEffectPass->Apply(deviceContext);
}

