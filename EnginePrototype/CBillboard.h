#pragma once
#include "Component.h"
#include <map>
using namespace std;
class CBillboard :public Component
{
	template <class T>
	using ComPtr = Microsoft::WRL::ComPtr<T>;
public:
	CBillboard(Actor* obj);
	static size_t tag;

	void Init(ID3D11Device* device);
	void SetTexture(ID3D11ShaderResourceView* texture);
	void Draw();
	void SetAnimeFrame(float frame);
private:
	ComPtr<ID3D11Buffer> mPointSpritesBuffer;
	ComPtr<ID3D11ShaderResourceView> m_pTexture;
	Material m_Material;
	float m_AnimeFrame = -1;
};
