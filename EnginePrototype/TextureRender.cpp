#include "TextureRender.h"

HRESULT TextureRender::InitResource(int texWidth, int texHeight, bool UseDepth,
	unsigned bufferCount, DXGI_FORMAT format, 
	 bool generateMips, ID3D11Device* device)
{
	if (bufferCount > MaxBufferCount)
		return false;
	m_BufferCount = bufferCount;
	m_DepthMap = UseDepth;
	//防止内存泄漏
	for (auto& i : m_pOutputTextureSRV)
	{
		i.Reset();
	}

	for (auto& i : m_pOutputTextureRTV)
	{
		i.Reset();
	}
	
	for (auto& i : m_pTexture)
	{
		i.Reset();
	}

	m_pOutputTextureDSV.Reset();
	m_pCacheRTV.Reset();
	m_pCacheDSV.Reset();

	m_GenerateMips = generateMips;
	HRESULT hr;
	//非纯深度RTT下创建一般RTT
	if (bufferCount > 0)
	{
		//纹理描述
		D3D11_TEXTURE2D_DESC texDesc;

		texDesc.Width = texWidth;
		texDesc.Height = texHeight;
		texDesc.MipLevels = (m_GenerateMips ? 0 : 1);	// 0为完整mipmap链
		texDesc.ArraySize = 1;
		texDesc.SampleDesc.Count = 1;
		texDesc.SampleDesc.Quality = 0;
		texDesc.Format = format;
		texDesc.Usage = D3D11_USAGE_DEFAULT;
		texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		texDesc.CPUAccessFlags = 0;
		texDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

		//渲染视图描述
		D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
		rtvDesc.Format = texDesc.Format;
		rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		rtvDesc.Texture2D.MipSlice = 0;

		//着色器资源视图描述
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.Format = texDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.MipLevels = -1;	// 使用所有的mip等级

		for (int i = 0; i < bufferCount; ++i)
		{
			//创建纹理
			ComPtr<ID3D11Texture2D> texture;
			hr = device->CreateTexture2D(&texDesc, nullptr, texture.GetAddressOf());

			if (FAILED(hr))
				return hr;

			//创建渲染视图
			hr = device->CreateRenderTargetView(texture.Get(), &rtvDesc, m_pOutputTextureRTV[i].GetAddressOf());

			if (FAILED(hr))
				return hr;

			//创建着色器视图
			hr = device->CreateShaderResourceView(texture.Get(), &srvDesc,
				m_pOutputTextureSRV[i].GetAddressOf());

			if (FAILED(hr))
				return hr;
		}
	}
	
	//
	//创建模板/深度视图
	//
	CD3D11_TEXTURE2D_DESC texDesc((m_DepthMap ? DXGI_FORMAT_R24G8_TYPELESS : DXGI_FORMAT_D24_UNORM_S8_UINT),
		texWidth, texHeight, 1, 1,
		D3D11_BIND_DEPTH_STENCIL | (m_DepthMap ? D3D11_BIND_SHADER_RESOURCE : 0));
	ComPtr<ID3D11Texture2D> depthTex;
	hr = device->CreateTexture2D(&texDesc, nullptr, depthTex.GetAddressOf());

	if (FAILED(hr))
		return hr;

	CD3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc(depthTex.Get(), 
		D3D11_DSV_DIMENSION_TEXTURE2D, 
		DXGI_FORMAT_D24_UNORM_S8_UINT);

	hr = device->CreateDepthStencilView(depthTex.Get(), &dsvDesc,
		m_pOutputTextureDSV.GetAddressOf());
	if (FAILED(hr))
		return hr;
	if (m_DepthMap)
	{
		CD3D11_SHADER_RESOURCE_VIEW_DESC srvDesc(depthTex.Get(), D3D11_SRV_DIMENSION_TEXTURE2D, DXGI_FORMAT_R24_UNORM_X8_TYPELESS);

		hr = device->CreateShaderResourceView(depthTex.Get(), &srvDesc,
			m_pOutputTextureSRV[bufferCount].GetAddressOf());
		if (FAILED(hr))
			return hr;
	}
	// ******************
	// 5. 初始化视口
	//
	m_OutputViewPort.TopLeftX = 0.0f;
	m_OutputViewPort.TopLeftY = 0.0f;
	m_OutputViewPort.Width = static_cast<float>(texWidth);
	m_OutputViewPort.Height = static_cast<float>(texHeight);
	m_OutputViewPort.MinDepth = 0.0f;
	m_OutputViewPort.MaxDepth = 1.0f;

	return S_OK;
}

void TextureRender::Begin(const FLOAT backgroundColor[4], bool notChangeDSV,ID3D11DeviceContext* deviceContext)
{
	// 缓存渲染目标和深度模板视图
	deviceContext->OMGetRenderTargets(1, m_pCacheRTV.GetAddressOf(), m_pCacheDSV.GetAddressOf());
	// 缓存视口
	UINT num_Viewports = 1;
	deviceContext->RSGetViewports(&num_Viewports, &m_CacheViewPort);


	// 清空缓冲区
	for (int i = 0; i < m_BufferCount; ++i)
	{
		deviceContext->ClearRenderTargetView(m_pOutputTextureRTV[i].Get(), backgroundColor);
	}
	
	deviceContext->ClearDepthStencilView(m_pOutputTextureDSV.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	// 设置渲染目标和深度模板视图
	ID3D11RenderTargetView* view[MaxBufferCount] = { nullptr };
	for (int i = 0; i < m_BufferCount; ++i)
	{
		view[i] = m_pOutputTextureRTV[i].Get();
	}
	deviceContext->OMSetRenderTargets(m_BufferCount, view, notChangeDSV ? m_pCacheDSV.Get() : m_pOutputTextureDSV.Get());
	// 设置视口
	deviceContext->RSSetViewports(1, &m_OutputViewPort);
}

void TextureRender::End(ID3D11DeviceContext* deviceContext)
{
	// 恢复默认设定
	deviceContext->RSSetViewports(1, &m_CacheViewPort);
	deviceContext->OMSetRenderTargets(1, m_pCacheRTV.GetAddressOf(), m_pCacheDSV.Get());

	// 若之前有指定需要mipmap链，则生成
	if (m_GenerateMips)
	{
		for (int i=0;i<m_BufferCount+m_DepthMap;++i)
		{
			deviceContext->GenerateMips(m_pOutputTextureSRV[i].Get());
		}
		
	}

	// 清空临时缓存的渲染目标视图和深度模板视图
	m_pCacheDSV.Reset();
	m_pCacheRTV.Reset();
}

ID3D11ShaderResourceView* TextureRender::GetOutputTexture(unsigned id)
{
	/*if (m_pOutputTextureSRV.size() < id)
		return nullptr;*/

	return m_pOutputTextureSRV[id].Get();
}

ID3D11ShaderResourceView* TextureRender::GetDepthTexture()
{
	if (m_DepthMap)
		return m_pOutputTextureSRV[m_BufferCount].Get();
	return nullptr;
}

ID3D11Texture2D* TextureRender::GetOutputTexture2D(unsigned id)
{
	return m_pTexture[id].Get();
}


