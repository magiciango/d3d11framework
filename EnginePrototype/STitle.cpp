#include "STitle.h"
#include "Game.h"
#include "Asset.h"
bool STitle::Init()
{
	if (!InitResource())
		return false;

	BasicEffect& effect = BasicEffect::Get();
	//effect.SetTextureCube(m_SkyBox.GetTextureCube());
	effect.SetFogStart(30);
	effect.SetFogRange(100);

	effect.SetFogColor(Colors::Red);
	effect.SetFogState(true);
	//Get::InputState().SetMouseMode(true);
	D3DAppGet::InputState().SetMouseMode(true);
	//m_FadeAmount = 1;
	return true;
}

int STitle::UpdateScene(float dt)
{
	
#pragma region ImGuiWindow
	ImGui::Begin("DebugMenu");
	{
		ImGui::Text("Press Shift Can Click Object In The Scene");
		if (ImGui::TreeNode("Create"))
		{
			if (ImGui::Button("CreateNewStaticMeshActor"))
			{
				CreateActor(true);
			}
			ImGui::SameLine();
			if (ImGui::Button("CreateGeometry"))
			{
				CreateActor(false);
			}
			ImGui::TreePop();
		}
		
		if (ImGui::TreeNode("Renderer"))
		{
			static int s = 0;
			if (ImGui::RadioButton("Solid", &s, 0))
				m_DrawSolid = true;
			ImGui::SameLine();
			if (ImGui::RadioButton("WireFrame", &s, 1))
				m_DrawSolid = false;
			ImGui::Text("DrawDebugTex");
			static int e = 0;
			ImGui::Checkbox("UseShadow", &m_UseShadow);
			ImGui::Checkbox("UseSSAO", &m_UseSSAO);
			if (ImGui::RadioButton("None", &e, 0))
				m_DebugTexture = None;

			if (m_UseShadow)
			{
				ImGui::SameLine();
				if (ImGui::RadioButton("Shadow", &e, 1))
					m_DebugTexture = ShadowMap;
			}

			if (m_UseSSAO)
			{
				ImGui::SameLine();
				if (ImGui::RadioButton("SSAO", &e, 2))
					m_DebugTexture = SSAOMap;
			}

			ImGui::TreePop();
		}
		if (ImGui::TreeNode("Misc"))
		{
			ImGui::Checkbox("RainParticle", &m_UseRain);
			ImGui::Checkbox("UseToneMapping", &m_UseToneMapping);
			ImGui::TreePop();
		}	
		
	}
	ImGui::End();
	ImGui::Begin("SkyBox");
	ImGui::Checkbox("UseDynamicSkyBox", &m_UseDynamicBox);
	ImGui::End();
#pragma endregion
	if (m_UseDynamicBox)
	{
		m_DynamicSkyBox.Update(dt);
		m_LightMng.m_DirectionList[0] = m_DynamicSkyBox.GetSunLight();
	}
	else
	{
		ImGui::Begin("SkyBox");
		if (ImGui::Button("ChangeSkyBox01"))
		{
			m_SkyBox.InitResource(D3DAppGet::pDevice(), D3DAppGet::pContext(), LSky_01, 5000.0f, 5000.0f);
		}
		if (ImGui::Button("ChangeSkyBox02"))
		{
			m_SkyBox.InitResource(D3DAppGet::pDevice(), D3DAppGet::pContext(), LSky_02, 5000.0f, 5000.0f);
		}
		if (ImGui::Button("ChangeSkyBox03"))
		{
			m_SkyBox.InitResource(D3DAppGet::pDevice(), D3DAppGet::pContext(), LSky_03, 5000.0f, 5000.0f);
		}
		if (ImGui::Button("ChangeSkyBox04"))
		{
			m_SkyBox.InitResource(D3DAppGet::pDevice(), D3DAppGet::pContext(), LSky_04, 5000.0f, 5000.0f);
		}
		ImGui::End();
	}
	m_LightMng.Update(dt);
	auto& shadowEffect = ShadowEffect::Get();
	XMVECTOR dirVec = XMLoadFloat3(&m_LightMng.m_DirectionList[0].direction);
	XMMATRIX LightView = XMMatrixLookAtLH(dirVec * 20.0f * (-2.0f), g_XMZero, g_XMIdentityR1);
	shadowEffect.SetViewMatrix(LightView);
	auto& effect = BasicEffect::Get();
	static XMMATRIX T(
		0.5f, 0.0f, 0.0f, 0.0f,
		0.0f, -0.5f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.5f, 0.5f, 0.0f, 1.0f);
	// S = V * P * T
	effect.SetShadowTransformMatrix(LightView * XMMatrixOrthographicLH(40.0f, 40.0f, 20.0f, 60.0f) * T);

	//摄像机
	Input& input = D3DAppGet::InputState();
	auto cam1st = std::dynamic_pointer_cast<FirstPersonCamera>(m_pCamera);
	bool shift = input.GetKeyDown(Keyboard::LeftControl);
	if (input.GetKeyDown(Keyboard::W))
		cam1st->MoveForward(dt * (shift ? 20.0f : 6.0f));
	if (input.GetKeyDown(Keyboard::S))
		cam1st->MoveForward(dt * (shift ? -20.0f : -6.0f));
	if (input.GetKeyDown(Keyboard::A))
		cam1st->Strafe(dt * (shift ? -20.0f : -6.0f));
	if (input.GetKeyDown(Keyboard::D))
		cam1st->Strafe(dt * (shift ? 20.0f : 6.0f));
	if (input.GetButtonDown(MouseButton::RightButton))
	{
		input.SetMouseMode(false,input.GetMousePosX(),input.GetMousePosY());
		cam1st->RotateY(input.GetMouseMoveX() * 20.0f * dt);
		cam1st->Pitch(input.GetMouseMoveY() * 20.0f * dt);
	}
	if(input.GetButtonRelease(MouseButton::RightButton))
	{
		input.SetMouseMode(true, input.GetMousePosX(), input.GetMousePosY());
	}
	//if(input.GetKeyDown(Keyboard::Right))
	//	cam1st->RotateY(dt * 6.25f);
	//if(input.GetKeyDown(Keyboard::Left))
	//	cam1st->RotateY(-dt * 6.25f);
	//if (input.GetKeyDown(Keyboard::Up))
	//	cam1st->Pitch(dt * 6.25f);
	//if(input.GetKeyDown(Keyboard::Down))
	//	cam1st->Pitch(-dt * 6.25f);
	//if (input.GetKeyPressed(Keyboard::T))
	//	cam1st->RotateY(XM_PIDIV2);
	//粒子系统更新
	if (m_UseRain)
		m_pRain->Update(dt, D3DAppGet::Timer().TotalTime());

	//鼠标选中actor
	static Actor* hitObj = {};
	if (D3DAppGet::InputState().GetButtonPressed(MouseButton::LeftButton) && D3DAppGet::InputState().GetKeyDown(Keyboard::LeftShift))
	{
		CRay ray = CRay::ScreenToRay(*m_pCamera.get(), D3DAppGet::InputState().GetMousePosX(), D3DAppGet::InputState().GetMousePosY());
		float dist;
		if (ray.Hit({ collider_default }, &dist, &hitObj))
		{
			m_Choose = hitObj;
		}
	}
	
	if (m_Choose != nullptr)
	{
		ImGui::Begin("ActorMeshEdit");
		{
			if (ImGui::CollapsingHeader("Transform"))
			{
				XMFLOAT3 pos, rot, sca;
				pos = m_Choose->GetTransform().GetPosition();
				rot = m_Choose->GetTransform().GetRotation();
				rot.x *= 180 / XM_PI;
				rot.y *= 180 / XM_PI;
				rot.z *= 180 / XM_PI;
				sca = m_Choose->GetTransform().GetScale();
				ImGui::DragFloat3("Location", (float*)&pos,0.1f);
				ImGui::DragFloat3("Rotation", (float*)&rot, 0.1f);
				ImGui::DragFloat3("Scale", (float*)&sca, 0.1f);
				rot.x *= XM_PI / 180;
				rot.y *= XM_PI / 180;
				rot.z *= XM_PI / 180;
				m_Choose->GetTransform().SetPosition(pos);
				m_Choose->GetTransform().SetRotation(rot);
				m_Choose->GetTransform().SetScale(sca);
			}
			auto mesh = m_Choose->GetComponent<CStaticMesh>();
			if (mesh && ImGui::CollapsingHeader("StaticMesh"))
			{
				if (ImGui::TreeNode("MeshData"))
				{
					const char* items[] = { Model_Dragon , Model_Toukui  ,Model_Column , Model_Coner,Model_Golem,Model_House };
					static int item_current = 0;
					ImGui::Combo("MatCap Texture", &item_current, items, IM_ARRAYSIZE(items));
					if (ImGui::Button("APPLY"))
					{
						mesh->Load(items[item_current]);
						mesh->SetUseTextureType(CStaticMesh::TextureType::AllModelTexture);
						m_Choose->GetComponent<CCollider>()->SetBoundingBox(mesh->GetBoundingBox());
					}
					ImGui::TreePop();
				}
				if (ImGui::TreeNode("Material"))
				{
					XMFLOAT4 amb, diff, spec, refla;
					amb = mesh->GetMatrial().ambient;
					diff = mesh->GetMatrial().diffuse;
					spec = mesh->GetMatrial().specular;
					spec.w = spec.w / 64;
					refla = mesh->GetMatrial().reflect;
					ImGui::DragFloat("Metallic", &amb.x, 0.05, 0, 1);
					ImGui::DragFloat("Roughness", &amb.y, 0.05, 0, 1);
					/*ImGui::ColorEdit4("ambient", (float*)&amb, 0.1f);
					ImGui::ColorEdit4("diffuse", (float*)&diff, 0.1f);
					ImGui::ColorEdit4("specular", (float*)&spec, 0.1f);
					ImGui::ColorEdit4("reflect", (float*)&refla, 0.1f);*/
					spec.w = spec.w * 64;
					Material mat = { amb,diff,spec,refla };
					mesh->SetMaterial(mat);
					ImGui::TreePop();
				}
				if (ImGui::TreeNode("MatCap"))
				{
					bool MatCap = mesh->GetuseMatCap();
					ImGui::Checkbox("UseMatCap?", &MatCap);
					mesh->SetUseTextureType(CStaticMesh::TextureType::AllModelTexture);
					if (MatCap)
					{
						ImGui::SameLine();
						const char* items[] = { MatCap_01, MatCap_Blue, MatCap_Cartoon,
							MatCap_Fresnel,MatCap_Golden,MatCap_Ice,MatCap_Magical,
							MatCap_Magical,MatCap_Metal,MatCap_Normal,MatCap_Punk,
							MatCap_Reflac,MatCap_Ruby,MatCap_Water,MatCap_Yellow };
						const wchar_t* litems[] = { LMatCap_01, LMatCap_Blue, LMatCap_Cartoon,
							LMatCap_Fresnel,LMatCap_Golden,LMatCap_Ice,LMatCap_Magical,
							LMatCap_Magical,LMatCap_Metal,LMatCap_Normal,LMatCap_Punk,
							LMatCap_Reflac,LMatCap_Ruby,LMatCap_Water,LMatCap_Yellow };
						static int item_current = 0;
						ImGui::Combo("MatCap Texture", &item_current, items, IM_ARRAYSIZE(items));
						mesh->SetMatCapTexture(TextureManager::Load(litems[item_current]));	
					}
					ImGui::TreePop();
				}
			}
			auto geometry = m_Choose->GetComponent<CGeometry>();
			if (geometry && ImGui::CollapsingHeader("GeometryMesh"))
			{
				static XMUINT2 slices;
				static XMFLOAT2 uv;
				static UINT slice;
				static UINT stack;
				if (ImGui::TreeNode("MeshData"))
				{
					const char* items[] = { "Plane","Box","Sphere","Cylinder","CylinderNoCap","Cone"};
					static int item_current = 0;
					ImGui::Combo("MaterialMapping", &item_current, items, IM_ARRAYSIZE(items));
					switch (item_current)
					{
					case 0:
						
						ImGui::SliderInt2("slices", (int*)&slices, 5, 20);
						ImGui::SliderFloat2("uv", (float*)&uv, 1, 10);
						if (ImGui::Button("APPLY"))
							geometry->SetBuffer(Geometry::CreateTerrain< VertexPosNormalTangentTex, UINT >(XMFLOAT2(20, 20), slices, uv));
						break;
					case 1:
						if (ImGui::Button("APPLY"))
							geometry->SetBuffer(Geometry::CreateBox< VertexPosNormalTangentTex, UINT >());
						break;
					case 2:
						if (ImGui::Button("APPLY"))
							geometry->SetBuffer(Geometry::CreateSphere< VertexPosNormalTangentTex, UINT >());
						break;
					case 3:
						ImGui::SliderInt("slices", (int*)&slice, 10, 30);
						ImGui::SliderInt("stack", (int*)&stack, 5, 20);
						if (ImGui::Button("APPLY"))
							geometry->SetBuffer(Geometry::CreateCylinder< VertexPosNormalTangentTex, UINT >(1.0f, 2.0f, slice, stack));
						break;
					case 4:
						
						ImGui::SliderInt("slices", (int*)&slice, 10, 30);
						ImGui::SliderInt("stack", (int*)&stack, 5, 20);
						if (ImGui::Button("APPLY"))
							geometry->SetBuffer(Geometry::CreateCylinderNoCap< VertexPosNormalTangentTex, UINT >(1.0f, 2.0f, slice, stack));
						break;
					case 5:
						ImGui::SliderInt("slices", (int*)&slice, 1, 10);
						if (ImGui::Button("APPLY"))
							geometry->SetBuffer(Geometry::CreateCone< VertexPosNormalTangentTex, UINT >(1.0f, 2.0f, slice));
						break;
					}
					
					ImGui::TreePop();
				}
				if (ImGui::TreeNode("Material"))
				{
					XMFLOAT4 amb, diff, spec, refla;
					amb = geometry->GetMatrial().ambient;
					diff = geometry->GetMatrial().diffuse;
					spec = geometry->GetMatrial().specular;
					spec.w = spec.w / 64;
					refla = geometry->GetMatrial().reflect;
					ImGui::DragFloat("Metallic", &amb.x, 0.05, 0, 1);
					ImGui::DragFloat("Roughness", &amb.y, 0.05, 0, 1);
					/*ImGui::ColorEdit4("ambient", (float*)&amb, 0.1f);
					ImGui::ColorEdit4("diffuse", (float*)&diff, 0.1f);
					ImGui::ColorEdit4("specular", (float*)&spec, 0.1f);
					ImGui::ColorEdit4("reflect", (float*)&refla, 0.1f);*/
					spec.w = spec.w * 64;
					Material mat = { amb,diff,spec,refla };
					geometry->SetMaterial(mat);
					ImGui::TreePop();
				}
				if (ImGui::TreeNode("TextureMapping"))
				{
					const char* items[] = { TexSet_Coral,TexSet_Ground,TexSet_StoneWall,TexSet_Tiles,TexSet_Wood,TexSet_NONE };
					const wchar_t* litems_A[] = { LTex_Coral_Albedo,LTex_Ground_Albedo,LTex_StoneWall_Albedo,LTex_Tiles_Albedo,LTex_Wood_Albedo,nullptr };
					const wchar_t* litems_N[] = { LTex_Coral_Normal,LTex_Ground_Normal,LTex_StoneWall_Normal,LTex_Tiles_Normal,LTex_Wood_Normal,nullptr };
					const wchar_t* litems_H[] = { LTex_Coral_Height,LTex_Ground_Height,LTex_StoneWall_Height,LTex_Tiles_Height,LTex_Wood_Height,nullptr };
					const wchar_t* litems_M[] = { LTex_Coral_Metallic,LTex_Ground_Height,LTex_StoneWall_Height,LTex_Tiles_Height,LTex_Wood_Height,nullptr };
					const wchar_t* litems_R[] = { LTex_Coral_Rough,LTex_Ground_Height,LTex_StoneWall_Height,LTex_Tiles_Height,LTex_Wood_Height,nullptr };
					static int item_current = 0;
					static bool UseTerrainMap;
					ImGui::Combo("MaterialMapping", &item_current, items, IM_ARRAYSIZE(items));
					ImGui::Checkbox("UseTerrainMap", &UseTerrainMap);
					if (ImGui::Button("APPLY"))
					{
						geometry->SetTexture(TextureManager::Load(litems_A[item_current]));
						geometry->SetTextureHeight(TextureManager::Load(UseTerrainMap ? LTex_Te01_Height : litems_H[item_current]));
						geometry->SetTextureNormal(TextureManager::Load(litems_N[item_current]));
					}
					ImGui::SliderFloat("HeightScale", &geometry->m_HeightScale, 0.0f, UseTerrainMap ? 100.0f : 1.0f);
					ImGui::TreePop();
				}
				if (ImGui::TreeNode("LOD"))
				{
					ImGui::SliderFloat("MinDistance", &geometry->m_a, 0.0f, 50.0f);
					ImGui::SliderFloat("MaxDistance", &geometry->m_b, 0.0f, 50.0f);
					ImGui::SliderInt("MinLod", &geometry->m_c, 1, 20);
					ImGui::SliderInt("MaxLod", &geometry->m_d, 1, 20);
					ImGui::TreePop();
				}
			}
			if (ImGui::Button("Destory"))
			{
				m_List.DeleteObj(m_Choose);
				m_Choose = nullptr;
			}
		}
		ImGui::End();
	}
	m_List.Update(dt);
	return -1;
}

void STitle::OnResize()
{
	DX11Contexts& renderer = D3DAppGet::AllContexts();
	m_pColorBrush.Reset();
	// 创建固定颜色刷和文本格式
	HR(renderer.pd2dRenderTarget->CreateSolidColorBrush(
		D2D1::ColorF(D2D1::ColorF::White),
		m_pColorBrush.GetAddressOf()));
	HR(renderer.pdwriteFactory->CreateTextFormat(L"宋体", nullptr, DWRITE_FONT_WEIGHT_NORMAL,
		DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 15, L"zh-cn",
		m_pTextFormat.GetAddressOf()));
	/*if (m_pCamera != nullptr)
	{
		m_pCamera->SetFrustum(XM_PI / 3, AspectRatio(), 1.0f, 1000.0f);
		m_pCamera->SetViewPort(0.0f, 0.0f, (float)m_ClientWidth, (float)m_ClientHeight);
		m_BasicEffect.SetProjMatrix(m_pCamera->GetProjXM());
	}*/

}

void STitle::DrawScene(bool isOpaquePass)
{
	//m_DeferredRender->PreClusterCompute(D3DAppGet::pContext(), m_pCamera.get());
	
	if (m_DrawSolid)
	{
		if (m_UseSSAO)
		{
			auto& SSAOEffect = SSAOEffect::Get();
			SSAOEffect.SetViewMatrix(m_pCamera->GetViewXM());
			SSAOEffect.SetRenderNormalDepth(D3DAppGet::pContext(), IEffect::RenderType::RenderObject);
			SSAOEffect.SetEyePos(m_pCamera->GetPosition());
			m_pSSAOMap->Begin(D3DAppGet::pContext(), D3DAppGet::pDepthStencilView());
			{
				m_List.DrawOpaque(&SSAOEffect, m_pCamera->GetViewXM(), m_pCamera->GetProjXM());

				m_pSSAOMap->ComputeSSAO(D3DAppGet::pContext(), SSAOEffect, *m_pCamera);

				m_pSSAOMap->BlurAmbientMap(D3DAppGet::pContext(), SSAOEffect, 4);
			}
			m_pSSAOMap->End(D3DAppGet::pContext());
		}
		if (m_UseShadow)
		{
			auto& shadowEffect = ShadowEffect::Get();
			shadowEffect.SetProjMatrix(XMMatrixOrthographicLH(40.0f, 40.0f, 20.0f, 60.0f));
			shadowEffect.SetEyePos(m_pCamera->GetPosition());
			m_pShadowMap->Begin(Colors::Black);
			{
				m_List.DrawOpaque(&shadowEffect, m_pCamera->GetViewXM(), m_pCamera->GetProjXM());
			}
			m_pShadowMap->End();
		}

	}
	if (m_UseToneMapping)
		m_pPostProcessRender->Begin(Colors::White, true);
	{
		auto& effect = BasicEffect::Get();
		m_LightMng.SetLights(effect);
		if (m_UseDynamicBox)
		{
			m_DynamicSkyBox.Draw(D3DAppGet::pContext(), SkyEffect::Get(), m_pCamera.get());
			effect.SetIBL(m_DynamicSkyBox.GetDiffuseCube(), m_DynamicSkyBox.GetSpecCube(), TextureManager::Load(LTex_BRDF));
		}	
		else
		{
			m_SkyBox.Draw(D3DAppGet::pContext(), SkyEffect::Get(), m_pCamera.get());
			effect.SetIBL(m_SkyBox.GetDiffuseCube(), m_SkyBox.GetSpecCube(), TextureManager::Load(LTex_BRDF));
		}

		effect.SetAnimeFrame(-1);
		effect.SetViewMatrix(m_pCamera->GetViewXM());
		effect.SetEyePos(m_pCamera->GetPosition());
		effect.SetRenderType(IEffect::RenderType::RenderObject);
		effect.SetFillMode(m_DrawSolid ? IEffect::RSFillMode::Solid : IEffect::RSFillMode::WireFrame);
		effect.SetIsTransparent(false);
		effect.SetTexTransform(XMMatrixIdentity());
		effect.SetTextureShadow(m_UseShadow ? m_pShadowMap->GetDepthTexture() : nullptr);
		effect.SetEnableSSAO(m_DrawSolid && m_UseSSAO);
		effect.SetTextureSSAO(m_pSSAOMap->GetAmbientTexture());

		if (isOpaquePass)
		{
			m_List.DrawOpaque(&effect, m_pCamera->GetViewXM(), m_pCamera->GetProjXM());
		}
		else
		{
			m_List.DrawTransparent(&effect, m_pCamera->GetViewXM(), m_pCamera->GetProjXM());
		}
		auto& SkyEffect = SkyEffect::Get();
		SkyEffect.SetRenderDefault(D3DAppGet::pContext());

		//m_SkyBox.Draw(*m_pCamera);
		if (m_UseRain)
		{
			static XMFLOAT3 lastCameraPos = m_pCamera->GetPosition();
			XMFLOAT3 cameraPos = m_pCamera->GetPosition();
			XMVECTOR cameraPosVec = XMLoadFloat3(&cameraPos);
			XMVECTOR lastCameraPosVec = XMLoadFloat3(&lastCameraPos);
			XMFLOAT3 emitPos;
			XMStoreFloat3(&emitPos, cameraPosVec + 3.0f * (cameraPosVec - lastCameraPosVec));
			m_ParticleEffect->SetViewProjMatrix(m_pCamera->GetViewProjXM());
			m_ParticleEffect->SetEyePos(m_pCamera->GetPosition());
			m_pRain->SetEmitPos(emitPos);

			lastCameraPos = m_pCamera->GetPosition();
			m_pRain->Draw(D3DAppGet::pContext(), *m_ParticleEffect, *m_pCamera);
		}
	}
	auto& debugEffect = Texture2DEffect::Get();
	XMMATRIX mat = XMMatrixIdentity();
	UINT strides[1] = { sizeof(VertexPosTex) };
	UINT offsets[1] = { 0 };
	if (m_UseToneMapping) {
		m_pPostProcessRender->End();

		debugEffect.SetRenderToneMap(D3DAppGet::pContext());
		debugEffect.SetPosition(XMFLOAT2(0, 0));
		debugEffect.SetSize(XMFLOAT2(1280 * 2, 720 * 2));
		debugEffect.SetTextureDiffuse(m_pPostProcessRender->GetOutputTexture());
		debugEffect.Apply(D3DAppGet::pContext());

		D3DAppGet::pContext()->IASetVertexBuffers(0, 1, m_pQuadVertexBuffer.GetAddressOf(), strides, offsets);
		D3DAppGet::pContext()->IASetIndexBuffer(m_pQuadIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
		D3DAppGet::pContext()->DrawIndexed(m_QuadIndexCount, 0, 0);
	}
	
	auto TextureCubePos = [](int x,int size)
	{
		if (x == 0)
			return XMFLOAT2(size, 0);
		if (x == 1)
			return XMFLOAT2(-size, 0);
		if (x == 2)
			return XMFLOAT2(0, size);
		if (x == 3)
			return XMFLOAT2(0, -size);
		if (x == 4)
			return XMFLOAT2(0, 0);
		if (x == 5)
			return XMFLOAT2(size * 2, 0);
	};
	switch (m_DebugTexture)
	{
	case None:
		break;
	case ShadowMap:
		debugEffect.SetTextureDiffuse(m_pShadowMap->GetDepthTexture());
		debugEffect.SetRenderOneComponentGray(D3DAppGet::pContext(), 0);
		debugEffect.SetPosition(XMFLOAT2(320,-540));
		debugEffect.SetSize(XMFLOAT2(640, 360));
		debugEffect.Apply(D3DAppGet::pContext());
		D3DAppGet::pContext()->IASetVertexBuffers(0, 1, m_pQuadVertexBuffer.GetAddressOf(), strides, offsets);
		D3DAppGet::pContext()->IASetIndexBuffer(m_pQuadIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
		D3DAppGet::pContext()->DrawIndexed(m_QuadIndexCount, 0, 0);
		break;
	case SSAOMap:
		debugEffect.SetTextureDiffuse(m_pSSAOMap->GetAmbientTexture());
		debugEffect.SetRenderOneComponentGray(D3DAppGet::pContext(), 0);
		debugEffect.SetPosition(XMFLOAT2(960, -540));
		debugEffect.SetSize(XMFLOAT2(640, 360));
		debugEffect.Apply(D3DAppGet::pContext());
		D3DAppGet::pContext()->IASetVertexBuffers(0, 1, m_pQuadVertexBuffer.GetAddressOf(), strides, offsets);
		D3DAppGet::pContext()->IASetIndexBuffer(m_pQuadIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
		D3DAppGet::pContext()->DrawIndexed(m_QuadIndexCount, 0, 0);
		break;
	}
	
}

bool STitle::InitResource()
{
	// ******************
	// カメラ初期化
	//
	m_pCamera = std::shared_ptr<FirstPersonCamera>(new FirstPersonCamera);
	m_pCamera->SetViewPort(0.0f, 0.0f, (float)D3DAppGet::Width(), (float)D3DAppGet::Height());
	m_pCamera->GetTransform().SetPosition(0, 5, -15);
	m_pCamera->SetFrustum(XM_PI / 3, D3DAppGet::AspectRatio(), 0.05, 1000.0f);

	// ******************
	// ViewPort初期化
	//
	D3D11_VIEWPORT viewPort = m_pCamera->GetViewPort();
	D3DAppGet::pContext()->RSSetViewports(1, &viewPort);
	auto& effect = BasicEffect::Get();
	auto& renderer = D3DAppGet::AllContexts();
	effect.SetViewMatrix(m_pCamera->GetViewXM());
	effect.SetEyePos(m_pCamera->GetPosition());
	effect.SetProjMatrix(m_pCamera->GetProjXM());
	SSAOEffect::Get().SetProjMatrix(m_pCamera->GetProjXM());

	// ******************
	// Render Texture 初期化
	//
	m_pShadowMap = std::make_unique<TextureRender>();
	HR(m_pShadowMap->InitResource(2048, 2048, true, 1));

	m_pPostProcessRender = std::make_unique<TextureRender>();
	HR(m_pPostProcessRender->InitResource(D3DAppGet::Width(), D3DAppGet::Height(),true,1,DXGI_FORMAT_R32G32B32A32_FLOAT));

	m_pSSAOMap = std::make_unique<SSAORender>();
	HR(m_pSSAOMap->InitResource(D3DAppGet::pDevice(), D3DAppGet::Width(), D3DAppGet::Height(), XM_PI / 3, 1000.0f));
	//TODO:搞清楚这几个Fov！！！
	// ******************
	// スカイボックス 初期化
	//
	m_DynamicSkyBox.Init();
	auto quad = Geometry::Create2DShow<VertexPosTex>();

	D3D11_BUFFER_DESC vbd;
	// Vertex Buffer
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.ByteWidth = sizeof(VertexPosTex) * (UINT)quad.vertexVec.size();
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = quad.vertexVec.data();

	D3DAppGet::pDevice()->CreateBuffer(&vbd, &InitData, &m_pQuadVertexBuffer);

	// Index Buffer
	m_QuadIndexCount = (UINT)quad.indexVec.size();

	D3D11_BUFFER_DESC ibd;
	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.ByteWidth = sizeof(DWORD) * m_QuadIndexCount;
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.StructureByteStride = 0;
	ibd.MiscFlags = 0;

	InitData.pSysMem = quad.indexVec.data();
	D3DAppGet::pDevice()->CreateBuffer(&ibd, &InitData, &m_pQuadIndexBuffer);

	m_SkyBox.InitResource(D3DAppGet::pDevice(), D3DAppGet::pContext(),
		LSky_02, 5000.0f, 5000.0f);

	// ******************
	// ゲームオブジェクト　初期化
	//
	m_List.Clear();
	{
		Material material{};
		material.ambient = XMFLOAT4(0.4f, 0.4f, 0.4f, 1.0f);
		material.diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
		material.specular = XMFLOAT4(1.0f, 1.0f, 1.0f, 32.0f);
		material.reflect = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

		Actor* actor = new Actor();
		actor->GetTransform().SetPosition(0, 0, 0);
		actor->GetTransform().SetRotation(0, 0, 0);
		actor->GetTransform().SetScale(0.2, 0.2, 0.2);
		auto mesh = new CStaticMesh();
		mesh->Load(Model_Coner);
		mesh->SetUseTextureType(CStaticMesh::TextureType::AllModelTexture);
		ID3D11ShaderResourceView* ptexture;
		HR(CreateWICTextureFromFile(D3DAppGet::pDevice(), LMatCap_Golden, nullptr, &ptexture));
		mesh->SetMatCapTexture(ptexture);
		mesh->SetMaterial(material);
		auto collider = new CCollider();
		collider->SetLayer(collider_default);
		collider->SetBoundingBox(mesh->GetBoundingBox());
		actor->AddComponent(mesh);
		actor->AddComponent(collider);
		auto move = new CMove();
		move->SetSpeed(XMFLOAT3(0, 0, 1));
		actor->AddComponent(move);
		m_List.AddOpaqueObj(actor);


		auto plane = new Actor();
		plane->GetTransform().SetPosition(0, 0, 0);
		plane->GetTransform().SetScale(1, 1, 1);
		plane->GetTransform().SetRotation(0, 0.5, 0);
		auto geometry = new CGeometry();
		geometry->SetBuffer(Geometry::CreateTerrain<VertexPosNormalTangentTex, DWORD>(XMFLOAT2(20.0f, 30.0f), XMUINT2(10, 20), XMFLOAT2(1.0f, 1.0f)));

		geometry->SetMaterial(material);
		geometry->SetTexture(TextureManager::Load(LTex_StoneWall_Albedo));
		geometry->SetTextureNormal(TextureManager::Load(LTex_StoneWall_Normal));
		geometry->SetTextureHeight(TextureManager::Load(LTex_StoneWall_Height));
		plane->AddComponent(geometry);

		collider = new CCollider();
		collider->SetLayer(collider_default);
		collider->SetBoundingBox(geometry->GetBoundingBox());
		plane->AddComponent(collider);


		m_List.AddOpaqueObj(plane);
	}
	
	m_LightMng.AddDirectionLight();
	
	m_pColorBrush.Reset();
	HR(renderer.pd2dRenderTarget->CreateSolidColorBrush(
		D2D1::ColorF(D2D1::ColorF::White),
		m_pColorBrush.GetAddressOf()));
	HR(renderer.pdwriteFactory->CreateTextFormat(L"宋体", nullptr, DWRITE_FONT_WEIGHT_NORMAL,
		DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 55, L"zh-cn",
		m_pTextFormat.GetAddressOf()));

	//粒子
	m_ParticleEffect = std::make_unique<ParticleEffect>();
	m_ParticleEffect->Init(D3DAppGet::pDevice(), L"HLSL\\Rain");
	m_pRain = std::make_unique<ParticleRender>();
	ComPtr<ID3D11ShaderResourceView> pRainSRV, pRandomSRV;

	HR(CreateTexture2DArrayFromFile(D3DAppGet::pDevice(), D3DAppGet::pContext(),
		std::vector<std::wstring>{ L"..\\Texture\\raindrop.dds" }, nullptr, pRainSRV.GetAddressOf()));
	HR(CreateRandomTexture1D(D3DAppGet::pDevice(), nullptr, pRandomSRV.ReleaseAndGetAddressOf()));
	m_pRain->Init(D3DAppGet::pDevice(), 10000);
	m_pRain->SetTextureArraySRV(pRainSRV.Get());
	m_pRain->SetRandomTexSRV(pRandomSRV.Get());
	m_pRain->SetEmitDir(XMFLOAT3(0.0f, -1.0f, 0.0f));
	m_pRain->SetEmitInterval(0.0015f);
	m_pRain->SetAliveTime(3.0f);

	
	
	m_DeferredRender = make_unique<DeferredRender>();
	m_DeferredRender->InitResource(D3DAppGet::pDevice(), D3DAppGet::pContext());
	return true;
}

void STitle::CreateActor(bool isStaticMesh)
{
	Material material{};
	material.ambient = XMFLOAT4(0.4f, 0.4f, 0.4f, 1.0f);
	material.diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	material.specular = XMFLOAT4(1.0f, 1.0f, 1.0f, 32.0f);
	material.reflect = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

	Actor* actor = new Actor();
	actor->GetTransform().SetPosition(0, 0, 0);
	actor->GetTransform().SetRotation(0, 0, 0);
	actor->GetTransform().SetScale(1, 1, 1);
	if (isStaticMesh)
	{
		auto mesh = new CStaticMesh();
		mesh->Load(Model_Coner);
		mesh->SetUseTextureType(CStaticMesh::TextureType::AllModelTexture);
		ID3D11ShaderResourceView* ptexture;
		HR(CreateWICTextureFromFile(D3DAppGet::pDevice(), LMatCap_Golden, nullptr, &ptexture));
		mesh->SetMatCapTexture(ptexture);
		mesh->SetMaterial(material);
		auto collider = new CCollider();
		collider->SetLayer(collider_default);
		collider->SetBoundingBox(mesh->GetBoundingBox());
		actor->AddComponent(mesh);
		actor->AddComponent(collider);
		m_List.AddOpaqueObj(actor);
	}
	else
	{
		auto geometry = new CGeometry();
		geometry->SetBuffer(Geometry::CreateTerrain<VertexPosNormalTangentTex, DWORD>(XMFLOAT2(10.0f, 10.0f), XMUINT2(10, 20), XMFLOAT2(1.0f, 1.0f)));
		geometry->SetMaterial(material);
		actor->AddComponent(geometry);

		auto collider = new CCollider();
		collider->SetLayer(collider_default);
		collider->SetBoundingBox(geometry->GetBoundingBox());
		actor->AddComponent(collider);
		m_List.AddOpaqueObj(actor);
	}
	m_Choose = actor;
}

void STitle::EditMode()
{
	static Material mat;
	static XMFLOAT3 pos, scale, rotation;
	enum GeometryType
	{
		Cube,
		Plane,
		Cylinder,
		Cone,
		Dragon,
	};
	static GeometryType type;
	ImGui::Begin("CreateActor");
	{
		
	}
	ImGui::End();
	Actor* actor = new Actor();
	auto mesh = new CStaticMesh();
	mesh->Load(Model_Dragon);
	mesh->SetUseTextureType(CStaticMesh::TextureType::AllModelTexture);
	ID3D11ShaderResourceView* texture;
	HR(CreateWICTextureFromFile(D3DAppGet::pDevice(), LMatCap_Golden, nullptr, &texture));
	mesh->SetMatCapTexture(texture);
}
