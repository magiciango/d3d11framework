#pragma once

#include <d3d11_1.h>
#include <wrl/client.h>
#include <string>
#include "Effects.h"
#include "Camera.h"
//0.分簇
//1.获取全套贴图
//2.拆分光源
//3.计算光源并输出
class DeferredRender
{
public:
	template<class T>
	using ComPtr = Microsoft::WRL::ComPtr<T>;

	DeferredRender() = default;
	~DeferredRender() = default;
	// 不允许拷贝，允许移动
	DeferredRender(const DeferredRender&) = delete;
	DeferredRender& operator=(const DeferredRender&) = delete;
	DeferredRender(DeferredRender&&) = default;
	DeferredRender& operator=(DeferredRender&&) = default;

	void InitResource(ID3D11Device* device,
		ID3D11DeviceContext* deviceContext);

	void Draw(ID3D11Device* device,
		ID3D11DeviceContext* deviceContext);

	void PreClusterCompute(ID3D11DeviceContext* deviceContext, Camera* camera);
private:

private:
	ComPtr<ID3D11Buffer> m_pSphereVertexBuffer;
	ComPtr<ID3D11Buffer> m_pSphereIndexBuffer;
	UINT m_SphereIndexCount = 0;

	ComPtr<ID3D11Buffer> m_pQuadVertexBuffer;
	ComPtr<ID3D11Buffer> m_pQuadIndexBuffer;
	UINT m_QuadIndexCount = 0;

	ComPtr<ID3D11Texture2D>				m_pTexture2D;
	ComPtr<ID3D11ShaderResourceView>	m_pDiffuseMapSRV;		// 动态天空盒对应的着色器资源视图
	ComPtr<ID3D11UnorderedAccessView>   m_pUAV;

	DirectX::XMFLOAT4 m_TileSizes;
	DirectX::XMFLOAT2 m_ClusterFactor;
	//PreClusterBuild
	ComPtr<ID3D11Buffer>				m_pClusterListBuffer;
	ComPtr<ID3D11UnorderedAccessView>	m_pClusterListUAV;

};