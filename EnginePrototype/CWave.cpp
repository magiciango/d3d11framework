#include "CWave.h"
#include "d3dApp.h"
#include "DXTrace.h"
CWave::CWave()
{
	m_render = std::make_unique<CpuWavesRender>();
	HR(m_render->InitResource(D3DAppGet::pDevice(),
		L"..\\Texture\\water.dds", 256, 256, 5.0f, 5.0f, 0.03f, 0.625f, 2.0f, 0.5f, 0.02f, 0.1f));
	Material material;
	material.ambient = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	material.diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 0.5f);
	material.specular = XMFLOAT4(0.8f, 0.8f, 0.8f, 32.0f);
	material.reflect = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	m_render->SetMaterial(material);
	
	m_RandEngine.seed(std::random_device()());
	m_RowRange = std::uniform_int_distribution<UINT>(5, m_render->RowCount() - 5);
	m_ColRange = std::uniform_int_distribution<UINT>(5, m_render->ColumnCount() - 5);
	m_MagnitudeRange = std::uniform_real_distribution<float>(0.5f, 1.0f);

	m_BaseTime = 0;
}

void CWave::Update(float dt)
{
	m_BaseTime += dt;
	if (m_BaseTime >= 0.5f)
	{
		m_BaseTime -= 0.5f;
		m_render->Disturb(m_RowRange(m_RandEngine), m_ColRange(m_RandEngine),
				m_MagnitudeRange(m_RandEngine));

	}
	m_render->Update(dt);
}

void CWave::Draw()
{
	m_render->Draw();
}
