#include "Component.h"

#include <unordered_map>

#include "assimp/cimport.h"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "assimp/matrix4x4.h"



//変形後頂点構造体
struct Bone_Vertex
{
	//aiVector3D Position;
	//aiVector3D Normal;

	unsigned             BoneNum[4] = { 0 };
	//std::string     BoneName[4];//本来はボーンインテックスで管理するべき
	float           BoneWeight[4] = { 0.0f };

	void AddBoneData(unsigned num, float weight)
	{
		for (int i = 0; i < 4; i++)
		{
			if (BoneWeight[i] == 0.0f)
			{
				BoneWeight[i] = weight;
				BoneNum[i] = num;
				return;
			}
		}
	}
};


//ボーン構造体
struct BONE
{
	aiMatrix4x4 Matrix;
	aiMatrix4x4 AnimationMatrix;
	aiMatrix4x4 OffsetMatrix;
};

struct MeshEntry
{
	unsigned MaterialIndex;
	unsigned IndecesNum;
	unsigned BaseVertex;
	unsigned BaseIndex;
};

class AnimationModel
{
	template <class T>
	using ComPtr = Microsoft::WRL::ComPtr<T>;
private:
	const aiScene* m_AiScene = NULL;
	std::unordered_map<std::string, const aiScene*>m_Animation;

	ComPtr<ID3D11Buffer>* m_VertexBuffer;
	ComPtr<ID3D11Buffer>* m_IndexBuffer;
	std::unordered_map<std::string, ID3D11ShaderResourceView*>m_Texture;

	std::unordered_map<std::string, unsigned int>m_BoneMapping;//ボーン参照(名前とIDの対応)
	std::vector<BONE> m_Bone;//ボーンデータ
	DirectX::XMMATRIX m_BoneMat[120];
	std::vector<MeshEntry> m_Entries;
	unsigned m_BoneNum;

	BoneVertexPosNormalTex* vertex;

	void CreateBone(aiNode* node);
	void UpdateBoneMatrix(aiNode* node, aiMatrix4x4 matrix);
public:
	void Load(const char* FileName);
	void LoadAnimation(const char* FileName, const char* AnimationName);
	void Unload();
	void Update(const char* AnimationName, int Frame);
	void Draw(DirectX::XMMATRIX world);
};