#pragma once
#include <memory>
#include "Camera.h"
#include "Object.h"
#include "SkyRender.h"
#include <DirectXMath.h>
#include "CModel.h"
#include "TextureRender.h"
#include "SSAORender.h"
class Scene
{
public:
	virtual bool Init() = 0;
	virtual void OnResize() {};
	virtual int UpdateScene(float dt) = 0;
	virtual void DrawScene(bool isOpaquePass) = 0;
	virtual bool InitResource() { return true; };
	virtual Camera* GetMainCamera() { return m_pCamera.get(); };

	ObjectManager m_List;
	std::shared_ptr<Camera> m_pCamera;
};