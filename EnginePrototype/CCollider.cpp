#include "CCollider.h"
#include "Object.h"

size_t CCollider::tag = typeid(CCollider).hash_code();

std::vector<std::list<CCollider*>> CCollider::m_pColliderList =
std::vector<std::list<CCollider*>>(COLLIDERLAYER_MAX);

bool CCollider::m_UseAABB = true;

CCollider::CCollider(Actor* obj, ColliderLayer layer)
	:Component(obj),
	m_ColliderLayer(layer),
	m_LocalBox{}
{
	m_tag = CCollider::tag;
	AddCollider(this);
}

CCollider::CCollider(ColliderLayer layer)
	:Component(),
	m_ColliderLayer(layer),
	m_LocalBox{}
{
	m_tag = CCollider::tag;
	AddCollider(this);
}

BoundingBox CCollider::GetBoundingBox()
{
	BoundingBox out;
	m_LocalBox.Transform(out, GetGameObject()->GetWorldTransformMatrixXM());
	return out;
}

void CCollider::SetBoundingBox(BoundingBox& box)
{
	m_LocalBox = box;
}

BoundingOrientedBox CCollider::GetBoundingOrientedBox()
{
	BoundingOrientedBox out;
	BoundingOrientedBox::CreateFromBoundingBox(out, m_LocalBox);
	out.Transform(out, GetGameObject()->GetWorldTransformMatrixXM());
	return out;
}

ColliderLayer CCollider::GetLayer()
{
	return m_ColliderLayer;
}

void CCollider::SetLayer(ColliderLayer layer)
{
	m_pColliderList[m_ColliderLayer].remove(this);
	m_ColliderLayer = layer;
	m_pColliderList[m_ColliderLayer].push_back(this);
}

void CCollider::AddCollider(CCollider* collider)
{
	m_pColliderList[(int)collider->GetLayer()].push_back(collider);
}

void CCollider::DeleteCollider(CCollider* collider)
{
	m_pColliderList[(int)collider->GetLayer()].remove(collider);
}

ContainmentType CCollider::Contains( CCollider& collider, std::initializer_list<ColliderLayer> usingLayer, 
	Actor** ppOutObj)
{
	ContainmentType info;
	for (auto p = usingLayer.begin(); p != usingLayer.end(); ++p)			//遍历Layer
	{
		for (auto& elem : CCollider::m_pColliderList[(int)*p])		//遍历处于每个Layer内的碰撞器
		{
			if (elem == &collider)
				break;
			if (!elem->GetGameObject()->GetActive())
				break;
			if (m_UseAABB)
				info = collider.GetBoundingBox().Contains(elem->GetBoundingBox());
			else
				info = collider.GetBoundingOrientedBox().Contains(elem->GetBoundingOrientedBox());
			if (info)
			{
				if (ppOutObj != nullptr)
					*ppOutObj = elem->GetGameObject();
				return info;
			}
		}
	}
	return ContainmentType::DISJOINT;
}

void CCollider::SetDegree(bool AABB)
{
	m_UseAABB = AABB;
}

std::list<CCollider*>& CCollider::GetColliderList(ColliderLayer usingLayer)
{
	return m_pColliderList[usingLayer];
}

bool CCollider::FrustumCulling(const DirectX::FXMMATRIX View, const DirectX::CXMMATRIX Proj)
{
	BoundingFrustum frustum;
	BoundingFrustum::CreateFromMatrix(frustum, Proj);

	XMMATRIX M = GetGameObject()->GetWorldTransformMatrixXM();
	BoundingBox box;
	m_LocalBox.Transform(box, M * View);
	return !frustum.Intersects(box);
}


CRay::CRay()
	:Component(),
	m_Origin(),
	m_Direction(0.0f, 0.0f, 1.0f)
{};

CRay::CRay(const DirectX::XMFLOAT3& origin, const DirectX::XMFLOAT3& direction)
	:Component(),
	m_Origin(origin)
{
	XMVECTOR dirLength = XMVector3Length(XMLoadFloat3(&direction));
	XMVECTOR error = XMVectorAbs(dirLength - XMVectorSplatOne());
	assert(XMVector3Less(error, XMVectorReplicate(1e-5f)));

	XMStoreFloat3(&this->m_Direction, XMVector3Normalize(XMLoadFloat3(&direction)));
}

bool CRay::Hit(const DirectX::BoundingBox& box, float* pOutDist, float maxDist)
{
	return false;
}

bool CRay::Hit(const DirectX::BoundingOrientedBox& box, float* pOutDist, float maxDist)
{
	float dist;
	bool res = box.Intersects(XMLoadFloat3(&m_Origin), XMLoadFloat3(&m_Direction), dist);
	if (pOutDist)
		*pOutDist = dist;
	return dist > maxDist ? false : res;
}

bool CRay::Hit(std::initializer_list<ColliderLayer> usingLayer, float* pOutDist, Actor** ppOutObj, float maxDist)
{
	float dist = maxDist;
	float tmpDist;
	for (auto p = usingLayer.begin(); p != usingLayer.end(); ++p)
	{
		auto list = CCollider::GetColliderList(*p);
		for (auto& elem : list)
		{
			if (!elem->GetGameObject()->GetActive())
				break;
			if (Hit(elem->GetBoundingOrientedBox(), &tmpDist, dist) && tmpDist < dist)
			{
				dist = tmpDist;
				if (ppOutObj)
					*ppOutObj = elem->GetGameObject();
			}
		}
	}
	if (pOutDist)
		*pOutDist = dist;
	return dist < maxDist;

}

CRay CRay::ScreenToRay(const Camera& camera, float screenX, float screenY)
{
	// 将屏幕坐标点从视口变换回NDC坐标系
	static const XMVECTORF32 D = { { { -1.0f, 1.0f, 0.0f, 0.0f } } };
	XMVECTOR V = XMVectorSet(screenX, screenY, 0.0f, 1.0f);
	D3D11_VIEWPORT viewPort = camera.GetViewPort();

	XMVECTOR Scale = XMVectorSet(viewPort.Width * 0.5f, -viewPort.Height * 0.5f, viewPort.MaxDepth - viewPort.MinDepth, 1.0f);
	Scale = XMVectorReciprocal(Scale);

	XMVECTOR Offset = XMVectorSet(-viewPort.TopLeftX, -viewPort.TopLeftY, -viewPort.MinDepth, 0.0f);
	Offset = XMVectorMultiplyAdd(Scale, Offset, D.v);

	// 从NDC坐标系变换回世界坐标系
	XMMATRIX Transform = XMMatrixMultiply(camera.GetViewXM(), camera.GetProjXM());
	Transform = XMMatrixInverse(nullptr, Transform);

	XMVECTOR Target = XMVectorMultiplyAdd(V, Scale, Offset);
	Target = XMVector3TransformCoord(Target, Transform);

	// 求出射线
	XMFLOAT3 direction;
	XMStoreFloat3(&direction, XMVector3Normalize(Target - camera.GetPositionXM()));
	return CRay(camera.GetPosition(), direction);
}
