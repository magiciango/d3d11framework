#include "CMove.h"
//
size_t CMove::tag = typeid(CMove).hash_code();

using namespace DirectX;
CMove::CMove(Actor* obj, DirectX::XMFLOAT3 speed, DirectX::XMFLOAT3 force, DirectX::XMFLOAT3 magnetic)
	:Component(obj),
	m_Speed(speed),
	m_Force(force),
	m_Magnetic(magnetic)
{
	m_tag = CMove::tag;
}

CMove::CMove()
	:Component()
{
	m_tag = CMove::tag;
}

void CMove::Move(Transform& transform, float dt)
{
	if (m_moveble)
	{
		XMVECTOR spd, acc, lorentz;
		spd = XMLoadFloat3(&m_Speed);
		//Friction
		float length;
		XMStoreFloat(&length, XMVector3Length(spd));
		if (m_Friction * dt > length)
		{
			spd = { 0,0,0 };
			acc = { 0,0,0 };
		}
		else
		{
			acc = -XMVector3Normalize(spd) * m_Friction;
		}
		//Force
		acc += XMLoadFloat3(&m_Force);
		//Lorentz
		lorentz = XMVector3Cross(spd, XMLoadFloat3(&m_Magnetic));
		acc += lorentz;
		//SpeedCheck
		spd += acc * dt;
		if (m_MaxSpeed > 0)
		{
			XMStoreFloat(&length, XMVector3Length(spd));
			if (length > m_MaxSpeed)
				spd = XMVector3Normalize(spd) * m_MaxSpeed;
		}
		//translate
		XMStoreFloat3(&m_Speed, spd);
		XMFLOAT3 pos;
		XMStoreFloat3(&pos, transform.GetPositionXM() + spd * dt);
		transform.SetPosition(pos);
		m_Force = { 0.0f, 0.0f, 0.0f};
	}
}

void CMove::Stop()
{
	m_moveble = false;
	m_Speed = { 0,0,0 };
}

void CMove::SetSpeed(DirectX::XMFLOAT3& speed)
{
	m_Speed = speed;
}

void CMove::SetSpeed(float x, float y, float z)
{
	m_Speed = XMFLOAT3(x, y, z);
}

void CMove::SetSpeed(const DirectX::XMFLOAT3& direction, float speed)
{
	XMVECTOR F = XMLoadFloat3(&direction);
	F *= speed;
	XMStoreFloat3(&m_Speed, F);
}

void CMove::AddForce(const DirectX::XMFLOAT3& direction, float force)
{
	XMVECTOR F = XMLoadFloat3(&direction);
	F *= force;
	F += XMLoadFloat3(&m_Force);
	XMStoreFloat3(&m_Force, F);
}

void CMove::AddElastic(float strength)
{
	AddForce(m_Speed, -strength);
}
