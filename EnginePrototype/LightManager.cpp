#include "LightManager.h"
#include "imGui/imgui.h"
using namespace DirectX;
void LightManager::Update(float dt)
{
	ImGui::Begin("Lights");
	
	ImGui::DragFloat("Ambient Strength", &m_IBLStrength, 0.05, 0, 1);
	if(ImGui::TreeNode("SunLight"))
	{
		ImGui::DragFloat3("SunLightDirection", (float*)&m_DirectionList[0].direction);
		XMVECTOR v;
		v = XMLoadFloat3(&m_DirectionList[0].direction);
		v = XMVector3Normalize(v);
		XMStoreFloat3(&m_DirectionList[0].direction, v);
		ImGui::ColorEdit4("SunLightColor", (float*)&m_DirectionList[0].ambient);
		ImGui::TreePop();
	}
	if (ImGui::CollapsingHeader("PointLights"))
	{
		if (ImGui::Button("Create A New PointLight"))
		{
			AddPointLight();
		}
		for (int i = 0; i < m_PointLightList.size(); i++)
		{
			std::string tmp = "PointLight";
			tmp += std::to_string(i);
			char const* light_char = tmp.c_str();
			
			if(ImGui::TreeNode(light_char))
			{
				ImGui::ColorEdit4("Color", (float*)&m_PointLightList[i].ambient);
				ImGui::DragFloat3("Position", (float*)&m_PointLightList[i].position);
				ImGui::DragFloat3("Att", (float*)&m_PointLightList[i].att, 0.01, 0, 1);
				ImGui::TreePop();
			}
			tmp += "Destory";
			light_char = tmp.c_str();
			if (ImGui::Button(light_char))
			{
				DestoryPointLight(i);
				break;
			}

		}
	}

	if (ImGui::CollapsingHeader("SpotLights"))
	{
		if (ImGui::Button("Create A New SpotLight"))
		{
			AddSpotLight();
		}
		for (int i = 0; i < m_SpotLightList.size(); i++)
		{
			std::string tmp = "SpotLight";
			tmp += std::to_string(i);
			char const* light_char = tmp.c_str();

			if (ImGui::TreeNode(light_char))
			{
				ImGui::ColorEdit4("Color", (float*)&m_SpotLightList[i].ambient);
				ImGui::DragFloat3("Position", (float*)&m_SpotLightList[i].position);
				ImGui::DragFloat3("Direction", (float*)&m_SpotLightList[i].direction, 0.1, -1, 1);
				XMVECTOR v;
				v = XMLoadFloat3(&m_SpotLightList[i].direction);
				v = XMVector3Normalize(v);
				XMStoreFloat3(&m_SpotLightList[i].direction, v);
				ImGui::DragFloat3("Att", (float*)&m_SpotLightList[i].att, 0.01, 0, 1);
				ImGui::DragFloat("Spot", &m_SpotLightList[i].spot, 1, 0);
				ImGui::TreePop();
			}
			tmp += "Destory";
			light_char = tmp.c_str();
			if (ImGui::Button(light_char))
			{
				DestorySpotLight(i);
				break;
			}

		}
	}
	ImGui::End();
}

void LightManager::SetLights(BasicEffect& effect)
{
	effect.SetIBLStrength(m_IBLStrength);
	effect.SetPointLightNumber(m_PointLightList.size());
	for (int i = 0; i < m_PointLightList.size(); i++)
	{
		effect.SetPointLight(i, m_PointLightList[i]);
	}
	effect.SetDirLightNumber(m_DirectionList.size());
	for (int i = 0; i < m_DirectionList.size(); i++)
	{
		effect.SetDirLight(i, m_DirectionList[i]);
	}
	effect.SetSpotLightNumber(m_SpotLightList.size());
	for (int i = 0; i < m_SpotLightList.size(); i++)
	{
		effect.SetSpotLight(i, m_SpotLightList[i]);
	}
}
