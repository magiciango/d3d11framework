#pragma once
#include "d3dApp.h"
#include "STitle.h"
#include "TextureRender.h"
#include "DeferredLight.h"
using namespace std;
class Game :
    public D3DApp
{
public:
    Game(HINSTANCE hInstance);
	~Game() = default;
	static Game* Get();
public:
	bool Init() override;
	void OnResize() override;
	void UpdateScene(float dt) override;
	void DrawScene() override;

	bool InitResource();
	bool LoadScene(int sceneNumber);
	bool SetScene();
	Scene* GetActiveScene();
	//OITRender* GetOITRender();
private:

	//Effect单例
	BasicEffect m_BasicEffect;
	SkyEffect m_SkyEffect;
	FadeEffect m_FadeEffect;
	//AtomosphereEffect m_AtomospereEffect;
	//SkyLightEffect m_SkyLightEffect;
	ShadowEffect m_ShadowEffect;
	SSAOEffect m_SSAOEffect;
	Texture2DEffect m_DebugEffect;
	DeferredEffect m_DeferredEffect;
	//DeferredEffect m_DeferredEffect;
	//Scene
	vector<unique_ptr<Scene>> m_pScenes;
	int m_SceneNumber;
	int m_NextScene;
	//SceneMesh
	std::unique_ptr<CGeometry> m_ScreenMesh;
	//FadeEffect
	std::unique_ptr<TextureRender> m_pFadeRender_old;
	std::unique_ptr<TextureRender> m_pFadeRender_new;

	bool m_FadeUsed;				//-1:FadeOut 0:nothing 1:FadeIn
	float m_FadeAmount;				//0-1
	enum class FadeType
	{
		normal,
		curl,
		Grid
	};
	FadeType m_FadeType;
	bool UseDeferred = false;
	bool UseOIT = false;
};