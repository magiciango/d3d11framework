#pragma once
#include <d3d11_1.h>
#include <wrl/client.h>
#include "LightHelper.h"
#include "Effects.h"
#include <vector>
using namespace std;
#define MaxPointLight		1024
#define MaxDirectionLight	1024
#define MaxSpotLight		1024
class LightManager
{
	template<class T>
	using ComPtr = Microsoft::WRL::ComPtr<T>;
public:
	void Update(float dt);
	void SetLights(BasicEffect& effect);
	void AddPointLight() { 
		PointLight light; 
		light.position = DirectX::XMFLOAT3(0, 0, 0);
		light.att = DirectX::XMFLOAT3(1, 0, 0);
		light.ambient = DirectX::XMFLOAT4(1, 1, 1, 1);
		m_PointLightList.push_back(light); 
	};
	void AddDirectionLight() { 
		DirectionalLight light; 
		light.ambient = DirectX::XMFLOAT4(1, 1, 1, 1);
		light.direction = DirectX::XMFLOAT3(0, -1, 0);
		m_DirectionList.push_back(light); };;
	void AddSpotLight() { 
		SpotLight light; 
		light.ambient = DirectX::XMFLOAT4(1, 1, 1, 1);
		light.att = DirectX::XMFLOAT3(1, 0, 0);
		light.direction = DirectX::XMFLOAT3(0, -1, 0);
		light.position = DirectX::XMFLOAT3(1, 1, 1);
		light.spot = 5;
		m_SpotLightList.push_back(light); 
	};;
	void DestoryPointLight(int num)
	{
		if (num < m_PointLightList.size())
		{
			auto it = m_PointLightList.begin() + num;
			m_PointLightList.erase(it);
		}
			
	}
	void DestoryDirectionLight(int num)
	{
		if (num < m_DirectionList.size())
		{
			auto it = m_DirectionList.begin() + num;
			m_DirectionList.erase(it);
		}
	}
	void DestorySpotLight(int num)
	{
		if (num < m_SpotLightList.size())
		{
			auto it = m_SpotLightList.begin() + num;
			m_SpotLightList.erase(it);
		}
	}
public:
	vector<PointLight> m_PointLightList;
	vector<DirectionalLight> m_DirectionList;
	vector<SpotLight> m_SpotLightList;
	float m_IBLStrength = 0.4f;
private:
	ComPtr<ID3D11Buffer> m_PointBuffer;
	ComPtr<ID3D11ShaderResourceView> m_PointSRV;

	ComPtr<ID3D11Buffer> m_DirectionBuffer;
	ComPtr<ID3D11ShaderResourceView> m_DirectionSRV;

	ComPtr<ID3D11Buffer> m_SpotBuffer;
	ComPtr<ID3D11ShaderResourceView> m_SpotSRV;
};

